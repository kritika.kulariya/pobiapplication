﻿'use strict';

/* Controllers */

angular.module('Capbility.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })
    .controller('CapbController', ['$scope', 'dataFactory', '$http', '$rootScope', function ($scope, dataFactory, $http, $rootScope) {
        var token = ""; var arr = []; var objects = []; var tabledata = [];


        token = $rootScope.token;

        dataFactory.Capability(token)
            .then(function (response) {

                $('#mydiv').hide();
                //var data = response.data;



                !function () {

                    var operation = d3.select('body').append('div').append('h2');

                    var data = [
                        {
                            "id": 1,
                            "name": "Planning and Preparation",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 1,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -2.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 1,
                                    "name": "Personal Management ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 2,
                                    "name": "Target Management ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 3,
                                    "name": "Time Management ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 4,
                                    "name": "Preparation ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 5,
                                    "name": "Using Data ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 6,
                                    "name": "Internal Relationship Development ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 2,
                            "name": "Business Intel",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 2,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -1.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 7,
                                    "name": "Market Knowledge ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 8,
                                    "name": "Compliance Knowledge ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 9,
                                    "name": "Product Knowledge ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 10,
                                    "name": "Company Knowledge ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 3,
                            "name": "Opening and Introduction",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 3,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -0.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 11,
                                    "name": "Opening ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 12,
                                    "name": "Rapport building ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 13,
                                    "name": "Control ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 4,
                            "name": "Understanding Needs and Wants",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 2,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -1.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 14,
                                    "name": "Questioning ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 15,
                                    "name": "Listening ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 16,
                                    "name": "Buying & Decision Making ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 17,
                                    "name": "Identifying influences ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 18,
                                    "name": "Clarifying and summarising ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 5,
                            "name": "Presenting",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 1,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -2.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 19,
                                    "name": "Creating solutions ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 20,
                                    "name": "Personal Impact ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 21,
                                    "name": "Presentation Skills ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 22,
                                    "name": "Needs matching ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 6,
                            "name": "Closing",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 0,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -3.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 23,
                                    "name": "Objection Handling ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 24,
                                    "name": "Negotiation skills ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 25,
                                    "name": "Closing techniques ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 26,
                                    "name": "Summarising ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 7,
                            "name": "Growing the Relationship",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 4,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": 0.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 27,
                                    "name": "Delivering ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 28,
                                    "name": "Communication Methods ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 29,
                                    "name": "Follow up and review ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 30,
                                    "name": "Managing Process ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 31,
                                    "name": "Managing Relationships ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 32,
                                    "name": "Networking ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 8,
                            "name": "Business Intelligence",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 3,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -0.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 33,
                                    "name": "Know the Market ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 34,
                                    "name": "Know your Competitors ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 35,
                                    "name": "Know your Customers ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 36,
                                    "name": "Know your Business ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 37,
                                    "name": "Fair and Ethical ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 38,
                                    "name": "Social Awareness and Participation ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 9,
                            "name": "Personal Preparation",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 2,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -1.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 39,
                                    "name": "Personal Attributes ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 40,
                                    "name": "Personal Awareness/Development ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 41,
                                    "name": "Planning and Time Management ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 42,
                                    "name": "Financial Acumen ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 43,
                                    "name": "Management Information ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 44,
                                    "name": "Leading from the Front ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 45,
                                    "name": "Management Style ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 10,
                            "name": "Creating the Environment",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 1,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -2.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 46,
                                    "name": "Strategy ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 1,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -2.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 47,
                                    "name": "Vision ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 48,
                                    "name": "Communicating ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 49,
                                    "name": "Innovation ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 50,
                                    "name": "Managing Change ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 51,
                                    "name": "Collaboration ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 52,
                                    "name": "Empowerment ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 53,
                                    "name": "Inspiration/Morale ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 11,
                            "name": "Developing Teams",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 2,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -1.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 54,
                                    "name": "Team Design ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 1,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -2.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 55,
                                    "name": "Team Dynamics ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 2,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -1.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 56,
                                    "name": "Performance Evaluation ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 1,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -2.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 57,
                                    "name": "Performance Appraisal ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 2,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -1.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 58,
                                    "name": "Feedback ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 0,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -3.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 59,
                                    "name": "Coaching ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 2,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -1.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 60,
                                    "name": "Mentoring ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 3,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -0.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 61,
                                    "name": "Development ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 4,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": 0.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 62,
                                    "name": "Recognition ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": 2,
                                    "isAssessed": true,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -1.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        },
                        {
                            "id": 12,
                            "name": "Execute with Excellence",
                            "level": 1,
                            "benchmarkScore": 3.5,
                            "assessmentScore": 1,
                            "isAssessed": true,
                            "isBenchmarked": true,
                            "gapToBenchmark": -2.5,
                            "userID": 1,
                            "userName": "Surname, Manager1P",
                            "children": [
                                {
                                    "id": 63,
                                    "name": "Decisions ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 64,
                                    "name": "Control ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 65,
                                    "name": "Provide focus ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 66,
                                    "name": "Removing Barriers ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 67,
                                    "name": "Negotiation and influence ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 68,
                                    "name": "Conflict Management ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                },
                                {
                                    "id": 69,
                                    "name": "Meetings ",
                                    "level": 2,
                                    "benchmarkScore": 3.5,
                                    "assessmentScore": -1,
                                    "isAssessed": false,
                                    "isBenchmarked": true,
                                    "gapToBenchmark": -4.5,
                                    "userID": 0,
                                    "userName": null,
                                    "children": []
                                }
                            ]
                        }];

                    //var  data =
                    //      [
                    //          {
                    //              "key": "Nokia Smartphone",
                    //              "values": [
                    //                  { "axis": "Battery Life", "value": 0.26 }, { "axis": "Brand", "value": 0.10 },
                    //                  { "axis": "Contract Cost", "value": 0.30 }, { "axis": "Design And Quality", "value": 0.14 },
                    //                  { "axis": "Have Internet Connectivity", "value": 0.22 }, { "axis": "Large Screen", "value": 0.04 },
                    //                  { "axis": "Price Of Device", "value": 0.41 }, { "axis": "To Be A Smartphone", "value": 0.30 }
                    //              ]
                    //          },
                    //          {
                    //              "key": "Samsung",
                    //              "values": [
                    //                  { "axis": "Battery Life", "value": 0.27 }, { "axis": "Brand", "value": 0.16 },
                    //                  { "axis": "Contract Cost", "value": 0.35 }, { "axis": "Design And Quality", "value": 0.13 },
                    //                  { "axis": "Have Internet Connectivity", "value": 0.20 }, { "axis": "Large Screen", "value": 0.13 },
                    //                  { "axis": "Price Of Device", "value": 0.35 }, { "axis": "To Be A Smartphone", "value": 0.38 }
                    //              ]
                    //          },
                    //          {
                    //              "key": "iPhone",
                    //              "values": [
                    //                  { "axis": "Battery Life", "value": 0.22 }, { "axis": "Brand", "value": 0.28 },
                    //                  { "axis": "Contract Cost", "value": 0.29 }, { "axis": "Design And Quality", "value": 0.17 },
                    //                  { "axis": "Have Internet Connectivity", "value": 0.22 }, { "axis": "Large Screen", "value": 0.02 },
                    //                  { "axis": "Price Of Device", "value": 0.21 }, { "axis": "To Be A Smartphone", "value": 0.50 }
                    //              ]
                    //          }
                    //      ];

                    setTimeout(function () {
                        operation.text(' radarChart.data(data).duration(1000).update(); ');
                        radarChart.data(data).duration(1000).update();
                    }, 200);

                    setTimeout(function () {
                        operation.html(" radarChart.options({'legend': {display: true}}); <br> radarChart.colors({'Planning and Preparation': 'blue', 'Business Intel': 'red', 'Opening and Introduction': 'yellow', 'Understanding Needs and Wants':'pink','Presenting':'green','Closing':'olive','Growing the Relationship':'aqua','Business Intelligence':'cadetblue','Personal Preparation':'crimson','Creating the Environment':'orange','Developing Teams':'maroon','Execute with Excellence':'brown'}).update(); ");
                        radarChart.options({ 'legend': { display: true } });
                        radarChart.colors({ 'Planning and Preparation': 'blue', 'Business Intel': 'red', 'Opening and Introduction': 'yellow', 'Understanding Needs and Wants': 'pink', 'Presenting': 'green', 'Closing': 'olive', 'Growing the Relationship': 'aqua', 'Business Intelligence': 'cadetblue', 'Personal Preparation': 'crimson', 'Creating the Environment': 'orange', 'Developing Teams': 'maroon', 'Execute with Excellence': 'brown' }).update();
                    }, 4000);

                    setTimeout(function () {
                        operation.html(" radarChart.filterAxes(7); <br> radarChart.options({circles: {maxValue: 1, levels: 4}}).update(); ");
                        radarChart.filterAxes(7);
                        radarChart.options({ circles: { maxValue: 1, levels: 4 } }).update();
                    }, 8000);

                    setTimeout(function () {
                        operation.text(" radarChart.maxValue(.5).levels(7).update(); ");
                        radarChart.maxValue(.5).levels(7).update();
                    }, 12000);

                    setTimeout(function () {
                        operation.text(" radarChart.invert(4).update(); ");
                        radarChart.invert(4).update();
                    }, 16000);

                    setTimeout(function () {
                        operation.text(" radarChart.ranges({'Contract Cost': [-1, 2]}).update(); ");
                        radarChart.ranges({ 'Contract Cost': [-1, 2] }).update();
                    }, 20000);

                    setTimeout(function () {
                        operation.html(" data.forEach(function(e) { e.values.forEach(function(v) { v.value = (Math.random() * .6) + .2; }) })<br> radarChart.data(data).update(); ");
                        var chart_data = JSON.parse(JSON.stringify(data));
                        chart_data.forEach(function (e) { e.values.forEach(function (v) { v.value = (Math.random() * .6) + .2; }) })
                        radarChart.data(chart_data).update();
                    }, 24000);

                    setTimeout(function () {
                        operation.html(" var one = radarChart.slice(1, 2); <br> radarChart.data(one).update(); ");
                        var one = radarChart.slice(1, 2);
                        radarChart.data(one).update();
                    }, 28000);

                    setTimeout(function () {
                        operation.html(" radarChart.ranges({'Contract Cost': []}).invert(4); <br> radarChart.data(data).update(); ");
                        radarChart.ranges({ 'Contract Cost': [] }).invert(4);
                        radarChart.data(data).update();
                    }, 32000);

                    setTimeout(function () {
                        operation.html(" radarChart.options({circles: {fill: 'violet'}}); <br> radarChart.options({axes: {lineColor: 'lightyellow'}}); <br> radarChart.options({circles: {color: '#FF99CC'}}); <br> radarChart.colors({'Planning and Preparation': 'blue', 'Business Intel': 'red', 'Opening and Introduction': 'yellow', 'Understanding Needs and Wants':'pink','Presenting':'green','Closing':'olive','Growing the Relationship':'aqua','Business Intelligence':'cadetblue','Personal Preparation':'crimson','Creating the Environment':'orange','Developing Teams':'maroon','Execute with Excellence':'brown'}); ");
                        radarChart.options({ circles: { fill: 'violet', color: '#FF99CC' } });
                        radarChart.options({ axes: { lineColor: "lightyellow" } });
                        radarChart.colors({ 'Planning and Preparation': 'blue', 'Business Intel': 'red', 'Opening and Introduction': 'yellow', 'Understanding Needs and Wants': 'pink', 'Presenting': 'green', 'Closing': 'olive', 'Growing the Relationship': 'aqua', 'Business Intelligence': 'cadetblue', 'Personal Preparation': 'crimson', 'Creating the Environment': 'orange', 'Developing Teams': 'maroon', 'Execute with Excellence': 'brown' });
                        radarChart.update();
                    }, 36000);

                    setTimeout(function () {
                        operation.text(" radarChart.options({circles: {maxValue: 1, levels: 3}, legend: {symbol: 'circle'}, filter: false}).update(); ");
                        radarChart.options({ circles: { maxValue: 1, levels: 3 }, legendSymbol: 'circle', filter: false }).update();
                    }, 40000);

                    setTimeout(function () {
                        operation.text(" radarChart.height(300).width(300).options({'areas': {'dotRadius': 2}}).update(); ");
                        radarChart.height(300).width(300).options({ 'areas': { 'dotRadius': 2 } }).update();
                    }, 44000);

                    setTimeout(function () {
                        operation.text(" radarChart.height(500).width(500).options({'areas': {'dotRadius': 4}}).update(); ");
                        radarChart.height(600).width(600).options({ 'areas': { 'dotRadius': 4 } }).update();
                    }, 48000);

                    setTimeout(function () {
                        operation.html(" radarChart.options({circles: {fill: '#CDCDCD', color: '#CDCDCD'}}); <br> radarChart.options({axes: {lineColor: 'white'}}); <br> radarChart.colors({}).data(data).update(); <br> radarChart.maxValue(.5).levels(7).filterAxes(7);");
                        radarChart.options({ circles: { fill: '#CDCDCD', color: '#CDCDCD' } });
                        radarChart.options({ axes: { lineColor: 'white' }, filter: 'glow' });
                        radarChart.maxValue(.5).levels(7).filterAxes(7);
                        radarChart.colors({}).data(data).update();
                    }, 52000);

                }();


                //////////////////// Draw the Chart //////////////////////////
                var color = d3.scale.ordinal()
                    .range(["#EDC951", "#CC333F", "#00A0B0"]);

                var radarChartOptions = {
                    width: 500,
                    height: 500,
                    color: color
                };

                var radarChart = RadarChart()

                d3.select('#radarChart')
                    .call(radarChart);

                radarChart.options(radarChartOptions).update();


                //$.each(data, function (index, value) {
                //    objects.push([value.url, value.uniqueVisitors]);
                //});
                //var dursum = 0; var vistsum = 0;

                //for (var i = 0; i < data.length; i++) {
                //    dursum = dursum + data[i]["totalDurationSec"];
                //    vistsum = vistsum + data[i]["uniqueVisitors"];
                //}

                //$.each(data, function (index, value) {
                //    tabledata.push([value.url, value.uniqueVisitors, value.totalDurationSec, Math.round(parseFloat((value.uniqueVisitors / vistsum) * 100)) + '%', Math.round(parseFloat((value.totalDurationSec / dursum) * 100)) + '%']);
                //});
                //addTable(tabledata);
                //$("#myTable").tablesorter({
                //    theme: 'blue',
                //    showProcessing: true,
                //    widgets: ["zebra"],
                //    widgetOptions: {
                //        zebra: ["normal-row", "alt-row"]
                //    }
                //});


                /*** UniqueVisitor Chart ***/
                //createVisitorChart(data);

                /*** Chart***/

                /*** Duration Chart ***/

                //createDurationChart(data);

                /*** Duration Chart ***/


            }, function (error) {
                alert('Capablity error');
                $('#mydiv').hide();
            });







        function RadarChart() {

            // TODO:
            // wrapWidth should probably be calculated rather than an option
            // slider to change maxValue on the fly
            // filter update make sure there is an element with URL
            //

            // options which should be accessible via ACCESSORS
            var data = [];
            var _data = [];
            var options = {
                filter: 'glow',        // define your own filter; false = no filter;

                width: window.innerWidth,
                height: window.innerHeight,

                // Margins for the SVG
                margins: {
                    top: 100,
                    right: 100,
                    bottom: 100,
                    left: 100
                },

                circles: {
                    levels: 8,
                    maxValue: 0,
                    labelFactor: 1.25,
                    opacity: 0.1,
                    fill: "#CDCDCD",
                    color: "#CDCDCD"
                },

                areas: {
                    colors: {},            // color lookup by key
                    opacity: 0.35,
                    borderWidth: 2,
                    rounded: true,
                    dotRadius: 4,
                    sort: true,          // sort layers by approximation of size, smallest on top
                    filter: []
                },

                axes: {
                    lineColor: "white",
                    lineWidth: "2px",
                    wrapWidth: 60,	      // The number of pixels after which a label needs to be given a new line
                    filter: [],
                    invert: [],
                    ranges: { "Large Screen": [0, 1] }           // { axisname: [min, max], axisname: [min, max]  }
                },

                legend: {
                    display: false,
                    symbol: 'cross', // 'circle', 'cross', 'diamond', 'triangle-up', 'triangle-down'
                    toggle: 'circle',
                    position: { x: 25, y: 25 }
                },

                color: d3.scale.category10()	   //Color function
            }

            // nodes layered such that radarInvisibleCircles always on top of radarAreas
            // and tooltip layer is at topmost layer 
            var chart_node;           // parent node for this instance of radarChart
            var hover_node;           // parent node for invisibleRadarCircles
            var tooltip_node;         // parent node for tooltip, to keep on top
            var legend_node;          // parent node for tooltip, to keep on top

            // DEFINABLE EVENTS
            // Define with ACCESSOR function chart.events()
            var events = {
                'update': { 'begin': null, 'end': null },
                'gridCircle': { 'mouseover': null, 'mouseout': null, 'mouseclick': null },
                'axisLabel': { 'mouseover': null, 'mouseout': null, 'mouseclick': null },
                'line': { 'mouseover': null, 'mouseout': null, 'mouseclick': null },
                'legend': { 'mouseover': legendMouseover, 'mouseout': areaMouseout, 'mouseclick': legendClick },
                'axis_legend': { 'mouseover': null, 'mouseout': null, 'mouseclick': null },
                'radarArea': { 'mouseover': areaMouseover, 'mouseout': areaMouseout, 'mouseclick': null },
                'radarInvisibleCircle': { 'mouseover': tooltip_show, 'mouseout': tooltip_hide, 'mouseclick': null }
            };

            // functions which should be accessible via ACCESSORS
            var updateData;

            // helper functions
            var tooltip;

            // programmatic
            var radial_calcs = {};
            var Format = d3.format('%'); // Percentage formatting
            var transition_time = 0;
            var delay = 0;
            var keys;
            var keyScale;
            var colorScale;

            function chart(selection) {
                selection.each(function () {

                    dataCalcs();
                    radialCalcs();

                    var dom = d3.select(this);

                    //////////// Create the container SVG and children g /////////////
                    var svg = dom.append('svg')
                        .attr('class', 'svg-class')
                        .attr('width', options.width)
                        .attr('height', options.height);

                    // append parent g for chart
                    chart_node = svg.append('g').attr('class', 'radar_node');
                    hover_node = svg.append('g').attr('class', 'hover_node');
                    tooltip_node = svg.append('g').attr('class', 'tooltip_node');
                    legend_node = svg.append("g").attr("class", "legendOrdinal");

                    // Wrapper for the grid & axes
                    var axisGrid = chart_node.append("g").attr("class", "axisWrapper");

                    ////////// Glow filter for some extra pizzazz ///////////
                    var filter = chart_node.append('defs').append('filter').attr('id', 'glow'),
                        feGaussianBlur = filter.append('feGaussianBlur').attr('stdDeviation', '2.5').attr('result', 'coloredBlur'),
                        feMerge = filter.append('feMerge'),
                        feMergeNode_1 = feMerge.append('feMergeNode').attr('in', 'coloredBlur'),
                        feMergeNode_2 = feMerge.append('feMergeNode').attr('in', 'SourceGraphic');

                    // Set up the small tooltip for when you hover over a circle
                    tooltip = tooltip_node.append("text")
                        .attr("class", "tooltip")
                        .style("opacity", 0);

                    // update
                    updateData = function () {
                        var duration = transition_time;

                        dataCalcs();
                        radialCalcs();

                        keys = _data.map(function (m) { return m.key; });
                        keyScale = d3.scale.ordinal()
                            .domain(_data.map(function (m) { return m._i; }))
                            .range(_data.map(function (m) { return m.key; }));
                        colorScale = d3.scale.ordinal()
                            .domain(_data.map(function (m) {
                                return options.areas.colors[keyScale(m._i)] ?
                                    keyScale(m._i)
                                    : m._i.toString();
                            }))
                            .range(_data.map(function (m) { return setColor(m); }));

                        svg.transition().delay(delay).duration(duration)
                            .attr('width', options.width)
                            .attr('height', options.height)

                        chart_node.transition().delay(delay).duration(duration)
                            .attr('width', options.width)
                            .attr('height', options.height)
                            .attr("transform",
                                "translate(" + ((options.width - (options.margins.left + options.margins.right)) / 2 + options.margins.left) + ","
                                + ((options.height - (options.margins.top + options.margins.bottom)) / 2 + options.margins.top) + ")")
                        hover_node.transition().delay(delay).duration(duration)
                            .attr('width', options.width)
                            .attr('height', options.height)
                            .attr("transform",
                                "translate(" + ((options.width - (options.margins.left + options.margins.right)) / 2 + options.margins.left) + ","
                                + ((options.height - (options.margins.top + options.margins.bottom)) / 2 + options.margins.top) + ")")
                        tooltip_node.transition().delay(delay).duration(duration)
                            .attr('width', options.width)
                            .attr('height', options.height)
                            .attr("transform",
                                "translate(" + ((options.width - (options.margins.left + options.margins.right)) / 2 + options.margins.left) + ","
                                + ((options.height - (options.margins.top + options.margins.bottom)) / 2 + options.margins.top) + ")")

                        legend_node
                            .attr("transform", "translate(" + options.legend.position.x + "," + options.legend.position.y + ")");

                        var update_gridCircles = axisGrid.selectAll(".gridCircle")
                            .data(d3.range(1, (options.circles.levels + 1)).reverse())

                        update_gridCircles
                            .transition().duration(duration)
                            .attr("r", function (d, i) { return radial_calcs.radius / options.circles.levels * d; })
                            .style("fill", options.circles.fill)
                            .style("fill-opacity", options.circles.opacity)
                            .style("stroke", options.circles.color)
                            .style("filter", function () { if (options.filter) return "url(#" + options.filter + ")" });

                        update_gridCircles.enter()
                            .append("circle")
                            .attr("class", "gridCircle")
                            .attr("r", function (d, i) { return radial_calcs.radius / options.circles.levels * d; })
                            .on('mouseover', function (d, i) { if (events.gridCircle.mouseover) events.gridCircle.mouseover(d, i); })
                            .on('mouseout', function (d, i) { if (events.gridCircle.mouseout) events.gridCircle.mouseout(d, i); })
                            .style("fill", options.circles.fill)
                            .style("fill-opacity", options.circles.opacity)
                            .style("stroke", options.circles.color)
                            .style("filter", function () { if (options.filter) return "url(#" + options.filter + ")" });

                        update_gridCircles.exit()
                            .transition().duration(duration * .5)
                            .delay(function (d, i) { return 0; })
                            .remove();

                        var update_axisLabels = axisGrid.selectAll(".axisLabel")
                            .data(d3.range(1, (options.circles.levels + 1)).reverse())

                        update_axisLabels
                            .transition().duration(duration / 2)
                            .style('opacity', 1) // don't change to 0 if there has been no change in dimensions! possible??
                            .transition().duration(duration / 2)
                            .text(function (d, i) { if (radial_calcs.maxValue) return Format(radial_calcs.maxValue * d / options.circles.levels); })
                            .attr("y", function (d) { return -d * radial_calcs.radius / options.circles.levels; })
                            .style('opacity', 1)

                        update_axisLabels.enter()
                            .append("text")
                            .attr("class", "axisLabel")
                            .attr("x", 4)
                            .attr("y", function (d) { return -d * radial_calcs.radius / options.circles.levels; })
                            .attr("dy", "0.4em")
                            .style("font-size", "10px")
                            .attr("fill", "#737373")
                            .on('mouseover', function (d, i) { if (events.axisLabel.mouseover) events.axisLabel.mouseover(d, i); })
                            .on('mouseout', function (d, i) { if (events.axisLabel.mouseout) events.axisLabel.mouseout(d, i); })
                            .text(function (d, i) { if (radial_calcs.maxValue) return Format(radial_calcs.maxValue * d / options.circles.levels); });

                        update_axisLabels.exit()
                            .transition().duration(duration * .5)
                            .remove();

                        var update_axes = axisGrid.selectAll(".axis")
                            .data(radial_calcs.axes, get_axis)

                        update_axes
                            .enter().append("g")
                            .attr("class", "axis")
                            .attr("key", function (d) { return d.name; });

                        update_axes.exit()
                            .transition().duration(duration)
                            .style('opacity', 0)
                            .remove()

                        var update_lines = update_axes.selectAll(".line")
                            .data(function (d) { return [d]; }, get_axis)

                        update_lines.enter()
                            .append("line")
                            .attr("class", "line")
                            .attr("x1", 0)
                            .attr("y1", 0)
                            .attr("x2", function (d, i, j) { return calcX(null, 1.1, j); })
                            .attr("y2", function (d, i, j) { return calcY(null, 1.1, j); })
                            .on('mouseover', function (d, i, j) { if (events.line.mouseover) events.line.mouseover(d, j); })
                            .on('mouseout', function (d, i, j) { if (events.line.mouseout) events.line.mouseout(d, j); })
                            .style("stroke", options.axes.lineColor)
                            .style("stroke-width", "2px")

                        update_lines.exit()
                            .transition().duration(duration * .5)
                            .delay(function (d, i) { return 0; })
                            .remove();

                        update_lines
                            .transition().duration(duration)
                            .style("stroke", options.axes.lineColor)
                            .style("stroke-width", options.axes.lineWidth)
                            .attr("x2", function (d, i, j) { return calcX(null, 1.1, j); })
                            .attr("y2", function (d, i, j) { return calcY(null, 1.1, j); })

                        var update_axis_legends = update_axes.selectAll(".axis_legend")
                            .data(function (d) { return [d]; }, get_axis)

                        update_axis_legends.enter()
                            .append("text")
                            .attr("class", "axis_legend")
                            .style("font-size", "11px")
                            .attr("text-anchor", "middle")
                            .attr("dy", "0.35em")
                            .attr("x", function (d, i, j) { return calcX(null, options.circles.labelFactor, j); })
                            .attr("y", function (d, i, j) { return calcY(null, options.circles.labelFactor, j); })
                            .on('mouseover', function (d, i, j) { if (events.axis_legend.mouseover) events.axis_legend.mouseover(d, i, j); })
                            .on('mouseout', function (d, i, j) { if (events.axis_legend.mouseout) events.axis_legend.mouseout(d, i, j); })
                            .call(wrap, options.axes.wrapWidth)

                        update_axis_legends.exit()
                            .transition().duration(duration * .5)
                            .delay(function (d, i) { return 0; })
                            .remove();

                        update_axis_legends
                            .transition().duration(duration)
                            .attr("x", function (d, i, j) { return calcX(null, options.circles.labelFactor, j); })
                            .attr("y", function (d, i, j) { return calcY(null, options.circles.labelFactor, j); })
                            .selectAll('tspan')
                            .attr("x", function (d, i, j) { return calcX(null, options.circles.labelFactor, j); })
                            .attr("y", function (d, i, j) { return calcY(null, options.circles.labelFactor, j); })

                        var radarLine = d3.svg.line.radial()
                            .interpolate(options.areas.rounded ?
                                "cardinal-closed" :
                                "linear-closed")
                            .radius(function (d) { return radial_calcs.rScale(d.benchmarkScore); })
                            .angle(function (d, i) { return i * radial_calcs.angleSlice; });

                        var update_blobWrapper = chart_node.selectAll(".radarWrapper")
                            .data(_data, get_key)

                        update_blobWrapper.enter()
                            .append("g")
                            .attr("class", "radarWrapper")
                            .attr("key", function (d) { return d.name; });

                        update_blobWrapper.exit()
                            .transition().duration(duration)
                            .style('opacity', 0)
                            .remove()

                        var update_radarArea = update_blobWrapper.selectAll('.radarArea')
                            .data(function (d) { return [d]; }, get_key);

                        update_radarArea.enter()
                            .append("path")
                            .attr("class", function (d) { return "radarArea " + d.name.replace(/\s+/g, '') })
                            .attr("d", function (d, i) { return radarLine(d.children); })
                            .style("fill", function (d, i, j) { return setColor(d); })
                            .style("fill-opacity", 0)
                            .on('mouseover', function (d, i) { if (events.radarArea.mouseover) events.radarArea.mouseover(d, i, this); })
                            .on('mouseout', function (d, i) { if (events.radarArea.mouseout) events.radarArea.mouseout(d, i, this); })

                        update_radarArea.exit().remove()

                        update_radarArea
                            .transition().duration(duration)
                            .style("fill", function (d, i, j) { return setColor(d); })
                            .attr("d", function (d, i) { return radarLine(d.children); })
                            .style("fill-opacity", function (d, i) {
                                return options.areas.filter.indexOf(d.name) >= 0 ? 0 : options.areas.opacity;
                            })

                        var update_radarStroke = update_blobWrapper.selectAll('.radarStroke')
                            .data(function (d) { return [d]; }, get_key);

                        update_radarStroke.enter()
                            .append("path")
                            .attr("class", "radarStroke")
                            .attr("d", function (d, i) { return radarLine(d.children); })
                            .style("opacity", 0)
                            .style("stroke-width", options.areas.borderWidth + "px")
                            .style("stroke", function (d, i, j) { return setColor(d); })
                            .style("fill", "none")
                            .style("filter", function () { if (options.filter) return "url(#" + options.filter + ")" });

                        update_radarStroke.exit().remove();

                        update_radarStroke
                            .transition().duration(duration)
                            .style("stroke", function (d, i, j) { return setColor(d); })
                            .attr("d", function (d, i) { return radarLine(d.children); })
                            .style("filter", function () { if (options.filter) return "url(#" + options.filter + ")" })
                            .style("opacity", function (d, i) {
                                return options.areas.filter.indexOf(d.name) >= 0 ? 0 : 1;
                            });

                        var update_radarCircle = update_blobWrapper.selectAll('.radarCircle')
                            .data(function (d, i) { return add_index(d._i, d.children); });

                        update_radarCircle.enter()
                            .append("circle")
                            .attr("class", "radarCircle")
                            .attr("r", options.areas.dotRadius)
                            .attr("cx", function (d, i) { return calcX(0, 0, i); })
                            .attr("cy", function (d, i) { return calcY(0, 0, i); })
                            .style("fill", function (d, i, j) { return setColor(d, d._i, _data[j].key); })
                            .style("fill-opacity", function (d, i) { return 0; })
                            .transition().duration(duration)
                            .attr("cx", function (d, i) { return calcX(d.benchmarkScore, 0, i); })
                            .attr("cy", function (d, i) { return calcY(d.benchmarkScore, 0, i); })

                        update_radarCircle.exit().remove();

                        update_radarCircle
                            .transition().duration(duration)
                            .style("fill", function (d, i, j) { return setColor(d, d._i, _data[j].key); })
                            .style("fill-opacity", function (d, i, j) {
                                var key = data.map(function (m) { return m.key })[j];
                                return options.areas.filter.indexOf(key) >= 0 ? 0 : 0.8;
                            })
                            .attr("r", options.areas.dotRadius)
                            .attr("cx", function (d, i) { return calcX(d.benchmarkScore, 0, i); })
                            .attr("cy", function (d, i) { return calcY(d.benchmarkScore, 0, i); })

                        var update_blobCircleWrapper = hover_node.selectAll(".radarCircleWrapper")
                            .data(_data, get_key)

                        update_blobCircleWrapper
                            .enter().append("g")
                            .attr("class", "radarCircleWrapper")
                            .attr("key", function (d) { return d.name; });

                        update_blobCircleWrapper.exit()
                            .transition().duration(duration)
                            .style('opacity', 0)
                            .remove()

                        var update_radarInvisibleCircle = update_blobCircleWrapper.selectAll(".radarInvisibleCircle")
                            .data(function (d, i) { return add_index(d._i, d.children); });

                        update_radarInvisibleCircle.enter()
                            .append("circle")
                            .attr("class", "radarInvisibleCircle")
                            .attr("r", options.areas.dotRadius * 1.5)
                            .attr("cx", function (d, i) { return calcX(d.benchmarkScore, 0, i); })
                            .attr("cy", function (d, i) { return calcY(d.benchmarkScore, 0, i); })
                            .style("fill", "none")
                            .style("pointer-events", "all")
                            .on('mouseover', function (d, i) {
                                if (events.radarInvisibleCircle.mouseover) events.radarInvisibleCircle.mouseover(d, i, this);
                            })
                            .on("mouseout", function (d, i) {
                                if (events.radarInvisibleCircle.mouseout) events.radarInvisibleCircle.mouseout(d, i, this);
                            })

                        update_radarInvisibleCircle.exit().remove();

                        update_radarInvisibleCircle
                            .attr("cx", function (d, i) { return calcX(d.benchmarkScore, 0, i); })
                            .attr("cy", function (d, i) { return calcY(d.benchmarkScore, 0, i); })

                        if (options.legend.display) {
                            var shape = d3.svg.symbol().type(options.legend.symbol).size(150)();
                            var foo;
                            legend_node.selectAll('cell').remove();
                            var colorScale = d3.scale.ordinal()
                                .domain(_data.map(function (m) { return m._i; }))
                                .range(_data.map(function (m) { return setColor(m); }));

                            if (d3.legend) {
                                var legendOrdinal = d3.legend.color()
                                    .shape("path", shape)
                                    .shapePadding(10)
                                    .scale(colorScale)
                                    .labels(colorScale.domain().map(function (m) { return keyScale(m); }))
                                    .on("cellclick", function (d, i) {
                                        if (events.legend.mouseclick) events.legend.mouseclick(d, i, this);
                                    })
                                    .on("cellover", function (d, i) {
                                        if (events.legend.mouseover) events.legend.mouseover(d, i, this);
                                    })
                                    .on("cellout", function (d, i) {
                                        if (events.legend.mouseout) events.legend.mouseout(d, i, this);
                                    });

                                legend_node
                                    .call(legendOrdinal);
                            }
                        }

                    }
                });
            }

            // REUSABLE FUNCTIONS
            // ------------------
            // calculate average for sorting, add unique indices for color
            // accounts for data updates and assigns unique colors when possible
            function dataCalcs() {

                // this deep copy method has limitations which should not be encountered
                // in this context
                _data = JSON.parse(JSON.stringify(data));

                var axes = getAxisLabels(_data);
                var ranges = {};

                // filter out axes
                var d_indices = axes.map(function (m, i) { return (options.axes.filter.indexOf(axes[i]) >= 0) ? i : undefined; }).reverse();
                _data.forEach(function (e) {
                    d_indices.forEach(function (i) { if (i >= 0) e.children.splice(i, 1); });
                });

                // determine min/max range for each axis
                _data.forEach(function (e) {
                    e.children.forEach(function (d, i) {
                        var range = ranges[axes[i]] ?                        // already started?
                            ranges[axes[i]]
                            : options.axes.ranges[axes[i]] ?         // rande defined in options?
                                options.axes.ranges[axes[i]].slice()
                                : [0, 1];                              // default
                        var max = d.name > range[1] ? d.name : range[1];
                        var min = d.name < range[0] ? d.name : range[0];
                        ranges[axes[i]] = [min, max];                        // update
                    })
                });

                // convert all axes to range [0,1] (procrustean)
                _data.forEach(function (e) {
                    e.children.forEach(function (d, i) {
                        if (ranges[axes[i]][0] != 0 && ranges[axes[i]][1] != 1) {
                            var range = ranges[axes[i]];
                            d.original_value = Number(d.benchmarkScore);
                            d.benchmarkScore = (d.benchmarkScore - range[0]) / (range[1] - range[0]);
                        }
                        if (options.axes.invert.indexOf(axes[i]) >= 0) { d.benchmarkScore = 1 - d.benchmarkScore; }
                    })
                })

                _data.forEach(function (d) { d['_avg'] = d3.mean(d.children, function (e) { return e.benchmarkScore }); })

                _data = options.areas.sort ?
                    _data.sort(function (a, b) {
                        var aa = a['_avg'];
                        var bb = b['_avg'];
                        return bb - aa;
                    })
                    : _data;

                var color_indices = (function (a, b) { while (a--) b[a] = a; return b })(10, []);
                var indices = _data.map(function (i) { return i._i });
                var unassigned = color_indices.filter(function (x) { return indices.indexOf(x) < 0; }).reverse();

                _data = _data.map(function (d, i) {
                    if (d['_i'] >= 0) {
                        return d;
                    } else {
                        d['_i'] = unassigned.length ? unassigned.pop() : i;
                        return d;
                    }
                });
            }

            function getAxisLabels(dataArray) {
                return dataArray.length ?
                    dataArray[0].children.map(function (i, j) { return i.name; })
                    : [];
            }

            function radialCalcs() {
                var axes = _data.length ?
                    _data[0].children.map(function (i, j) { return i; })
                    : [];
                var axisLabels = getAxisLabels(_data);

                radial_calcs = {
                    // Radius of the outermost circle
                    radius: Math.min((options.width - (options.margins.left + options.margins.right)) / 2,
                        (options.height - (options.margins.bottom + options.margins.top)) / 2),
                    axes: axes,
                    axisLabels: axisLabels,

                    // If the supplied maxValue is smaller than the actual one, replace by the max in the data
                    maxValue: Math.max(options.circles.maxValue, d3.max(_data, function (i) {
                        return d3.max(i.children.map(function (o) { return o.benchmarkScore; }))
                    }))
                }
                radial_calcs.total = radial_calcs.axes.length;

                // The width in radians of each "slice"
                radial_calcs.angleSlice = radial_calcs.total > 0 ?
                    Math.PI * 2 / radial_calcs.total
                    : 1;

                //Scale for the radius
                radial_calcs.rScale = d3.scale.linear()
                    .range([0, radial_calcs.radius])
                    .domain([0, radial_calcs.maxValue])
            }

            function modifyList(list, values, valid_list) {

                if (values.constructor === Array) {
                    values.forEach(function (e) { checkType(e); });
                } else if (typeof values != "object") {
                    checkType(values);
                } else {
                    return chart;
                }

                function checkType(v) {
                    if (!isNaN(v) && (function (x) { return (x | 0) === x; })(parseFloat(v))) {
                        checkValue(parseInt(v));
                    } else if (typeof v == "string") {
                        checkValue(v);
                    }
                }

                function checkValue(val) {
                    if (valid_list.indexOf(val) >= 0) {
                        modify(val);
                    } else if (val >= 0 && val < valid_list.length) {
                        modify(valid_list[val]);
                    }
                }

                function modify(index) {
                    if (list.indexOf(index) >= 0) {
                        remove(list, index);
                    } else {
                        list.push(index);
                    }
                }

                function remove(arr, item) {
                    for (var i = arr.length; i--;) { if (arr[i] === item) { arr.splice(i, 1); } }
                }
            }

            function calcX(value, scale, index) {
                return radial_calcs.rScale(value ?
                    value
                    : radial_calcs.maxValue * scale) * Math.cos(radial_calcs.angleSlice * index - Math.PI / 2);
            }

            function calcY(value, scale, index) {
                return radial_calcs.rScale(value ?
                    value
                    : radial_calcs.maxValue * scale) * Math.sin(radial_calcs.angleSlice * index - Math.PI / 2);
            }

            function setColor(d, index, key) {
                index = index ? index : d._i;
                key = key ? key : d.key;
                return options.areas.colors[key] ? options.areas.colors[key] : options.color(index);
            }
            // END REUSABLE FUNCTIONS

            // ACCESSORS
            // ---------
            chart.nodes = function () {
                return { svg: svg, chart: chart_node, hover: hover_node, tooltip: tooltip_node, legend: legend_node };
            }

            chart.events = function (functions) {
                if (!arguments.length) return events;
                var fKeys = Object.keys(functions);
                var eKeys = Object.keys(events);
                for (var k = 0; k < fKeys.length; k++) {
                    if (eKeys.indexOf(fKeys[k]) >= 0) events[fKeys[k]] = functions[fKeys[k]];
                }
                return chart;
            }

            chart.width = function (value) {
                if (!arguments.length) return options.width;
                options.width = value;
                return chart;
            };

            chart.height = function (value) {
                if (!arguments.length) return options.height;
                options.height = value;
                return chart;
            };

            chart.duration = function (value) {
                if (!arguments.length) return transition_time;
                transition_time = value;
                return chart;
            }

            chart.updateDimensions = function () {
                if (typeof updateDimensions === 'function') updateDimensions(transition_time);
            }

            chart.update = function () {
                if (events.update.begin) events.update.begin(_data);
                if (typeof updateData === 'function') updateData();
                setTimeout(function () {
                    if (events.update.end) events.update.end(_data);
                }, transition_time);
            }

            chart.data = function (value) {
                if (!arguments.length) return data;
                data = value;
                return chart;
            };

            chart.pop = function () {
                return data.pop();
            };

            chart.push = function (row) {
                if (row && row.constructor === Array) {
                    for (var i = 0; i < row.length; i++) {
                        check_key(row[i]);
                    }
                } else {
                    check_key(row);
                }

                function check_key(one_row) {
                    if (one_row.key && data.map(function (m) { return m.key }).indexOf(one_row.key) < 0) {
                        data.push(one_row);
                    }
                }

                return chart;
            };

            chart.shift = function () {
                return data.shift();
            };

            chart.unshift = function (row) {
                if (row && row.constructor === Array) {
                    for (var i = 0; i < row.length; i++) {
                        check_key(row[i]);
                    }
                } else {
                    check_key(row);
                }

                function check_key(one_row) {
                    if (one_row.key && data.map(function (m) { return m.key }).indexOf(one_row.key) < 0) {
                        data.unshift(one_row);
                    }
                }

                return chart;
            };

            chart.slice = function (begin, end) {
                return data.slice(begin, end);
            };

            // allows updating individual options and suboptions
            // while preserving state of other options
            chart.options = function (values) {
                if (!arguments.length) return options;
                var vKeys = Object.keys(values);
                var oKeys = Object.keys(options);
                for (var k = 0; k < vKeys.length; k++) {
                    if (oKeys.indexOf(vKeys[k]) >= 0) {
                        if (typeof (options[vKeys[k]]) == 'object') {
                            var sKeys = Object.keys(values[vKeys[k]]);
                            var osKeys = Object.keys(options[vKeys[k]]);
                            for (var sk = 0; sk < sKeys.length; sk++) {
                                if (osKeys.indexOf(sKeys[sk]) >= 0) {
                                    options[vKeys[k]][sKeys[sk]] = values[vKeys[k]][sKeys[sk]];
                                }
                            }
                        } else {
                            options[vKeys[k]] = values[vKeys[k]];
                        }
                    }
                }
                return chart;
            }

            chart.margins = function (value) {
                if (!arguments.length) return options.margins;
                var vKeys = Object.keys(values);
                var mKeys = Object.keys(options.margins);
                for (var k = 0; k < vKeys.length; k++) {
                    if (mKeys.indexOf(vKeys[k]) >= 0) options.margins[vKeys[k]] = values[vKeys[k]];
                }
                return chart;
            }

            chart.levels = function (value) {
                if (!arguments.length) return options.circles.levels;
                options.circles.levels = value;
                return chart;
            }

            chart.maxValue = function (value) {
                if (!arguments.length) return options.circles.maxValue;
                options.circles.maxValue = value;
                return chart;
            }

            chart.opacity = function (value) {
                if (!arguments.length) return options.areas.opacity;
                options.areas.opacity = value;
                return chart;
            }

            chart.borderWidth = function (value) {
                if (!arguments.length) return options.areas.borderWidth;
                options.areas.borderWidth = value;
                return chart;
            }

            chart.rounded = function (value) {
                if (!arguments.length) return options.areas.rounded;
                options.areas.rounded = value;
                return chart;
            }

            // range of colors to set color based on index
            chart.color = function (value) {
                if (!arguments.length) return options.color;
                options.color = value;
                return chart;
            }

            // colors set according to data keys
            chart.colors = function (colores) {
                if (!arguments.length) return options.areas.colors;
                options.areas.colors = colores;
                return chart;
            }

            chart.keys = function () {
                return data.map(function (m) { return m.key });
            }

            chart.axes = function () {
                return getAxisLabels(data);
            }

            // add or remove keys (or key indices) to filter axes
            chart.filterAxes = function (values) {
                if (!arguments.length) return options.axes.filter;
                var axes = getAxisLabels(data);
                modifyList(options.axes.filter, values, axes);
                return chart;
            }

            // add or remove keys (or key indices) to filter areas
            chart.filterAreas = function (values) {
                if (!arguments.length) return options.areas.filter;
                var keys = data.map(function (m) { return m.key });
                modifyList(options.areas.filter, values, keys);
                return chart;
            }

            // add or remove keys (or key indices) to invert
            chart.invert = function (values) {
                if (!arguments.length) return options.axes.invert;
                var axes = getAxisLabels(data);
                modifyList(options.axes.invert, values, axes);
                return chart;
            }

            // add or remove ranges for keys
            chart.ranges = function (values) {
                if (!arguments.length) return options.axes.ranges;
                if (typeof values == "string") return chart;

                var axes = getAxisLabels(data);

                if (values && values.constructor === Array) {
                    values.forEach(function (e) { checkRange(e); });
                } else {
                    checkRange(values);
                }

                function checkRange(range_declarations) {
                    var keys = Object.keys(range_declarations);
                    for (var k = 0; k < keys.length; k++) {
                        if (axes.indexOf(keys[k]) >= 0       // is valid axis
                            && range_declarations[keys[k]]    // range array not undefined
                            && range_declarations[keys[k]].constructor === Array
                            && checkValues(keys[k], range_declarations[keys[k]])) {
                            options.axes.ranges[keys[k]] = range_declarations[keys[k]];
                        }
                    }
                }

                function checkValues(key, range) {
                    if (range.length == 2 && !isNaN(range[0]) && !isNaN(range[1])) {
                        return true;
                    } else if (range.length == 0) {
                        delete options.axes.ranges[key];
                    }
                    return false;
                }

                return chart;
            }
            // END ACCESSORS

            // DEFAULT EVENTS
            // --------------
            function areaMouseover(d, i, self) {
                //Dim all blobs
                d3.selectAll(".radarArea")
                    .transition().duration(200)
                    .style("fill-opacity", function (d, i, j) {
                        return options.areas.filter.indexOf(d.key) >= 0 ? 0 : 0.1;
                    });
                //Bring back the hovered over blob
                d3.select(self)
                    .transition().duration(200)
                    .style("fill-opacity", function (d, i, j) {
                        return options.areas.filter.indexOf(d.key) >= 0 ? 0 : 0.7;
                    });
            }

            function areaMouseout(d, i, self) {
                //Bring back all blobs
                d3.selectAll(".radarArea")
                    .transition().duration(200)
                    .style("fill-opacity", function (d, i, j) {
                        return options.areas.filter.indexOf(d.key) >= 0 ? 0 : options.areas.opacity;
                    });
            }

            // on mouseover for the legend symbol
            function legendMouseover(d, i, self) {
                var area = keys.indexOf(d) >= 0 ? d : keyScale(d);

                //Dim all blobs
                d3.selectAll(".radarArea")
                    .transition().duration(200)
                    .style("fill-opacity", function (d, i, j) {
                        return options.areas.filter.indexOf(d.key) >= 0 ? 0 : 0.1;
                    });
                //Bring back the hovered over blob
                d3.selectAll(".radarArea." + area.replace(/\s+/g, ''))
                    .transition().duration(200)
                    .style("fill-opacity", function (d, i, j) {
                        return options.areas.filter.indexOf(d.key) >= 0 ? 0 : 0.7;
                    });
            }

            function legendClick(d, i, self) {
                var keys = data.map(function (m) { return m.key });
                modifyList(options.areas.filter, keys[d], keys);
                updateData();
                var state = d3.select(self).select('path').attr('toggle');
                if (state == 'true') {
                    var shape = d3.svg.symbol().type(options.legend.symbol).size(150)()
                } else {
                    var shape = d3.svg.symbol().type(options.legend.toggle).size(150)()
                }
                d3.select(self).select('path')
                    .attr('toggle', state == 'true' ? 'false' : 'true')
                    .attr('d', function (d, i) { return shape; });
            }

            function tooltip_show(d, i, self) {
                var value = d.original_value ? d.original_value : Format(d.value);
                newX = parseFloat(d3.select(self).attr('cx')) - 10;
                newY = parseFloat(d3.select(self).attr('cy')) - 10;

                tooltip
                    .attr('x', newX)
                    .attr('y', newY)
                    .text(value)
                    .transition().duration(200)
                    .style('opacity', 1);
            }

            function tooltip_hide() {
                tooltip
                    .transition().duration(200)
                    .style("opacity", 0);
            }

            // Helper Functions
            // ----------------

            function add_index(key, values) {
                for (var v = 0; v < values.length; v++) {
                    values[v]['_i'] = key;
                }
                return values;
            }

            var get_key = function (d) { return d && d.benchmarkScore; };
            var get_axis = function (d) { return d && d.name; };

            // Wraps SVG text	
            // modification of: http://bl.ocks.org/mbostock/7555321
            function wrap(text, width) {
                text.each(function (d, i, j) {
                    var text = d3.select(this);
                    var words = d.name.split(/\s+/).reverse();
                    var word;
                    var line = [];
                    var lineNumber = 0;
                    var lineHeight = 1.4; // ems
                    var x = calcX(null, options.circles.labelFactor, j);
                    var y = calcY(null, options.circles.labelFactor, j);
                    var dy = parseFloat(text.attr("dy"));
                    var tspan = text.text(null).append("tspan").attr("dy", dy + "em");

                    while (word = words.pop()) {
                        line.push(word);
                        tspan.text(line.join(" "));
                        if (tspan.node().getComputedTextLength() > width) {
                            line.pop();
                            tspan.text(line.join(" "));
                            line = [word];
                            tspan = text.append("tspan").attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                        }
                    }
                });
            }

            return chart;
        }





        function addTable(data) {
            var myTableDiv = document.getElementById("metric_results")
            var table = document.createElement('TABLE')
            var tableheader = document.createElement('THEAD')
            var tableBody = document.createElement('TBODY')

            table.border = '1'
            table.className = "tablesorter";
            table.id = "myTable";
            table.appendChild(tableheader);
            // table.appendChild(tableBody);

            var heading = new Array();
            heading[0] = "Section"
            heading[1] = "Unique Visitors"
            heading[2] = "Duration"
            heading[3] = "% Visitors"
            heading[4] = "% Duration"


            //TABLE COLUMNS
            var tr = document.createElement('TR');
            tableheader.appendChild(tr);
            for (var i = 0; i < heading.length; i++) {
                var th = document.createElement('TH')
                th.width = '75';
                // th.id = 'facility_header';
                th.appendChild(document.createTextNode(heading[i]));
                tr.appendChild(th);
            }
            table.appendChild(tableBody);
            //TABLE ROWS
            for (var i = 0; i < data.length; i++) {
                tr = document.createElement('TR');
                for (var j = 0; j < data[i].length; j++) {
                    var td = document.createElement('TD')
                    td.appendChild(document.createTextNode($.isNumeric(data[i][j]) == true ? Math.round(data[i][j]) : data[i][j]));
                    tr.appendChild(td)
                }
                tableBody.appendChild(tr);
            }
            myTableDiv.appendChild(table)

        }

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#myTable th');
            $('#myTable tbody tr').each(function (i, tr) {
                var obj = {},

                    $tds = $(tr).find('td');

                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }



    }]);

