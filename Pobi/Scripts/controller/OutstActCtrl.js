﻿'use strict';

/* Controllers */

var module = angular.module('OutStAct.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    });

module.controller('OutStActController', ['$scope', 'dataFactory', '$http', '$rootScope', '$timeout',
    function ($scope, dataFactory, $http, $rootScope, $timeout) {

        var token = localStorage.getItem("JWT");
        $scope.tabledata = [];
        $scope.formInfo = {};
        $scope.MainData = [];
        $scope.propertyName = 'name';
        $scope.reverse = true;
        $scope.summeryData = {};

        $scope.counter = 10;

        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };


        //token = $rootScope.token;
        dataFactory.outstAct(token)
            .then(function (response) {
                console.log(response.data);
                var data = response.data;
                //createTimelineChart(data);
                $rootScope.$on("Outsearchevent", function (event, message) {

                    $scope.formInfo = message;
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username);
                    //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    tabledata = [];
                    $.each(data1, function (index, value) {
                        tabledata.push([value.name, value.roleName, (value.managerName == null ? "" : value.managerName), value.outstandingNum, convertDate(value.achieveDate), (value.isOverdue == true ? "Yes" : "No")]);
                    });
                    addTable(tabledata);
                    createDiscretePlotter(data);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["normal-row", "alt-row"]
                        }
                    });
                });

                $rootScope.$on("Outclearevent", function (event, message) {
                    console.log('Outclearevent');
                    $scope.formInfo = message;
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username); //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    data1 = (data1.length == 0 ? data : data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');

                    tabledata = [];

                    $.each(data1, function (index, value) {
                        tabledata.push([value.name, value.roleName, (value.managerName == null ? "" : value.managerName), value.outstandingNum, convertDate(value.achieveDate), (value.isOverdue == true ? "Yes" : "No")]);
                    });
                    addTable(tabledata);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["normal-row", "alt-row"]
                        }
                    });
                });


                $('#mydiv').hide();

                $scope.tabledata = data;

                //$.each(data, function (index, value) {
                //    $scope.tabledata.push([value.name, value.roleName, (value.managerName == null ? "" : value.managerName), value.outstandingNum, convertDate(value.achieveDate), (value.isOverdue == true ? "Yes" : "No")]);
                //});
                addTable($scope.tabledata);
                $("#myTable").tablesorter({
                    theme: 'blue',
                    showProcessing: true,
                    widgets: ["zebra"],
                    widgetOptions: {
                        zebra: ["normal-row", "alt-row"]
                    }
                });


                $('.sortddl').change(function () {

                    var column = 0,//parseInt($(this).val(), 10),
                        direction = $(this).val(),//1, // 0 = ascending, 1 =  descending
                        sort = [[column, direction]];
                    if (column >= 0) {
                        $('#myTable').trigger("sorton", [sort]);
                    }
                });


            }, function (error) {

                alert('OutstActivity error');
            });
        var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };


        function addTable(data) {

            var d = groupBy(data, 'userID');
            var emptyArray = [];
            var achiveArray = [];
            var totalOS = 0;
            $scope.summeryData.OSoverdue = 0;
            Object.keys(d).forEach(function (key, index) {

                var jsndata = { UserId: key, Items: d[key] };

                totalOS += jsndata.osActivity = jsndata.Items.length;

                var overdue = 0;
                var mgrName = "";
                var name = "";
                var role = "";
                var newest;
                var oldDateArray = [];
                var latestDateArray = [];

                d[key].map(function (LD) {
                    mgrName = LD.managerName;
                    role = LD.roleName;
                    name = LD.name;

                    if (LD.isOverdue) {
                        overdue++;
                    }
                    achiveArray.push(new Date(LD.achieveDate));
                    oldDateArray.push(new Date(LD.achieveDate));
                });


                $scope.summeryData.totalOS = totalOS;
                $scope.summeryData.OSoverdue += overdue;
                $scope.summeryData.percentOD = ($scope.summeryData.totalOS / $scope.summeryData.OSoverdue) * 100;

                var percentOD = (overdue / jsndata.osActivity) * 100;

                oldDateArray.forEach((x) => {
                    if (x > new Date()) {
                        latestDateArray.push(x);
                    }
                });

                if (oldDateArray.length > 0) {
                    var earliest = new Date(Math.min.apply(null, oldDateArray));
                    earliest = convertDate(earliest);
                }
                else {
                    earliest = "";
                }

                if (latestDateArray.length > 0) {
                    newest = new Date(Math.min.apply(null, latestDateArray));
                    newest = convertDate(newest);
                } else {
                    newest = "";
                }

                jsndata.oldestDate = earliest ? earliest : "";
                jsndata.nextDate = newest ? newest : "";
                jsndata.overdue = overdue;
                jsndata.ODpercent = percentOD;
                jsndata.managerName = mgrName;
                jsndata.role = role;
                jsndata.name = name;

                emptyArray.push(jsndata);
            });


            var oldestDate;
            var longest;
            if (achiveArray.length > 0) {
                oldestDate = new Date(Math.min.apply(null, achiveArray));
                oldestDate = convertDate(oldestDate);

                longest = new Date(Math.max.apply(null, achiveArray));
                longest = convertDate(longest);
            }

            $scope.summeryData.OldestDate = oldestDate;
            $scope.summeryData.Longest = longest;

            $scope.MainData = emptyArray;
            emptyArray = $scope.MainData.slice(0, $scope.counter);

            $scope.tabledata = emptyArray;

            $('.tableO').empty();
            var table = $('.tableO');
            var tr = $('<tr>');

            $('<th  rowspan="2" title="Overall Summary" class="first-child">').html("Overall Summary").appendTo(tr);

            $('<th class="header" title="O/S Activity" style="width:12.5%">').html("O/S Activity").appendTo(tr);
            $('<th class="header" title="O/S Activity Overdue" style="width:12.5%">').html("O/S Activity Overdue").appendTo(tr);
            $('<th class="header" title="% Overdue">').html("% Overdue").appendTo(tr);
            $('<th class="header" title="Oldest Item">').html("Oldest Item").appendTo(tr);
            $('<th class="header" title="Longest Item">').html("Longest Item").appendTo(tr);
            table.append(tr);

            tr = $('<tr class="number" style="background-color: rgb(212, 227, 232);">');
            $('<td>').html($scope.summeryData.totalOS).appendTo(tr);
            $('<td>').html($scope.summeryData.OSoverdue).appendTo(tr);
            $('<td>').html((($scope.summeryData.totalOS / $scope.summeryData.OSoverdue) * 100).toFixed(1) + '%').appendTo(tr);
            $('<td>').html($scope.summeryData.OldestDate).appendTo(tr);
            $('<td>').html($scope.summeryData.Longest).appendTo(tr);
            table.append(tr);
        }

        function convertDate(date) {
            var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

            let d = new Date(Date.parse(date));
            var u = d.getDate() + ' ' + monthShortNames[d.getMonth()] + ' ' + d.getFullYear();
            return u;

        }

        $scope.btnLoadMore = function () {
            $scope.counter += 10;
            $scope.tabledata = $scope.MainData.slice(0, $scope.counter);
        };

        $scope.LoadAll = function () {
            $scope.tabledata = $scope.MainData;
        };

        $scope.ChartArray = [];
        $scope.GenerateChart = function () {
            debugger;
            $scope.showChart = "block";
            $scope.showTable = "none";
            var MainData = $scope.MainData;
            //   createDiscretePlotter(MainData);
            console.log(MainData);
            $scope.ChartArray = MainData.map(function (d) { return [d.overdue, d.name] });
            //createDiscretePlotter($scope.MainData);
            //createTimelineChart($scope.ChartArray );
        };

        $scope.ShowTable = function () {
            $scope.showChart = "none";
            $scope.showTable = "block";
        }

        function filter(data1, manager, role, aspiration, username) {
            var result = [];
            if (manager)
                result = data1.filter(function (item) { return item.managerName == manager });
            if (role)
                result = data1.filter(function (item) { return item.roleName == role });
            if (aspiration)
                result = data1.filter(function (item) { return item.roleName == aspiration });
            if (username)
                result = data1.filter(function (item) { return item.name == username });
            return result;
        }

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#myTable th');
            $('#myTable tbody tr').each(function (i, tr) {
                var obj = {},

                    $tds = $(tr).find('td');

                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }

<<<<<<< HEAD
        //(
            function createDiscretePlotter(data) {
            var ResponseData = data;//JSON.parse(localStorage.getItem("OutStandingdata"));
            debugger;
            var name = [];
            var areaDetails = [];
            var obser = [];
            var xval = ResponseData.map(function (item) { return item.name })
            for (var i = 0; i < xval.length; i++) {
                name.push(xval[i]);
            }

            var occurences = ResponseData.reduce(function (r, row) {
                r[row.name] = ++r[row.name] || 1;
                return r;
            }, {});

            var result = Object.keys(occurences).map(function (key) {
                return { key: key, value: occurences[key] };
            });

            var yAreaDescription = ResponseData.map(function (item) { return item.areaDescription });
            var yAreaName = ResponseData.map(function (item) { return item.areaName });
            var xdaysToDeadline = ResponseData.map(function (item) { return item.daysToDeadline });
            for (var i = 0; i < name.length; i++) {
                areaDetails.push("<b>Area Name: </b> " + yAreaName[i] + "<br/><b>Area Description: </b> " + yAreaDescription[i]);
                obser.push({ 'y': i, 'x': i, 'value1': result.filter(a => a.key == name[i])[0].value, 'value': xdaysToDeadline[i] < 60 ? -60 : (xdaysToDeadline[i] > 60 ? 60 : xdaysToDeadline[i]) });
                console.log(obser);
            }
            var state = {
                data: {
                    nodes: null
                },
                domains: {
                    x: null,
                    values: null
                },
                scales: {
                    x: null,
                    y: null,
                    color: null
                }
            }

            var width = 760,
                height = 760;

            var margin = {
                top: 30,
                right: 30,
                bottom: 30,
                left: 230
            }

            var chart = {
                container: '#discrete-scatterplot',
                tooltip: '#discrete-scatterplot-tooltip',
                width: width - margin.left - margin.right,
                height: height - margin.top - margin.bottom,
                levelHeight: 13,
                jitterVal: 8,
                data:
                {
                    "variables": { "y": "", "x": "" },
                    "levels": {
                        "y": name,
                        "x": areaDetails
                    },
                    "observations": obser
                }
            }

            var svg = d3.select(chart.container).append('svg')
                .attr('preserveAspectRatio', 'xMidYMid meet')
                .attr('viewBox', '0 0 ' + width + ' ' + height);

            var g = svg.append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            var grid = g.append('rect')
                .attr('class', 'grid')
                .attr('fill', '#e7e7e7')
                .attr('width', chart.width)
                .attr('height', chart.height);

            g.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + chart.height + ')')

            g.append('g')
                .attr('class', 'y axis')
                .attr('transform', 'translate(0,0)');

            var neutral = g.append('line')
                .attr('class', 'neutral-line');

            function updateChart(data) {
                var xLevels = data.levels.x;
                var yLevels = data.levels.y;
                var values = [];
                var nodes = [];

                data.observations.map(function (d) {
                    values.push(d.value);
                    nodes.push({
                        xLevel: xLevels[d.x],
                        yLevel: yLevels[d.y],
                        value: d.value,
                        value1: d.value1
                    })
                })

                chart.height = yLevels.length * chart.levelHeight;
                height = chart.height + margin.top + margin.bottom;

                var valueDomain = d3.extent([-60, 60]);
                // console.log(values);
                var x = getXScale(xLevels, valueDomain);//.domain([-5, 5]);
                //console.log(x);

                var y = d3.scale.ordinal()
                    .domain(yLevels)
                    .rangeBands([0, (chart.height)]);
                console.log(y);
                var positiveColors = d3.scale.quantize()
                    .domain([0, valueDomain[1]])
                    .range(['#52beda', '#3691a8', '#004165']);

                var negativeColors = d3.scale.linear()
                    .domain([0, valueDomain[0]])
                    .range(['#fc8e80', '#d53a26', '#FF6308']);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .innerTickSize(-chart.height)
                    .tickPadding(5)
                    .ticks(8)
                    .orient('bottom');

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .innerTickSize(-chart.width)
                    .tickPadding(10)
                    .orient('left');

                /**
                * Cache scales, data, and domains for checkbox callbacks
                **/

                state.scales = {
                    x: x,
                    y: y,
                    color: {
                        positive: positiveColors,
                        negative: negativeColors
                    }
                }

                state.data = {
                    nodes: nodes,
                };

                state.domains = {
                    x: xLevels,
                    values: valueDomain
                }

                /**
                * Transition elems
                **/

                svg.transition()
                    .attr('viewBox', '0 0 ' + width + ' ' + height);

                grid.transition()
                    .attr('width', chart.width)
                    .attr('height', chart.height);

                g.select('.x.axis').transition()
                    .attr('transform', 'translate(0,' + chart.height + ')')
                    .call(xAxis);

                g.select('.y.axis').transition()
                    .call(yAxis);


                g.append("text")
                    .attr("transform", "translate(" + 0 + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "left")
                    .style("font-weight", "bold")
                    .text("Overdue");
                g.append("text")
                    .attr("transform", "translate(" + ((chart.width / 2)) + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "middle")
                    .style("font-weight", "bold")
                    .text("Today");
                g.append("text")
                    .attr("transform", "translate(" + ((chart.width)) + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "middle")
                    .style("font-weight", "bold")
                    .text("Due");

                //g.append("text")      // text label for the x axis
                //    .attr("x", (width / 2))
                //    .attr("y", (height + margin.bottom + 5))
                //    .style("text-anchor", "middle")
                //    .style("font-weight", "bold")
                //    .text("Employees");

                //g.append("text")
                //    .attr("transform", "rotate(-90)")
                //    .attr("y", -45)//0 - margin.left)
                //    .attr("x", 0 - (height / 2))
                //    .attr("dy", "1em")
                //    .style("text-anchor", "middle")
                //    .style("font-weight", "bold")
                //    .text("Manager");

                neutral.transition()
                    .attr('x1', x(0))
                    .attr('x2', x(0))
                    .attr('y1', 0)
                    .attr('y2', chart.height - 100);

                // update all points with discretized styles if necessary
                handleDiscretize();

                /**
                * Transition nodes
                **/

                var node = g.selectAll('.node').data(nodes);

                node.exit()
                    .remove();

                var elemEnter = node.enter()
                    .append("g")

                //  .attr("transform", function (d) { return "translate(" + d.x + ",80)" })

                node.enter()
                    .append('circle')
                    .attr('class', 'node')
                    .attr('r', 5)
                    .style('opacity', 0)

                    .on('mousemove', handleMousemove)
                    .on('mouseout', handleMouseout)



                elemEnter.append("text")
                    //.attr("dy", -10)
                    //.attr("dx", 28)
                    .attr('dx', 10)
                    .attr('dy', getYPosition)
                    .text(function (d) {
                        return d.value1;
                    })


                node.transition()
                    .delay(function (d, i) { return i * 3 })
                    .duration(500)
                    .attr('cx', getXPosition)
                    .attr('cy', getYPosition)
                    .attr('stroke', getStroke)
                    .attr('fill', getFill)
                    .style('opacity', 1)
                    .style('fill', 'white');
            }

            function getXScale(xLevels, valueDomain) {
                return discretize.checked ?
                    d3.scale.ordinal()
                        .domain(xLevels)
                        .rangeBands([0, chart.width])

                    : d3.scale.linear()
                        .domain([valueDomain[0] - 0.015, valueDomain[1] + 0.015])
                        .range([0, chart.width]);
            }

            function getXPosition(d, i) {
                return discretize.checked ?
                    state.scales.x(d.xLevel) + state.scales.x.rangeBand() / 2
                    : state.scales.x(d.value)
            }
            function getYPosition(d, i) {
                var position = state.scales.y(d.yLevel) +
                    state.scales.y.rangeBand() / 2;
                if (jitter.checked) {
                    return i % 2 === 0 ?
                        position + Math.random() * chart.jitterVal
                        : position - Math.random() * chart.jitterVal
                } else {
                    return position;
                }
            }

            function getStroke(d) {
                return d.value >= 0 ?
                    d3.rgb(state.scales.color.positive(d.value)).darker(.5)
                    : d3.rgb(state.scales.color.negative(d.value)).darker(.5)
            }

            function getFill(d) {
                return d.value >= 0 ?
                    d3.rgb(state.scales.color.positive(d.value))
                    : d3.rgb(state.scales.color.negative(d.value))
            }

            function handleDiscretize() {
                state.scales.x = getXScale(state.domains.x, state.domains.values)

                margin.bottom = discretize.checked ? 140 : 30;
                height = chart.height + margin.top + margin.bottom;

                svg.transition()
                    .attr('viewBox', '0 0 ' + width + ' ' + height);

                var xAxis = d3.svg.axis()
                    .scale(state.scales.x)
                    .innerTickSize(-chart.height)
                    .tickPadding(0)
                    .ticks(8)
                    .orient('bottom');

                g.select('.x.axis').transition()
                    .attr('transform', 'translate(0,' + chart.height + ')')
                    .call(xAxis);

                svg.select('.x.axis').selectAll('text').transition()
                    .style('text-anchor', function () {
                        return discretize.checked ? 'start' : 'middle'
                    })
                    .attr('transform', function () {
                        return discretize.checked ? 'rotate(90)' : 'rotate(0)'
                    })
                    .attr('dx', function () {
                        return discretize.checked ? '0.5em' : 0
                    })
                    .attr('dy', function () {
                        return discretize.checked ? -8 : '.71em'
                    })

                d3.selectAll('.node').transition()
                    .delay(function (d, i) { return i * 3 })
                    .attr('cx', getXPosition)

                d3.select('.neutral-line').transition()
                    .style('opacity', function () {
                        return discretize.checked ? 0 : 1
                    });
            }

            var tooltip = d3.select(chart.tooltip);
            function handleMousemove(d, i) {
                var mousePosition = d3.mouse(this);
                debugger;
                tooltip
                    .style('left', mousePosition[0] - 100 + 'px')
                    .style('top', mousePosition[1] - 100 + 'px')
                    .style('opacity', 1)
                    .style('z-index', 10);

                tooltip.select('.x-level').html(d.xLevel)
                tooltip.select('.value').style('color', function () {
                    return d.value >= 0 ?
                        state.scales.color.positive(d.value)
                        : state.scales.color.negative(d.value)
                })
            }

            function handleMouseout(d, i) {
                tooltip
                    .style('opacity', 0)
                    .style('z-index', -1)

                d3.select(chart.container).selectAll('.circle')
                    .style('opacity', 1)
            }

            var container = document.querySelector('#discrete-scatterplot-selects'),
                jitter = container.querySelector('#jitterbug-perfume'),
                discretize = container.querySelector('#discretize'),
                selects = container.querySelectorAll('select');

            for (var i = 0; i < selects.length; i++) {
                var elem = selects[i];
                elem.addEventListener('change', redraw);
            }

            function getDatafilePath() {
                return chart.data;
            }

            function redraw() {
                d3.json(getDatafilePath(chart.data), updateChart(chart.data));
            };

            // initialize the chart
            redraw();

=======
        //(
        function createDiscretePlotter(data) {
            var ResponseData = data;//JSON.parse(localStorage.getItem("OutStandingdata"));
            debugger;
            var name = [];
            var areaDetails = [];
            var obser = [];
            var xval = ResponseData.map(function (item) { return item.name })
            for (var i = 0; i < xval.length; i++) {
                name.push(xval[i]);
            }

            var occurences = ResponseData.reduce(function (r, row) {
                r[row.name] = ++r[row.name] || 1;
                return r;
            }, {});

            var result = Object.keys(occurences).map(function (key) {
                return { key: key, value: occurences[key] };
            });

            var yAreaDescription = ResponseData.map(function (item) { return item.areaDescription });
            var yAreaName = ResponseData.map(function (item) { return item.areaName });
            var xdaysToDeadline = ResponseData.map(function (item) { return item.daysToDeadline });
            for (var i = 0; i < name.length; i++) {
                areaDetails.push("<b>Area Name: </b> " + yAreaName[i] + "<br/><b>Area Description: </b> " + yAreaDescription[i]);
                obser.push({ 'y': i, 'x': i, 'value1': result.filter(a => a.key == name[i])[0].value, 'value': xdaysToDeadline[i] < 60 ? -60 : (xdaysToDeadline[i] > 60 ? 60 : xdaysToDeadline[i]) });
                console.log(obser);
            }
            var state = {
                data: {
                    nodes: null
                },
                domains: {
                    x: null,
                    values: null
                },
                scales: {
                    x: null,
                    y: null,
                    color: null
                }
            }

            var width = 760,
                height = 760;

            var margin = {
                top: 30,
                right: 30,
                bottom: 30,
                left: 230
            }

            var chart = {
                container: '#discrete-scatterplot',
                tooltip: '#discrete-scatterplot-tooltip',
                width: width - margin.left - margin.right,
                height: height - margin.top - margin.bottom,
                levelHeight: 13,
                jitterVal: 8,
                data:
                {
                    "variables": { "y": "", "x": "" },
                    "levels": {
                        "y": name,
                        "x": areaDetails
                    },
                    "observations": obser
                }
            }

            var svg = d3.select(chart.container).append('svg')
                .attr('preserveAspectRatio', 'xMidYMid meet')
                .attr('viewBox', '0 0 ' + width + ' ' + height);

            var g = svg.append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            var grid = g.append('rect')
                .attr('class', 'grid')
                .attr('fill', '#e7e7e7')
                .attr('width', chart.width)
                .attr('height', chart.height);

            g.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + chart.height + ')')

            g.append('g')
                .attr('class', 'y axis')
                .attr('transform', 'translate(0,0)');

            var neutral = g.append('line')
                .attr('class', 'neutral-line');

            function updateChart(data) {
                var xLevels = data.levels.x;
                var yLevels = data.levels.y;
                var values = [];
                var nodes = [];

                data.observations.map(function (d) {
                    values.push(d.value);
                    nodes.push({
                        xLevel: xLevels[d.x],
                        yLevel: yLevels[d.y],
                        value: d.value,
                        value1: d.value1
                    })
                })

                chart.height = yLevels.length * chart.levelHeight;
                height = chart.height + margin.top + margin.bottom;

                var valueDomain = d3.extent([-60, 60]);
                // console.log(values);
                var x = getXScale(xLevels, valueDomain);//.domain([-5, 5]);
                //console.log(x);

                var y = d3.scale.ordinal()
                    .domain(yLevels)
                    .rangeBands([0, (chart.height)]);
                console.log(y);
                var positiveColors = d3.scale.quantize()
                    .domain([0, valueDomain[1]])
                    .range(['#52beda', '#3691a8', '#004165']);

                var negativeColors = d3.scale.linear()
                    .domain([0, valueDomain[0]])
                    .range(['#fc8e80', '#d53a26', '#FF6308']);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .innerTickSize(-chart.height)
                    .tickPadding(5)
                    .ticks(8)
                    .orient('bottom');

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .innerTickSize(-chart.width)
                    .tickPadding(10)
                    .orient('left');

                /**
                * Cache scales, data, and domains for checkbox callbacks
                **/

                state.scales = {
                    x: x,
                    y: y,
                    color: {
                        positive: positiveColors,
                        negative: negativeColors
                    }
                }

                state.data = {
                    nodes: nodes,
                };

                state.domains = {
                    x: xLevels,
                    values: valueDomain
                }

                /**
                * Transition elems
                **/

                svg.transition()
                    .attr('viewBox', '0 0 ' + width + ' ' + height);

                grid.transition()
                    .attr('width', chart.width)
                    .attr('height', chart.height);

                g.select('.x.axis').transition()
                    .attr('transform', 'translate(0,' + chart.height + ')')
                    .call(xAxis);

                g.select('.y.axis').transition()
                    .call(yAxis);


                g.append("text")
                    .attr("transform", "translate(" + 0 + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "left")
                    .style("font-weight", "bold")
                    .text("Overdue");
                g.append("text")
                    .attr("transform", "translate(" + ((chart.width / 2)) + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "middle")
                    .style("font-weight", "bold")
                    .text("Today");
                g.append("text")
                    .attr("transform", "translate(" + ((chart.width)) + " ," + (height - margin.bottom - 3) + ")")
                    .style("text-anchor", "middle")
                    .style("font-weight", "bold")
                    .text("Due");

                neutral.transition()
                    .attr('x1', x(0))
                    .attr('x2', x(0))
                    .attr('y1', 0)
                    .attr('y2', chart.height - 100);

                // update all points with discretized styles if necessary
                handleDiscretize();

                /**
                * Transition nodes
                **/

                var node = g.selectAll('.node').data(nodes);

                node.exit()
                    .remove();

                var elemEnter = node.enter()
                    .append("g")

                //  .attr("transform", function (d) { return "translate(" + d.x + ",80)" })

                node.enter()
                    .append('circle')
                    .attr('class', 'node')
                    .attr('r', 5)
                    .style('opacity', 0)

                    .on('mousemove', handleMousemove)
                    .on('mouseout', handleMouseout)



                elemEnter.append("text")
                    //.attr("dy", -10)
                    //.attr("dx", 28)
                    .attr('dx', 10)
                    .attr('dy', getYPosition)
                    .text(function (d) {
                        return d.value1;
                    })


                node.transition()
                    .delay(function (d, i) { return i * 3 })
                    .duration(500)
                    .attr('cx', getXPosition)
                    .attr('cy', getYPosition)
                    .attr('stroke', getStroke)
                    .attr('fill', getFill)
                    .style('opacity', 1)
                    .style('fill', 'white');
            }

            function getXScale(xLevels, valueDomain) {
                return discretize.checked ?
                    d3.scale.ordinal()
                        .domain(xLevels)
                        .rangeBands([0, chart.width])

                    : d3.scale.linear()
                        .domain([valueDomain[0] - 0.015, valueDomain[1] + 0.015])
                        .range([0, chart.width]);
            }

            function getXPosition(d, i) {
                return discretize.checked ?
                    state.scales.x(d.xLevel) + state.scales.x.rangeBand() / 2
                    : state.scales.x(d.value)
            }
            function getYPosition(d, i) {
                var position = state.scales.y(d.yLevel) +
                    state.scales.y.rangeBand() / 2;
                if (jitter.checked) {
                    return i % 2 === 0 ?
                        position + Math.random() * chart.jitterVal
                        : position - Math.random() * chart.jitterVal
                } else {
                    return position;
                }
            }

            function getStroke(d) {
                return d.value >= 0 ?
                    d3.rgb(state.scales.color.positive(d.value)).darker(.5)
                    : d3.rgb(state.scales.color.negative(d.value)).darker(.5)
            }

            function getFill(d) {
                return d.value >= 0 ?
                    d3.rgb(state.scales.color.positive(d.value))
                    : d3.rgb(state.scales.color.negative(d.value))
            }

            function handleDiscretize() {
                state.scales.x = getXScale(state.domains.x, state.domains.values)

                margin.bottom = discretize.checked ? 140 : 30;
                height = chart.height + margin.top + margin.bottom;

                svg.transition()
                    .attr('viewBox', '0 0 ' + width + ' ' + height);

                var xAxis = d3.svg.axis()
                    .scale(state.scales.x)
                    .innerTickSize(-chart.height)
                    .tickPadding(0)
                    .ticks(8)
                    .orient('bottom');

                g.select('.x.axis').transition()
                    .attr('transform', 'translate(0,' + chart.height + ')')
                    .call(xAxis);

                svg.select('.x.axis').selectAll('text').transition()
                    .style('text-anchor', function () {
                        return discretize.checked ? 'start' : 'middle'
                    })
                    .attr('transform', function () {
                        return discretize.checked ? 'rotate(90)' : 'rotate(0)'
                    })
                    .attr('dx', function () {
                        return discretize.checked ? '0.5em' : 0
                    })
                    .attr('dy', function () {
                        return discretize.checked ? -8 : '.71em'
                    })

                d3.selectAll('.node').transition()
                    .delay(function (d, i) { return i * 3 })
                    .attr('cx', getXPosition)

                d3.select('.neutral-line').transition()
                    .style('opacity', function () {
                        return discretize.checked ? 0 : 1
                    });
            }

            var tooltip = d3.select(chart.tooltip);
            function handleMousemove(d, i) {
                var mousePosition = d3.mouse(this);
                debugger;
                tooltip
                    .style('left', mousePosition[0] - 50 + 'px')
                    .style('top', mousePosition[1] - 80 + 'px')
                    .style('opacity', 1)
                    .style('z-index', 10);

                tooltip.select('.x-level').html(d.xLevel)
                tooltip.select('.value').style('color', function () {
                    return d.value >= 0 ?
                        state.scales.color.positive(d.value)
                        : state.scales.color.negative(d.value)
                })
            }

            function handleMouseout(d, i) {
                tooltip
                    .style('opacity', 0)
                    .style('z-index', -1)

                d3.select(chart.container).selectAll('.circle')
                    .style('opacity', 1)
            }

            var container = document.querySelector('#discrete-scatterplot-selects'),
                jitter = container.querySelector('#jitterbug-perfume'),
                discretize = container.querySelector('#discretize'),
                selects = container.querySelectorAll('select');

            for (var i = 0; i < selects.length; i++) {
                var elem = selects[i];
                elem.addEventListener('change', redraw);
            }

            function getDatafilePath() {
                return chart.data;
            }

            function redraw() {
                d3.json(getDatafilePath(chart.data), updateChart(chart.data));
            };

            // initialize the chart
            redraw();

>>>>>>> b88beb5503bcf0da756b1350c2310719805836b0
        };//)();

        function createTimelineChart(data) {
            var YaxisLables = [];

            var margin = { top: 10, right: 10, bottom: 30, left: 30 },
                width = 500 - margin.left - margin.right,
                height = 300 - margin.top - margin.bottom;

            var chartData = [
                { name: "AB", group: "A", number: 0.5 },
                { name: "ABC", group: "A", number: 10.0 },
                { name: "BC", group: "B", number: 3.0 },
                { name: "BCD", group: "B", number: 5.0 },
                { name: "BCDE", group: "B", number: 0.3 },
                { name: "CD", group: "C", number: 1.6 },
                { name: "DE", group: "D", number: 1.5 }
            ];

            var lkUp = {};
            chartData.forEach(function (d) { lkUp[d.name] = { group: d.group }; });

            var x = d3.scale.ordinal()
                .domain(chartData.map(function (d) { return d.name; }))
                .rangePoints([0, width]),
                y = d3.scale.linear()
                    .domain([0, d3.max(chartData, function (d) { return d.number; })])
                    .range([height, 0]),
                xAxis = d3.svg.axis()
                    .scale(x)
                    .tickFormat(function (d) { return lkUp[d].group; })
                    .orient("bottom"),
                yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left");

            var svg = d3.select(".mkk")
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            svg.selectAll("circle.point")
                .data(chartData)
                .enter().append("circle")
                .attr({
                    "class": "point",
                    cx: function (d) { return y(d.number); },
                    cy: function (d) { return x(d.name); },
                    r: 5
                });

            svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "x axis")
                .call(yAxis);

            //var XaxisValues = [5, 10, 15, 20, 25, 30, -5, -10, -15, -20, -25, -30];

            //data.forEach(x => {
            //    YaxisLables.push(x[1]);
            //});

            //var margin = { top: 20, right: 15, bottom: 60, left: 60 }
            //    , width = 960 - margin.left - margin.right
            //    , height = 500 - margin.top - margin.bottom;

            ////var x = d3.scale.linear()
            ////    .domain([-d3.max(data, function (d) { return d[0]; }), d3.max(data, function (d) { return d[0]; })])
            ////    .range([0, width]);


            //var x = d3.scale.linear()
            //    .domain([-30, 30])
            //    .range([0, width]);

            //var y = d3.scale.linear()
            //    .domain([0, YaxisLables.length])
            //    .range([height, 0]);    

            //$(".mkk").html = "";

            //var chart = d3.select('.mkk')
            //    .append('svg:svg')
            //    .attr('width', width + margin.right + margin.left)
            //    .attr('height', height + margin.top + margin.bottom)
            //    .attr('class', 'chart');

            //var main = chart.append('g')
            //    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
            //    .attr('width', width)
            //    .attr('height', height)
            //    .attr('class', 'main');

            //// draw the x axis
            //var xAxis = d3.svg.axis()
            //    .scale(x)
            //    .orient('bottom');

            //main.append('g')
            //    .attr('transform', 'translate(0,' + height + ')')
            //    .attr('class', 'main axis date')
            //    .call(xAxis);

            //// draw the y axis
            //var yAxis = d3.svg.axis()
            //    .scale(y)
            //    .orient('left');

            //main.append('g')
            //    .attr('transform', 'translate(0,0)')
            //    .attr('class', 'main axis date grid')
            //    .call(yAxis.tickSize(-width, 0, 0)
            //        .tickFormat(function (d, i) { return YaxisLables[d]}));

            //var g = main.append("svg:g");

            //g.selectAll("scatter-dots")
            //    .data(data)
            //    .enter().append("svg:circle")
            //    .attr("cx", function (d, i) { return x(d[0]); })
            //    .attr("cy", function (d) { return y(d[1]); })
            //    .attr("r", 4)
            //    .style("stroke-width", 3)
            //    .attr('fill', 'white')
            //    .attr('stroke', 'black');
        }
<<<<<<< HEAD
        
=======

>>>>>>> b88beb5503bcf0da756b1350c2310719805836b0

    }]);