﻿'use strict';

/* Controllers */

angular.module('PDPTeamAct.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

    .controller('PDPTeamActController', ['$scope', 'dataFactory', '$http', '$rootScope', '$timeout', function ($scope, dataFactory, $http, $rootScope, $timeout) {

        var token = ""; var tabledata = []; var arr = []; var objects = []; var counter = 0; $scope.notstarted = [];
        $scope.formInfo = {};


        token = $rootScope.token;
        //$('#mydiv').hide(); 
        dataFactory.goalAct(token)
            .then(function (response) {
                var data = response.data;
                localStorage.setItem("PDPdata", JSON.stringify(response.data));

                $rootScope.$on("searchevent", function (event, message) {
                    $scope.formInfo = message;
                    var data1 = filter(data, $scope.formInfo.Loc, $scope.formInfo.Manager, $scope.formInfo.Dept, $scope.formInfo.Role,
                        $scope.formInfo.Aspr, $scope.formInfo.Username); //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    //tabledata = [];
                    //$.each(data1, function (index, value) {
                    //    tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, Math.round((value.areasModeratedNum / (value.totalModerated == 0 ? 1 : value.totalModerated)) * 100) + '%']);
                    //});
                    //addTable(tabledata);
                    //$("#myTable").tablesorter({
                    //    theme: 'blue',
                    //    showProcessing: true
                    //});

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    createProActChart(data1);
                    /*** Re-draw chart ***/


                });

                $rootScope.$on("clearevent", function (event, message) {
                    $scope.formInfo = message;
                    var data1 = filter(data, $scope.formInfo.Loc, $scope.formInfo.Manager, $scope.formInfo.Dept, $scope.formInfo.Role,
                        $scope.formInfo.Aspr, $scope.formInfo.Username); //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    data1 = (data1.length == 0 ? data : data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    //tabledata = [];

                    //$.each(data1, function (index, value) {
                    //    tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, Math.round((value.areasModeratedNum / (value.totalModerated == 0 ? 1 : value.totalModerated)) * 100) + '%']);
                    //});
                    //addTable(tabledata);
                    //$("#myTable").tablesorter({
                    //    theme: 'blue',
                    //    showProcessing: true
                    //});

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    createProActChart(data1);
                    /*** Re-draw chart ***/


                });

                function filter(data1, location, manager, dept, role, aspiration, username, frmdate, todate) {
                    var result = [];
                    if (location)
                        result = data1.filter(function (item) { return item.location == location });
                    if (manager)
                        result = data1.filter(function (item) { return item.managerName == manager });
                    if (dept)
                        result = data1.filter(function (item) { return item.department == dept });
                    if (role)
                        result = data1.filter(function (item) { return item.roleName == role });
                    if (aspiration)
                        result = data1.filter(function (item) { return item.roleName == aspiration });
                    if (username)
                        result = data1.filter(function (item) { return item.name == username });
                    //if (frmdate)
                    //    result = data1.filter(function (item) { return item.roleName == frmdate });
                    //if (todate)
                    //    result = data1.filter(function (item) { return item.roleName == todate });

                    return result;
                }

                $('#mydiv').hide();

                var data = appendData(counter, 0);
                counter = counter + 10;

                $.each(data, function (index, value) {
                    tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.PerGoals),
                        value.teamPercentComplete, value.teamGoals, value.teamGoalsCompleted, (value.PerTeamGoals), value.comparisonExpert,
                        ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
                    ]);
                });
                
                addTable(tabledata);
                addanothertbl(tabledata);
                $("#myTable").tablesorter({
                    theme: 'blue',
                    showProcessing: true,
                    widgets: ["zebra"],
                    widgetOptions: {
                        zebra: ["even", "odd"]
                    }
                }).find('tr:nth-child(even)')
                    .css('background-color', 'white')
                    .end()
                    .find('tr:nth-child(odd)')
                    .css('background-color', 'rgb(212,227,232)')
                    .end();

                //var Languages = [
                //    {
                //        "name": "HTML5/CSS3",
                //        "Vote": 65,
                //        "rating": 2
                //    },
                //    {
                //        "name": "JavaScript",
                //        "Vote": 60,
                //        "rating": 3
                //    },
                //    {
                //        "name": "Java",
                //        "Vote": 60,
                //        "rating": 4
                //    },
                //    {
                //        "name": "C#",
                //        "Vote": 80,
                //        "rating": 1
                //    },
                //    {
                //        "name": "SQL",
                //        "Vote": 55,
                //        "rating": 5
                //    },
                //];  

                var data = [
                    {
                        "userID": 3,
                        "managerID": null,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, Manager2P",
                        "userGoals": 1,
                        "userCompleted": 0,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 2,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 7,
                        "managerID": 2,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, User3I",
                        "userGoals": 3,
                        "userCompleted": 2,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 4,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 9,
                        "managerID": 2,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, User5I",
                        "userGoals": 3,
                        "userCompleted": 1,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 6,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 2,
                        "managerID": 4,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, Manager1I",
                        "userGoals": 1,
                        "userCompleted": 5,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 8,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 15,
                        "managerID": 4,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Man, Super",
                        "userGoals": 8,
                        "userCompleted": 9,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 12,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 17,
                        "managerID": 3,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Man, Bat",
                        "userGoals": 8,
                        "userCompleted": 7,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 16,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 1,
                        "managerID": 3,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, Manager1P",
                        "userGoals": 1,
                        "userCompleted": 12,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 10,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 20,
                        "managerID": null,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, TestManager1P@pobiltd.com",
                        "userGoals": 2,
                        "userCompleted": 14,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 18,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 10,
                        "managerID": 1,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, NewUser1P",
                        "userGoals": 1,
                        "userCompleted": 10,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 14,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 54,
                        "managerID": 1,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Knobloch, Sarah",
                        "userGoals": 1,
                        "userCompleted": 16,
                        "userPercentComplete": 1,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 24,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 57,
                        "managerID": 54,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "SK, User1",
                        "userGoals": 1,
                        "userCompleted": 18,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 20,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 61,
                        "managerID": 54,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "SK, User3",
                        "userGoals": 1,
                        "userCompleted": 20,
                        "userPercentComplete": 1,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 26,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 19,
                        "managerID": 2,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Shannon, Paul",
                        "userGoals": 2,
                        "userCompleted": 2,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 22,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 40,
                        "managerID": 20,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, TestUser19P@pobiltd.com",
                        "userGoals": 1,
                        "userCompleted": 3,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 35,
                        "teamPercentComplete": 0.0571428571428571
                    },
                    {
                        "userID": 41,
                        "managerID": 20,
                        "deptID": 1,
                        "deptName": "HR",
                        "userName": "Surname, TestUser20P@pobiltd.com",
                        "userGoals": 1,
                        "userCompleted": 11,
                        "userPercentComplete": 0,
                        "teamGoals": 35,
                        "teamGoalsCompleted": 30,
                        "teamPercentComplete": 0.0571428571428571
                    }
                ];

                //showScatterPlot(data);//Languages
                createChart(data);


            }, function (error) {
                alert('PDPActivity error');
                $("#mydiv").hide();
            });



        //}, function (error) {
        //    alert('Calling error');
        //}),3000);


        function addTable(data) {
            var myTableDiv = document.getElementById("PDPActTbl")
            //myTableDiv.innerHTML = "";
            if (data.length > 0) {
                $('#myTable').remove();
                var table = document.createElement('TABLE')
                var tableheader = document.createElement('THEAD')
                var tableBody = document.createElement('TBODY')
                var tablefoot = document.createElement('TFOOT')

                table.border = '1'
                table.className = "tablesorter";
                table.id = "myTable";
                table.appendChild(tableheader);
                table.appendChild(tableBody);

                var heading = new Array();
                heading[0] = "Employee Name"
                heading[1] = "Expertise completion %"
                heading[2] = "# Goals Set"
                heading[3] = "# Goals Signed off"
                heading[4] = "% Goals Signed off"
                heading[5] = "Manager Expertise Completion %"
                heading[6] = "# Avg. Goals across team"
                heading[7] = "# Avg. Goals signed off"
                heading[8] = "% Team Goals signed off"
                heading[9] = "Comparison Expertise"
                heading[10] = "Comparison Goal vs Goal"

                var Hover = new Array();
                Hover[0] = "Employee Name"
                Hover[1] = "Expertise completion %"
                Hover[2] = "# Goals Set"
                Hover[3] = "# Goals Signed off"
                Hover[4] = "% Goals Signed off"
                Hover[5] = "Manager Expertise Completion %"
                Hover[6] = "# Avg. Goals across team"
                Hover[7] = "# Avg. Goals signed off"
                Hover[8] = "% Team Goals signed off"
                Hover[9] = "Comparison Expertise"
                Hover[10] = "Comparison Goal vs Goal"


                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tableheader.appendChild(tr);
                for (var i = 0; i < heading.length; i++) {
                    var th = document.createElement('TH')
                    //th.width = '75';
                    th.setAttribute('data-title', Hover[i]);
                    th.appendChild(document.createTextNode(heading[i]));
                    tr.appendChild(th);
                }
                table.appendChild(tableBody);
                //TABLE ROWS
                for (var i = 0; i < data.length; i++) {
                    tr = document.createElement('TR');
                    for (var j = 0; j < data[i].length; j++) {
                        var td = document.createElement('TD')
                        if (j == 9 || j == 10) {
                            if (data[i][j] < 0) {
                                td.className = "fa fa-arrow-down";
                            }
                            else if (data[i][j] > 0) {
                                td.className = "fa fa-arrow-up";
                            }
                            else if (data[i][j] == 0) {
                                td.className = "fa fa-equals";
                            }
                        }
                        else {
                            td.appendChild(document.createTextNode(data[i][j]));
                        }
                        
                        tr.appendChild(td);
                    }
                    tableBody.appendChild(tr);
                }
                //TABLE FOOTER
                table.appendChild(tablefoot);
                var footer = new Array();
                footer[0] = ""
                footer[1] = "Total:"
                footer[2] = "Total:"
                footer[3] = "Total:"
                footer[4] = "Total:"
                footer[5] = "Total:"
                footer[6] = "Total:"
                footer[7] = "Total:"
                footer[8] = "Total:"
                footer[9] = ""
                footer[10] = ""

                //TABLE COLUMNS
                tr = document.createElement('TR');
                tr.className = "grandtotal";
                tablefoot.appendChild(tr);
                for (var i = 0; i < footer.length; i++) {
                    var td = document.createElement('TD');
                    td.className = "total";
                    td.appendChild(document.createTextNode(footer[i]));
                    tr.appendChild(td);
                }

                //myTableDiv.appendChild(table)
                myTableDiv.insertBefore(table, myTableDiv.childNodes[0]);

                $('#myTable thead th').each(function (i) {
                    calculateColumn(i);
                });
            }
            else {
                myTableDiv.innerHTML = "No data";
            }

            function calculateColumn(index) {
                var total = 0;
                $('#myTable tbody tr').each(function () {

                    var value = parseFloat($('td', this).eq(index).text());
                    if (!isNaN(value)) {
                        total += value;
                    }
                });
                if (index != 0) {
                    if (index == 1 || index == 4 || index == 5 ||  index == 8) {
                        $('#myTable tfoot .grandtotal td').eq(index).text(parseInt(total).toFixed(1) + '%');
                    }
                    else if (index == 9 || index == 10) {
                        $('#myTable tfoot .grandtotal td').eq(index).text("").addClass("darkgrey");
                        //$('#myTable tfoot .grandtotal td').eq(index).addClass("darkgrey");//css("background-color", "rgb(99, 94, 94)");
                        // $scope.Col3 = parseInt(total) + '%';
                    }
                    else {
                        $('#myTable tfoot .grandtotal td').eq(index).text(parseInt(total));
                        //$scope.Col1 = parseInt(total);
                        //$scope.Col2 = parseInt(total);
                    }

                }
                else {
                    $('#myTable tfoot .grandtotal td').eq(index).text("Summary of Table");
                    $('#myTable tfoot .grandtotal td').eq(index).css('font-weight', 'bold');
                }
            }

        }

        function appendData(counter, flag) {
            var html = '';
            arr = [];
            var data = JSON.parse(localStorage.getItem("PDPdata"));
            var length = Object.keys(data).length;

            var j = 0;
            if (flag == 0) j = counter + 10;
            else if (flag == 2) { j = length; counter = 0; }
            else j = length;


            var table = $("<table class='table-striped'>");
            if (counter == 0) {
                var tr = $('<tr>');
                $('<th>').html("Employee Name").appendTo(tr);
                $('<th>').html("Expertise completion %").appendTo(tr);
                $('<th>').html("# Goals Set").appendTo(tr);
                $('<th>').html("# Goals Signed off").appendTo(tr);
                $('<th>').html("% Goals Signed off").appendTo(tr);
                $('<th>').html("Manager Expertise Completion %").appendTo(tr);
                $('<th>').html("# Avg. Goals across team").appendTo(tr);
                $('<th>').html("# Avg. Goals signed off").appendTo(tr);
                $('<th>').html("% Team Goals signed off").appendTo(tr);
                $('<th>').html("Comparison Expertise").appendTo(tr);
                $('<th>').html("Comparison Goal vs Goal").appendTo(tr);
                $('.table-striped').append(tr);
            }

            //$.each(data, function (index, value) {
            //    tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.userCompleted / value.userGoals),
            //    (value.teamPercentComplete).toFixed(1), value.teamGoals, value.teamGoalsCompleted, (value.teamGoalsCompleted / value.teamGoals).toFixed(1), (value.userPercentComplete - value.teamPercentComplete),
            //    ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
            //    ]);
            //});

            for (var k = 0; k < j && k < length; k++) {

                var tr = $('<tr>');
                var userName = data[k].userName;
                var userPercentComplete = data[k].userPercentComplete.toFixed(1) + '%';
                var userGoals = data[k].userGoals;
                var userCompleted = data[k].userCompleted;
                var PerGoals = (data[k].userCompleted / data[k].userGoals).toFixed(1) + '%';
                var teamPercentComplete = (data[k].teamPercentComplete).toFixed(1) + '%';
                var teamGoals = data[k].teamGoals;
                var teamGoalsCompleted = data[k].teamGoalsCompleted;
                var PerTeamGoals = (data[k].teamGoalsCompleted / data[k].teamGoals).toFixed(1) + '%';
                var comparisonExpert = (data[k].userPercentComplete - data[k].teamPercentComplete);
                var compareGoal = ((data[k].userCompleted / data[k].userGoals) - (data[k].teamGoalsCompleted / data[k].teamGoals));


                $('<td>').html(userName).appendTo(tr);
                $('<td>').html(userPercentComplete).appendTo(tr);
                $('<td>').html(userGoals).appendTo(tr);
                $('<td>').html(userCompleted).appendTo(tr);
                $('<td>').html(PerGoals).appendTo(tr);
                $('<td>').html(teamPercentComplete).appendTo(tr);
                $('<td>').html(teamGoals).appendTo(tr);
                $('<td>').html(teamGoalsCompleted).appendTo(tr);
                $('<td>').html(PerTeamGoals).appendTo(tr);
                $('<td>').html(comparisonExpert).appendTo(tr);
                $('<td>').html(compareGoal).appendTo(tr);
                $('.table-striped').append(tr);
                if (counter >= length) $('#btnSubmit').val("No more data");

                arr.push({
                    'userName': data[k].userName, 'userPercentComplete': data[k].userPercentComplete.toFixed(1) + "%", 'userGoals': data[k].userGoals,
                    'userCompleted': data[k].userCompleted, 'PerGoals': (data[k].userCompleted / data[k].userGoals).toFixed(1) + "%",
                    'teamPercentComplete': (data[k].teamPercentComplete).toFixed(1) + "%", 'teamGoals': data[k].teamGoals, 'teamGoalsCompleted': data[k].teamGoalsCompleted,
                    'PerTeamGoals': (data[k].teamGoalsCompleted / data[k].teamGoals).toFixed(1) + "%", 'comparisonExpert': (data[k].userPercentComplete - data[k].teamPercentComplete),
                    'compareGoal': ((data[k].userCompleted / data[k].userGoals) - (data[k].teamGoalsCompleted / data[k].teamGoals))
                });

            }
            // counter++;
            console.log(arr);
            return arr;
        }
        
        function addanothertbl(data) {
            $('.tableparent').empty();
            var table = $('.tableparent')
            var tr = $('<tr>');

            $('<th  rowspan="2" colspan="0" data-title="Overall Summary" class="first-child">').html("Overall Summary").appendTo(tr);

            $('<th data-title="Expertise completion %" style="width:9%;">').html("Expertise completion %").appendTo(tr);
            $('<th data-title="# Goals Set" style="width:9%;">').html("# Goals Set").appendTo(tr);
            $('<th data-title="# Goals Signed off" style="width:9%;">').html("# Goals Signed off").appendTo(tr);
            $('<th data-title="% Goals Signed off" style="width:9%;">').html("% Goals Signed off").appendTo(tr);
            $('<th data-title="Manager Expertise Completion %" style="width:9%;">').html("Manager Expertise Completion %").appendTo(tr);
            $('<th data-title="# Avg. Goals across team" style="width:9%;">').html("# Avg. Goals across team").appendTo(tr);
            $('<th data-title="# Avg. Goals signed off" style="width:9%;">').html("# Avg. Goals signed off").appendTo(tr);
            $('<th data-title="% Team Goals signed off" style="width:9%;">').html("% Team Goals signed off").appendTo(tr);
            $('<th data-title="Comparison Expertise">').html("Comparison Expertise").appendTo(tr);
            $('<th data-title="Comparison Goal vs Goal">').html("Comparison Goal vs Goal").appendTo(tr);
            table.append(tr);

            var a = 0; var b = 0; var c = 0; var d = 0; var e = 0; var f = 0; var g = 0; var h = 0;
            $.each(data, function (i, item) {

                a += parseFloat(item[1].replace('%', ''))
                b += item[2]
                c += item[3]
                d += parseFloat(item[4].replace('%', ''))
                e += parseInt(item[5])
                f += item[6]
                g += item[7]
                h += parseInt(item[8])
            });
            tr = $('<tr class="number" style="background-color: rgb(212, 227, 232);">');
            

            $('<td>').html(a.toFixed(1) + '%').appendTo(tr);
            $('<td>').html(b).appendTo(tr);
            $('<td>').html(c).appendTo(tr);
            $('<td>').html(d.toFixed(1) + '%').appendTo(tr);
            $('<td>').html(e.toFixed(1) + '%').appendTo(tr);
            $('<td>').html(f).appendTo(tr);
            $('<td>').html(g).appendTo(tr);
            $('<td>').html(h.toFixed(1) + '%').appendTo(tr);
            $('<td>').html("").addClass("darkgrey").appendTo(tr);
            $('<td>').html("").addClass("darkgrey").appendTo(tr);
            table.append(tr);
        }

        $("#btnSubmit").button().click(function () {
            debugger;
            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 10;
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.PerGoals),
                value.teamPercentComplete, value.teamGoals, value.teamGoalsCompleted, (value.PerTeamGoals), value.comparisonExpert,
                ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
                ]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
           // showScatterPlot(Chartdata);
        });
        $("#btnAll").button().click(function () {
            debugger;
            tabledata = [];
            counter = 0;
            var data = appendData(counter, 1);
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.PerGoals),
                value.teamPercentComplete, value.teamGoals, value.teamGoalsCompleted, (value.PerTeamGoals), value.comparisonExpert,
                ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
                ]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
            //showScatterPlot(Chartdata);

            $("#btnLess").show();
            $("#btnAll").hide();
        });

        $("#btnLess").button().click(function () {
            $("#btnLess").hide();
            $("#btnAll").show();

            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 10;
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.PerGoals),
                value.teamPercentComplete, value.teamGoals, value.teamGoalsCompleted, (value.PerTeamGoals), value.comparisonExpert,
                ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
                ]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
            //showScatterPlot(Chartdata);

        });

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#myTable th');
            $('#myTable tbody tr').each(function (i, tr) {
                var obj = {},

                    $tds = $(tr).find('td');

                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }

        function showScatterPlot(data) {
            // Spacing Around   
            var margins =
            {
                "left": 30,
                "right": 30,
                "top": 30,
                "bottom": 30
            };

            var width = 700;
            var height = 500;

            // This will be our colour scale. An Ordinal scale.  
            var colors = d3.scale.category10();

            var config = {
                "avatar_size": 25
            }

         

            // Adding the SVG component to the scatter-load div  
            var svg = d3.select("#scatter-load").append("svg").attr("width", width).attr("height", 600).append("g")
                .attr("transform", "translate(" + margins.left + "," + margins.top + ")");

            var defs = svg.append('svg:defs');

            svg.append('clipPath')
                .attr('id', 'clipObj')
                .append('circle')
                .attr('cx', config.avatar_size / 2)
                .attr('cy', config.avatar_size / 2)
                .attr('r', config.avatar_size / 2);

            // Setting the scale that we're using for the X axis.   
            // Domain define the min and max variables to show. In this case, it's the min and max Votes of items.  
            // this is made a compact piece of code due to d3.extent which gives back the max and min of the Vote variable within the dataset  
            debugger;
            //var x = d3.scale.linear()
            //    .domain(d3.extent(data, function (d) {
            //        return d.userCompleted;
            //    }))

            //    // Range maps the domain to values from 0 to the width minus the left and right margins (used to space out the visualization)  
            //    .range([0, width - margins.left - margins.right]);

            var x = d3.scale.linear()
                .domain([
                    d3.min([0, d3.min(data, function (d) { return d.userCompleted })]),
                    d3.max([0, d3.max(data, function (d) { return d.userCompleted })])
                ])
                .range([0, width])

            

            //// Scalling for the y axis but maps from the rating variable to the height to 0.   
            //var y = d3.scale.linear()
            //    .domain(d3.extent(data, function (d) {
            //        return d.teamGoalsCompleted;
            //    }))

            //    // Note that height goes first due to the weird SVG coordinate system  
            //    .range([height - margins.top - margins.bottom, 0]);
            var y = d3.scale.linear()
                .domain([
                    d3.min([0, d3.min(data, function (d) { return d.teamGoalsCompleted })]),
                    d3.max([0, d3.max(data, function (d) { return d.teamGoalsCompleted })])
                ])
                .range([height,0])

           

            // Adding the axes SVG component. At this point, this is just a placeholder. The actual axis will be added in a bit  
            svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + y.range()[0] + ")");
            svg.append("g").attr("class", "y axis");
            // X axis label. Nothing too special to see here.  
            svg.append("text")
                .attr("text-anchor", "middle")
                .style("font-weight", "bold")
                .attr("x", 210)//width / 2
                .attr("y", height - 15)
                .text("Goals#");

            svg.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", -62)//0 - margin.left)
                .attr("x", 0 - (height / 2))
                .attr("dy", "1em")
                .style("text-anchor", "middle")
                .style("font-weight", "bold")
                .text("Assessment (%)");


            // Actual definition of our x and y axes. The orientation refers to where the labels appear - for the x axis, below or above the line, and for the y axis, left or right of the line. Tick padding refers to how much space between the tick and the label. There are other parameters too - see https://github.com/mbostock/d3/wiki/SVG-Axes for more information  
            //var xAxis = d3.svg.axis().scale(x).orient("bottom").tickFormat(function (d, i) {
            //    return i == 0 ? "0%" : (i == 10 ? "100%" : "");
            //});
            //var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(function (d, i) {
            //    return i == 0 ? "0%" : (i == 6 ? "100%" : "");
            //});

            var xAxis = d3.svg.axis().scale(x).orient("bottom");
            var yAxis = d3.svg.axis().scale(y).orient("left");

            // Selecting the axis we created a few lines earlier. See how we select the axis item. in our svg we appended a g element with a x/y and axis class. To pull that back up, we do this svg select, then 'call' the appropriate axis object for rendering.   
            svg.selectAll("g.y.axis").call(yAxis);
            svg.selectAll("g.x.axis").call(xAxis);

            // Now, we can get down to the data part, and drawing stuff. We are telling D3 that all nodes (g elements with class node) will have data attached to them. The 'key' we use (to let D3 know the uniqueness of items) will be the name. Not usually a great key, but fine for this example.  
            var chocolate = svg.selectAll("g.node").data(data, function (d) {
                if (x(d.userCompleted) > 0 && y(d.teamGoalsCompleted) > 0) {
                    return d.userName;
                }
                
            });

            // We 'enter' the data, making the SVG group (to contain a circle and text) with a class node. This corresponds with what we told the data it should be above.  

            var chocolateGroup = chocolate.enter().append("g").attr("class", "node")

                // this is how we set the position of the items. Translate is an incredibly useful function for rotating and positioning items   
                .attr('transform', function (d) {
                   
                    if (x(d.userCompleted) > 0 && y(d.teamGoalsCompleted) > 0) {
                        return "translate(" + x(d.userCompleted) + "," + y(d.teamGoalsCompleted) + ")";
                    }
                    else {
                        //$.each(data, function (index, value) {
                        //    tabledata.push([value.userName, value.userPercentComplete, value.userGoals, value.userCompleted, (value.userCompleted / value.userGoals),
                        //    value.teamPercentComplete, value.teamGoals, value.teamGoalsCompleted, (value.teamGoalsCompleted / value.teamGoals).toFixed(1), (value.userPercentComplete - value.teamPercentComplete),
                        //    ((value.userCompleted / value.userGoals) - (value.teamGoalsCompleted / value.teamGoals))
                        //    ]);
                        //});
                       // $scope.notstarted.push(d.userName);
                    }
                   // return "translate(" + x(d.userCompleted) + "," + y(d.teamGoalsCompleted) + ")";
                });
           // console.log($scope.notstarted);

            svg.selectAll(".point")
                .data(data)
                .enter().append("image")//path

                .attr("xlink:href", function (d) { return "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID; })

                
                .attr("width", config.avatar_size)
                .attr("height", config.avatar_size)
                .attr('transform', function (d) {

                    if (x(d.userCompleted) > 0 && y(d.teamGoalsCompleted) > 0) {
                        return "translate(" + x(d.userCompleted) + "," + y(d.teamGoalsCompleted) + ")";
                    }
                    else {
                        //var data = data.map(function (item) { return { 'Name': item.name, 'Steps ': item.stepsAssessedPCT, 'Area': item.areasAssessedPCT, 'Competencies': item.compsAssessedPCT } });
                        $scope.notstarted.push({ 'Name': d.userName, 'Url': "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID});
                        //$scope.notstarted.push(d.userName);
                    }
                    // return "translate(" + x(d.userCompleted) + "," + y(d.teamGoalsCompleted) + ")";
                })
                .attr('clip-path', 'url(#clipObj)');
            console.log($scope.notstarted);
            
            // Adding our first graphics element! A circle!   
            //chocolateGroup.append("circle")
            //    .attr("r", 10)
            //    .attr("class", "dot")
                //.attr("class", "logo")
                //.attr("cx", 225)
                //.attr("cy", 225)
                //.attr("r", 20)
                //.style("fill", "transparent")
                //.style("stroke", "black")
                //.style("stroke-width", 0.25)
                //.style("fill", "url(#image)");


                //.style("fill", function (d) {
                //    // remember the ordinal scales?   
                //    // We use the colors scale to get a colour for our votes. Now each node will be coloured  
                //    // by votes to the languages.   
                //    return colors(d.manufacturer);
                //});

            // Adding some text, so we can see what each item is.  
            chocolateGroup.append("text")
                .style("text-anchor", "middle")
                .style("font-size","10px")
                .attr("dy", -10)
                .attr("dx",28)
                .text(function (d) {
                    // this shouldn't be a surprising statement.  
                    return d.userName;
                });
        }

        function createChart(data) {
            data.forEach(function (d) { // convert strings to numbers
                d.userCompleted = +d.userCompleted;
                d.teamGoalsCompleted = +d.teamGoalsCompleted;
            });
            makeVis(data);
            
            function makeVis(data) {
                d3.selectAll("#scatter-load > svg").remove();
                // Common pattern for defining vis size and margins
                var margin = { top: 20, right: 40, bottom: 30, left: 50 },
                    width = 960 - margin.left - margin.right,
                    height = 500 - margin.top - margin.bottom;

                // Add the visualization svg canvas to the vis-container <div>
                var canvas = d3.select("#scatter-load").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", 550)//height + margin.top + margin.bottom
                    .attr("style", "margin-left:" + (-100) + "px")
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                var config = {
                    "avatar_size": 25
                }

                var defs = canvas.append('svg:defs');

                // Define our scales
                var colorScale = d3.scale.category10();

                var xScale = d3.scale.linear()
                    .domain([d3.min(data, function (d) { return d.userCompleted; }) - 1,
                    d3.max(data, function (d) { return d.userCompleted; }) + 1])
                    .range([0, width]).nice();

                var yScale = d3.scale.linear()
                    .domain([d3.min(data, function (d) { return d.teamGoalsCompleted; }) - 1,
                    d3.max(data, function (d) { return d.teamGoalsCompleted; }) + 1])
                    .range([height, 0]).nice(); // flip order because y-axis origin is upper LEFT

                // Define our axes
                var len = xScale.ticks().length;
                var xAxis = d3.svg.axis()
                    .scale(xScale)
                    .orient('bottom').tickFormat(function (d, i) {
                        return i == 0 ? "0%" : (i == (len - 1) ? "100%" : "");
                    });



                var yAxis = d3.svg.axis()
                    .scale(yScale)
                    .orient('left').tickFormat(function (d, i) {
                        return i == 0 ? "0%" : (i == 8 ? "100%" : "");
                    });
                debugger;
                // Add x-axis to the canvas
                canvas.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")") // move axis to the bottom of the canvas
                    .call(xAxis)
                    .append("text")
                    .attr("class", "label")
                    .attr("x", width / 2) // x-offset from the xAxis, move label all the way to the right
                    .attr("y", 42)    // y-offset from the xAxis, moves text UPWARD!
                    .style("text-anchor", "end") // right-justify text
                    .text("Goals#");

                // Add y-axis to the canvas
                canvas.append("g")
                    .attr("class", "y axis") // .orient('left') took care of axis positioning for us
                    .call(yAxis)
                    .append("text")
                    .attr("class", "label")
                    .attr("transform", "rotate(-90)") // although axis is rotated, text is not
                    //.attr("y", 15) // y-offset from yAxis, moves text to the RIGHT because it's rotated, and positive y is DOWN
                    .attr("y", -50)//0 - margin.left)
                    .attr("x", 0 - (220)) //height / 2
                    .attr("dy", "1.5em")
                    .style("text-anchor", "end")
                    .text("Assessment (%)");

                canvas.append("text")
                    .attr("x", (width / 2))
                    .attr("y", 0 - ((margin.top / 2) - 2))//0 - ((margin.top / 2) - 1))
                    .attr("text-anchor", "middle")
                    .style("font-size", "16px")
                    .style("text-decoration", "underline")
                    .style("font-weight", "bold")
                    .text("Assessment Completed (%)");

                var gdots = canvas.selectAll("g.dot")
                    .data(data)
                    .enter().append('g');
              

                // Add the tooltip container to the vis container
                // it's invisible and its position/contents are defined during mouseover


                //var tooltip = d3.select("#scatter-load")
                //    .append("div")
                //    .style("position", "absolute")
                //    .style("z-index", "10")
                //    .style("visibility", "hidden")
                //    .text("a simple tooltip");

                $scope.notstarted = [];

                data.forEach(function (d, i) {

                    if ((d.userCompleted) > 0 && (d.teamGoalsCompleted) > 0) {
                        defs.append("svg:pattern")
                            .attr("id", "grump_avatar" + i)
                            .attr("width", config.avatar_size)
                            .attr("height", config.avatar_size)
                            //.attr("patternUnits", "userSpaceOnUse")
                            .append("svg:image")
                            .attr("xlink:href", "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID)
                            .attr("width", 35)
                            .attr("height", 35)
                            .attr("x", 0)
                            .attr("y", 0);
                        // Add data points!
                        var circle = gdots.append("circle")
                            //.attr("transform", "translate(" + d.totalStarted + "," + d.totalAssessed + ")")
                            .attr("r", 15) // radius size, could map to another data dimension
                            .attr("cx", xScale(d.userCompleted))     // x position
                            .attr("cy", yScale(d.teamGoalsCompleted) - 10)  // y position
                            //.style("fill", "#fff")
                            .style("fill", "url(#grump_avatar" + i + ")");
                            //.on("mouseover", function () { return tooltip.style("visibility", "visible"), tooltip.text(d.userName); })
                            //.on("mousemove", function () { return tooltip.style("top", (d3.event.pageY - 221) + "px").style("left", (d3.event.pageX - 15) + "px"); })
                            //.on("mouseout", function () { return tooltip.style("visibility", "hidden"); });

                        gdots.append("text").text(function (d) {
                            if ((d.userCompleted) > 0 && (d.teamGoalsCompleted) > 0) {
                                return d.userName;
                            }
                        })
                            .attr("x", function (d) {
                                return xScale(d.userCompleted)+20;
                            })
                            .attr("y", function (d) {
                                return yScale(d.teamGoalsCompleted);
                            })
                            
                            .attr("font-size", "10.5px")  // Font size
                            .attr("fill", "black");   // Font color;

                    }
                    else {
                        //console.log((d.totalStarted), (d.totalAssessed));
                        $scope.notstarted.push({ 'Name': d.userName, 'Url': "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID });
                        console.log($scope.notstarted);
                    }
                })



            };
        }

    }])