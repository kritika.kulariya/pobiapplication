﻿'use strict';

/* Controllers */

angular.module('progrerssRep.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

    .controller('ProgressController', function ($scope, $http) {
        $scope.Col1 = 0;
        $scope.Col2 = 0;
        $scope.Col3 = 0;
        $scope.Col4 = 0;
        $scope.Col5 = 0;
        var TotalSummaryArea = 0;
        var TotalSummaryComp = 0;
        var counter = 0;
        var arrArea = new Array();
        var token = ""; var arr = []; var objects = []; var tabledata = [];

        var data = $.param({
            username: "insights@pobi.co.uk",
            password: "Xd2actQu74pB$",
            grant_type: "password"
        });
        var config = {
            async: false
        }

        $http.post("https://dev.pobi.co.uk/api/Token", data).then(function (response) {
            //  debugger;
            token = response.data.access_token;
            $http({
                method: 'GET',
                url: 'https://dev.pobi.co.uk/api/api/Insights/Usage/Progress',
                async: false,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }).then(function (response) {
                //debugger;
                var data = response.data;
                $.each(data, function (index, value) {
                    tabledata.push([value.name, value.stepsAssessedPCT == 100 ? "Yes" : "No", value.areasAssessedNum + '/' + value.totalAreas, value.areasAssessedPCT.toFixed(1), value.compsAssessedNum + '/' + value.totalComps, value.compsAssessedPCT.toFixed(1), value.totalWeighted.toFixed(1)]);
                });
                localStorage.setItem("ChartWholeData", JSON.stringify(data));
                localStorage.setItem("TableWholeData", JSON.stringify(tabledata));

                localStorage.setItem("ChartData", JSON.stringify(data));
                localStorage.setItem("TableData", JSON.stringify(tabledata));
                makeTableAndChart();
                });
        });

        function makeTableAndChart() {
            var datachart = addnewTable(counter, 0);
            makeProgressChart(datachart);
            SummaryChart();//Sorting
            $("#myTable").tablesorter({});
        }
        $("#Sortdata").change(function () {
            var SortPara = $("#Sortdata option:selected").text();
            var para = "";
            if (SortPara == "Employee") para = "2";
            if (SortPara == "Role") para = "3";
            if (SortPara == "Manager") para = "4";

            var sorting = [[0, 0], [para, 0]];
            $("#myTable").trigger("sorton", [sorting]);
            return false;
        });

        function sortResults(prop, asc) {
            var SysData = JSON.parse(localStorage.getItem("Filterdata"));
            SysData = SysData.sort(function (a, b) {
                if (asc) {
                    return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                } else {
                    return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                }
            });
            console.log(SysData);
        }

        function addnewTable(counter, flag) {
            var ChartData = JSON.parse(localStorage.getItem("ChartData"));
            var data = JSON.parse(localStorage.getItem("TableData"));
            debugger;
            var myTableDiv = document.getElementById("systemuse");
            if (counter > 0) tablerows(flag);
            else {
                if (data.length > 0) {
                    var table = document.createElement('TABLE')
                    var tableheader = document.createElement('THEAD')
                    var tableBody = document.createElement('TBODY')
                    var tablefooter = document.createElement('TFOOT')

                    table.border = '1'
                    table.className = "tablesorter";
                    table.id = "myTable";
                    table.appendChild(tableheader);
                    tableBody.id="Progresstbody"
                    // table.appendChild(tableBody);
                    var heading = new Array();
                    heading[0] = "Name"
                    heading[1] = "Onboarded"
                    heading[2] = "Areas Assessed"
                    heading[3] = "% Areas Assessed"
                    heading[4] = "Comps Assessed"
                    heading[5] = "% Comps Assessed"
                    heading[6] = "% Framework Complete"


                    var Hover = new Array();
                    Hover[0] = "Name"
                    Hover[1] = "Onboarded"
                    Hover[2] = "Areas Assessed"
                    Hover[3] = "% Areas Assessed"
                    Hover[4] = "Comps Assessed"
                    Hover[5] = "% Comps Assessed"
                    Hover[6] = "% Framework Complete"


                    //TABLE COLUMNS
                    var tr = document.createElement('TR');
                    tableheader.appendChild(tr);
                    for (var i = 0; i < heading.length; i++) {
                        var th = document.createElement('TH')
                        //th.width = '75';
                        // th.id = 'facility_header';
                        th.title = Hover[i];
                        th.className = 'header'
                        //th.setAttribute('onClick', 'sortResults("+ heading[0],"asc");');
                        th.appendChild(document.createTextNode(heading[i]));
                        tr.appendChild(th);
                    }
                    table.appendChild(tableBody);
                    myTableDiv.appendChild(table);
                    //  myTableDiv.appendChild(table)
                }
                else {
                    myTableDiv.innerHTML = "No data";
                }
                tablerows(flag);

                var footer = new Array();
                footer[0] = "Summary of table"
                //footer[1] = ""
                footer[1] = $scope.Col1;
                footer[2] = $scope.Col2;
                footer[3] = $scope.Col3;
                footer[4] = $scope.Col4
                footer[5] = $scope.Col5;

                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tr.className = "grandtotal";
                tablefooter.appendChild(tr);
                for (var i = 0; i < footer.length; i++) {
                    var td = document.createElement('TD');
                    td.className = "total";

                    td.appendChild(document.createTextNode(footer[i]));
                    tr.appendChild(td);
                    if (i == 0) {
                        td.className = "total tablefooter";
                    }
                }
                table.appendChild(tablefooter);
            }
            return arr;
        }

        function tablerows(flag) {
            var table = document.getElementById('myTable')
            var tableBody = document.getElementById('myTable').getElementsByTagName('tbody')[0];

            var ChartData = JSON.parse(localStorage.getItem("ChartData"));
            var TableData = JSON.parse(localStorage.getItem("TableData"));

            // console.log(localStorage.getItem(Filterdata));
            //TABLE ROWS
            var m = 0;
            if (flag == 0) m = counter + 10;
            else if (flag == 1) { m = TableData.length; counter = 0; }
            else m = TableData.length;
            var percent = "";
            for (var i = counter; i < m && i < TableData.length; i++) {
                var tr = document.createElement('TR');
                for (var j = 0; j < TableData[i].length; j++) {
                    var td = document.createElement('TD')
                    if (j == 3 || j == 5 || j == 6) percent = "%";
                    else percent = "";
                    td.appendChild(document.createTextNode((TableData[i][j]) + percent));
                    tr.appendChild(td)
                }

                tableBody.appendChild(tr);
                arr.push({ 'name': ChartData[i].name, 'stepsAssessedPCT': ChartData[i].stepsAssessedPCT, 'areasAssessedPCT': ChartData[i].areasAssessedPCT, 'compsAssessedPCT': ChartData[i].compsAssessedPCT, 'areasAssessedNum': ChartData[i].areasAssessedNum, 'compsAssessedNum': ChartData[i].compsAssessedNum, 'stepsAssessedPCT': ChartData[i].stepsAssessedPCT, 'totalWeighted': ChartData[i].totalWeighted, 'totalAreas': ChartData[i].totalAreas, 'totalComps': ChartData[i].totalComps });
                addanothertbl(arr);
            }
        }
        function addanothertbl(data) {
            //  debugger;
            $('.tableparent').empty();
            var table = $('.tableparent')
            var tr = $('<tr>');
            $('<th  class="first-child" rowspan="2" title="Overall Summary">').html("Overall Summary").appendTo(tr);
            $('<th  class="otherchild" title="1st Accessed">').html("Areas Assessed").appendTo(tr);
            $('<th  class="otherchild1" title="Last Accessed">').html("% Areas Assessed").appendTo(tr);
            $('<th class="otherchild1" title="Total Times Used">').html("Comps Assessed").appendTo(tr);
            $('<th class="" title="Avg. Uses Per Week">').html("% Comps Assessed").appendTo(tr);
            $('<th class="" title="Avg. Session Time">').html("% Framework Complete").appendTo(tr);
            table.append(tr);

            var areasAssessedNum = 0; var totalAreas = 0;
            var areasAssessedPCT = 0; var compsAssessedNum = 0; var totalComps = 0; var compsAssessedPCT = 0;
            var totalWeighted = 0;
            //  debugger;
            $.each(data, function (i, item) {
                // debugger;
                areasAssessedNum += item.areasAssessedNum
                totalAreas += item.totalAreas
                areasAssessedPCT += item.areasAssessedPCT
                compsAssessedNum += item.compsAssessedNum
                totalComps += item.totalComps
                compsAssessedPCT += item.compsAssessedPCT
                totalWeighted += item.totalWeighted
            });

            var A = $scope.Col1 = (areasAssessedNum / data.length).toFixed(0) + '/' + (totalAreas / data.length).toFixed(0);
            var B = $scope.Col2 = (areasAssessedPCT / data.length).toFixed(1) + '%';
            var C = $scope.Col3 = (compsAssessedNum / data.length).toFixed(0) + '/' + (totalComps / data.length).toFixed(0);
            var D = $scope.Col4 = (compsAssessedPCT / data.length).toFixed(1) + '%';
            var E = $scope.Col5 = (totalWeighted / data.length).toFixed(1) + '%';


            $("#myTable tfoot td:eq(1)").text(A);
            $("#myTable tfoot td:eq(2)").text(B);
            $("#myTable tfoot td:eq(3)").text(C);
            $("#myTable tfoot td:eq(4)").text(D);
            $("#myTable tfoot td:eq(5)").text(E);

            var tr = $('<tr>');
            $('<td>').html(A).appendTo(tr);
            $('<td>').html(B).appendTo(tr);
            $('<td>').html(C).appendTo(tr);
            $('<td>').html(D).appendTo(tr);
            $('<td>').html(E).appendTo(tr);
            table.append(tr);
            TotalSummaryArea = Math.round((areasAssessedPCT / data.length).toFixed(1));
            TotalSummaryComp = Math.round((compsAssessedPCT / data.length).toFixed(1));

        }

        function makeProgressChart(data) {

            $(".chart").empty();
            $(".xaxis").empty();

            var EmpName = data.map(function (item) { return item.name });
            var Steps = data.map(function (item) { return item.stepsAssessedPCT });
            var Area = data.map(function (item) { return item.areasAssessedPCT });
            var Comp = data.map(function (item) { return item.compsAssessedPCT });

            var data = {
                labels:
                    EmpName,
                series: [
                    {
                        label: 'Area',
                        values: Area
                    },
                    {
                        label: 'Comp',
                        values: Comp
                    },]
            };

            var chartWidth = 600,
                barHeight = 20,
                groupHeight = barHeight * data.series.length,
                gapBetweenGroups = 20,
                spaceForLabels = 150,
                spaceForLegend = 150;
            var onboardedval = 0;
            // Zip the series data together (first values, second values, etc.)
            var zippedData = [];
            for (var i = 0; i < data.labels.length; i++) {
                for (var j = 0; j < data.series.length; j++) {
                    zippedData.push(data.series[j].values[i]);
                }
            }

            // Color scale
            // var color = d3.scale.category20();
            var color = d3.scale.ordinal().range(["#FF6308", "#004165"]);
            var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;

            var x = d3.scale.linear()
                .domain([0, 100])
                .range([0, chartWidth]);

            var y = d3.scale.linear()
                .range([chartHeight + gapBetweenGroups, 0]);

            var yAxis = d3.svg.axis()
                .scale(y)
                .tickFormat('')
                .tickSize(0)
                .orient("left");

            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function (d, i) {
                    //if (i % data.series.length === 0)
                    //    return "Steps";
                    if (i % data.series.length === 0)
                        return "The development area";
                    if (i % data.series.length === 1)
                        return "The detail that makes up an Area";
                })
            if (chartHeight > 350) { $(".chart").css("overflow-y", "scroll").css("height", "350px"); }
            else { $(".chart").removeAttr("style").css("overflow-y", "scroll")}

            

            // Specify the chart area and dimensions
            var chart = d3.select(".chart").append("svg")
                .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                .attr("height", chartHeight)
                .style("border", "solid 1px #cccccc").style("padding", "1px").style("padding-left", "130px");

            // Create bars
            var bar = chart.selectAll("g")
                .data(zippedData)
                .enter().append("g")
                .attr("transform", function (d, i) {
                    return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i / data.series.length))) + ")";
                });

            chart.call(tip);

            // Create rectangles of the correct width
            bar.append("rect")
                .attr("fill", function (d, i) { return color(i % data.series.length); })
                .attr("class", "bar")
                .attr("width", x)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide)
                .style('stroke', function (d, i) { return "#000000"; })
                .attr("height", barHeight - 1);

            // Add text label in bar
            // debugger;
            bar.append("text")
                .attr("x", function (d, i) { return x(d) - 2; })
                .attr("y", barHeight / 2)
                // .attr("fill", "red")
                .attr("dy", ".35em")
                .text(function (d, i) {
                    return d.toFixed(1) + '%';
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return 110; })
                .attr("y", (barHeight / 2) + 10)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                //.text(function (d) { return "A"; });
                .text(function (d, i) {
                    if (i % 2 == 0) {
                        onboardedval = 0;
                    }
                    onboardedval += d;
                    if (onboardedval == 0) {
                        if (i % 2 == 0) {
                            return "Not Onboarded";
                        }
                    }
                    else {

                    }
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return -5; })
                .attr("y", barHeight / 2)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                //.text(function (d) { return "A"; });
                .text(function (d, i) {
                    //if (i % data.series.length === 0)
                    //    return "Steps";
                    if (i % data.series.length === 0)
                        return "Areas";
                    if (i % data.series.length === 1)
                        return "Competencies";
                });


            // Draw labels
            //var txt = bar.append("text")
            //    .attr("class", "label")
            bar.append("foreignObject").append("xhtml:body")
                .attr("class", "innerNode")
                .style("width", "210px")
                .style("margin-left", "-339px")
                .style("font-weight", "bold")
                .attr("x", function (d) { return - 110; })
                .attr("y", groupHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % data.series.length === 0)
                        return data.labels[Math.floor(i / data.series.length)];
                    // return data.labels[Math.floor(i / data.series.length)];
                    else
                        return ""
                });


            chart.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + spaceForLabels + ", 0)")
                .call(yAxis).selectAll(".tick text")
            //.call(wrap, 40);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(7)

            var xAxisSvg = d3.select('.xaxis').append('svg')
                .attr("width", chartWidth)
                .attr("height", 20)
                .attr("margin-left", "-100px")
            //    .append("g");

            var xAxisDom = xAxisSvg.selectAll('.x.axis')
            if (xAxisDom.empty()) {
                xAxisDom = xAxisSvg.append("g")
                    .attr("class", "x axis")
            }
            xAxisDom
                .attr("transform", "translate(" + (122) + "," + 1 + ")")
                // .call(xAxis);
                .call(d3.svg.axis().scale(x).orient("bottom").tickFormat(function (d, i) {
                    return i == 0 ? "0%" : (i == 10 ? "100%" : "");
                }));
        }

        function SummaryChart() {
            var area = [TotalSummaryArea, 100];
            var Comp = [TotalSummaryComp, 100];
            console.log(area);
            var label = ["Overall Summary"];
            $(".summarychart").empty();
            var data = {
                labels:
                    label,
                series: [
                    {
                        label: 'Area',
                        values: area
                    },
                    {
                        label: 'Comp',
                        values: Comp
                    },]
            };


            var chartWidth = 600,
                barHeight = 20,
                groupHeight = barHeight * data.series.length,
                gapBetweenGroups = 20,
                spaceForLabels = 150,
                spaceForLegend = 150;
            var onboardedval = 0;
            // Zip the series data together (first values, second values, etc.)
            var zippedData = [];
            for (var i = 0; i < 2; i++) {
                for (var j = 0; j < data.series.length; j++) {
                    zippedData.push(data.series[j].values[i]);
                }
            }

            // Color scale
            // var color = d3.scale.category20();
            var color = d3.scale.ordinal().range(["#FF6308", "#004165"]);
            //  var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;
            var chartHeight = 110;

            var x = d3.scale.linear()
                .domain([0, d3.max(zippedData)])
                .range([0, chartWidth]);

            var y = d3.scale.linear()
                .range([80, 0]);

            var yAxis = d3.svg.axis()
                .scale(y)
                .tickFormat('')
                .tickSize(0)
                .orient("left");

            var xAxis = d3.svg.axis()
                .scale(x)
                .tickFormat('')
                .tickSize(0)
                .orient("bottom");

            //debugger;
            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function (d, i) {
                    //if (i % data.series.length === 0)
                    //    return "Steps";
                    if (i % data.series.length === 0)
                        return "The development area";
                    if (i % data.series.length === 1)
                        return "The detail that makes up an Area";
                })

            // Specify the chart area and dimensions
            var chart = d3.select(".summarychart").append("svg")
                .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                .attr("height", chartHeight)
                .style("border", "solid 1px #cccccc").style("padding", "1px").style("padding-left", "130px").style("padding-top", "14px");


            // Create bars
            var bar = chart.selectAll("g")
                .data(zippedData)
                .enter().append("g")
                .attr("transform", function (d, i) {
                  return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i / data.series.length))) + ")";
                });
            chart.select("g:nth-child(3)").style("visibility", "hidden");
            chart.select("g:nth-child(4)").style("visibility", "hidden");
            chart.call(tip);

            // Create rectangles of the correct width
            bar.append("rect")
                .attr("fill", function (d, i) { return color(i % data.series.length); })
                .attr("class", "bar")
                .attr("width", x)
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide)
                .style('stroke', function (d, i) { return "#000000"; })
                .attr("height", barHeight - 1);

            // Add text label in bar
            // debugger;
            bar.append("text")
                .attr("x", function (d, i) { return x(d) - 2; })
                .attr("y", barHeight / 2)
                // .attr("fill", "red")
                .attr("dy", ".35em")
                .text(function (d, i) {
                    return d.toFixed(1) + '%';
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return 110; })
                .attr("y", (barHeight / 2) + 10)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                //.text(function (d) { return "A"; });
                .text(function (d, i) {
                    if (i % 2 == 0) {
                        onboardedval = 0;
                    }
                    onboardedval += d;
                    if (onboardedval == 0) {
                        if (i % 2 == 0) {
                            return "Not Onboarded";
                        }
                    }
                    else {

                    }
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return -5; })
                .attr("y", barHeight / 2)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                //.text(function (d) { return "A"; });
                .text(function (d, i) {
                    //if (i % data.series.length === 0)
                    //    return "Steps";
                    if (i % data.series.length === 0)
                        return "Areas";
                    if (i % data.series.length === 1)
                        return "Competencies";
                });


            // Draw labels
            var txt = bar.append("text")
                .attr("class", "label")
                .style("font-weight", "bold")
                .style("font-size", "13px")
                .attr("x", function (d) { return - 110; })
                .attr("y", groupHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % data.series.length === 0)
                        return data.labels[Math.floor(i / data.series.length)];
                    // return data.labels[Math.floor(i / data.series.length)];
                    else
                        return ""
                });


            chart.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups / 2 + ")")
                .call(yAxis);


            // Add the x-axis.
            chart.append("g")
                .attr("class", "x axis")

                // .style("fill", "black")
                .attr("transform", "translate(" + spaceForLabels + ", " + (chartHeight - 50) + ")")
                .call(d3.svg.axis().scale(x).orient("bottom").tickFormat(function (d, i) {
                    return i == 0 ? "0%" : (i == 10 ? "100%" : "");
                }));


        }
        $("#btnSubmit").button().click(function () {
            // debugger;
            $("#myTable").tablesorter({
            });
            counter = counter + 10;
            var data = addnewTable(counter, 0);
            // addanothertbl(data);
            makeProgressChart(data);
            SummaryChart();
            $("#myTable").tablesorter({
            });
        });

        $("#btnAll").button().click(function () {
            counter = 10;
            $('.tablesorter tbody').empty()
            arr = [];
            $('#btnSubmit').prop('disabled', true);
            // counter = 0;
            var data = addnewTable(counter, 1);
            makeProgressChart(data);
            SummaryChart();
        });

        $("#btnclear").button().click(function () {
            arr = [];
            $('#Progresstbody').empty();
            $("#myTable").tablesorter({
            });
            localStorage.setItem("ChartData", localStorage.getItem("ChartWholeData"));
            localStorage.setItem("TableData", localStorage.getItem("TableWholeData"));
            //counter = 0;
            $('#systemuse').html('');
            makeTableAndChart();
        });
        $("#btnapply").button().click(function () {
            // debugger;
            event.preventDefault();
            alert("test");
            var data = JSON.parse(localStorage.getItem("ChartData"));
            var username = $('#txtusername').val();
            var managername = $('#txtmanager').val();
            var rolename = $('#ddlRole').val();
            if (managername != "" || username != "") {
                data = jQuery.grep(data, function (product, i) {
                    if (managername != "")
                        return product.managerName == managername;
                    if (username != "")
                        return product.name == username;
                    if (rol != "")
                        return product.rolename == rolename;
                });
            }
            tabledata = [];
            arr = [];
            $('#Progresstbody').empty();
            $("#myTable").tablesorter({
            });
           // $('#myTable').empty();
            $.each(data, function (index, value) {
                tabledata.push([value.name, value.stepsAssessedPCT == 100 ? "Yes" : "No", value.areasAssessedNum + '/' + value.totalAreas, value.areasAssessedPCT.toFixed(1), value.compsAssessedNum + '/' + value.totalComps, value.compsAssessedPCT.toFixed(1), value.totalWeighted.toFixed(1)]);
            });
            localStorage.setItem("ChartData", JSON.stringify(data));
            localStorage.setItem("TableData", JSON.stringify(tabledata));
            var datachart = addnewTable(1, 0);
            makeProgressChart(datachart);
            SummaryChart();

        });
        function appendData(counter, flag) {
            var html = '';
            //$('.table-striped').empty();
            //  arr = [];
            var data = JSON.parse(localStorage.getItem("dataVal"));
            var length = Object.keys(data).length;
            //alert(length);

            var j = 0;
            if (flag == 0) j = counter + 10;
            else if (flag == 2) { j = length; counter = 0; }
            else j = length;

            var table = $("<table class='table-striped'>");
            if (counter == 0) {
                var tr = $('<tr>');
                $('<th>').html("Name(group)").appendTo(tr);
                $('<th>').html("Onboarded?").appendTo(tr);
                $('<th>').html("Areas Assessed").appendTo(tr);
                $('<th>').html("Comps Assessed").appendTo(tr);
                $('<th>').html("% of Total Framework").appendTo(tr);
                $('.table-striped').append(tr);
            }


            for (var k = counter; k < j && k < length; k++) {
                var tr = $('<tr>');
                var name = data[k].name;
                var Onboarded = data[k].stepsAssessedPCT == 100 ? "Yes" : "No"; //data["stepsAssessedPCT"] == 100 ? "Yes" : "No";
                var Areas = data[k].areasAssessedNum + '/' + data[k].totalAreas; //data["areasAssessedNum"] + '/' + data["totalAreas"];
                var Comps = data[k].compsAssessedNum + '/' + data[k].totalComps;//data["compsAssessedNum"] + '/' + data["totalComps"];
                var Total = data[k].totalWeighted.toFixed(1) + '%';//data["totalWeighted"].toFixed(2) + '%';

                $('<td>').html(name).appendTo(tr);
                $('<td>').html(Onboarded).appendTo(tr);
                $('<td>').html(Areas).appendTo(tr);
                $('<td>').html(Comps).appendTo(tr);
                $('<td>').html(Total).appendTo(tr);
                $('.table-striped').append(tr);
                if (counter == length) $('#btnSubmit').val("No more data");
                arr.push({ 'name': name, 'stepsAssessedPCT': data[k].stepsAssessedPCT, 'areasAssessedPCT': data[k].areasAssessedPCT, 'compsAssessedPCT': data[k].compsAssessedPCT });
            }
            // counter++;
            console.log(arr);
            return arr;
        }


    })