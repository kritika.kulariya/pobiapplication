﻿'use strict';

/* Controllers */

angular.module('Area.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })
    .controller('AreaController', ['$scope', 'dataFactory', '$http', '$rootScope', function ($scope, dataFactory, $http, $rootScope) {
        //var arrArea = new Array();
        var token = ""; var arr = []; var objects = []; var tabledata = [];
        var counter = 0;

        //var data = $.param({
        //    username: "insights@pobi.co.uk",
        //    password: "Xd2actQu74pB$",
        //    grant_type: "password"
        //});

        //dataFactory.getToken(data)
        //    .then(function (response) {
        //        token = response.data.access_token;

        token = $rootScope.token;

        dataFactory.popularAreas(token)
            .then(function (response) {
                $('#mydiv').hide();
                var data = response.data;
                localStorage.setItem("areaData", JSON.stringify(response.data));
                

                $.each(data, function (index, value) {
                    objects.push([value.url, value.uniqueVisitors]);
                });
                var dursum = 0; var vistsum = 0;

                for (var i = 0; i < data.length; i++) {
                    dursum = dursum + data[i]["totalDurationSec"];
                    vistsum = vistsum + data[i]["uniqueVisitors"];
                }
                localStorage.setItem("visitorsum", vistsum);
                var data = appendData(counter, 0, vistsum);
                //makeProgressChart(data);
                counter = counter + 5;
                //alert("Sucess");

                $.each(data, function (index, value) {
                    tabledata.push([value.url, value.uniqueVisitors, value.totalDurationSec, value.perVisitor, value.perDuration, value.sec]);
                    //tabledata.push([value.url, value.uniqueVisitors, secondsToHms(value.totalDurationSec,"ms"), parseFloat((value.uniqueVisitors / vistsum) * 100).toFixed(1) + '%', Math.round(value.totalDurationPCT) + '%', value.totalDurationSec]);
                });
                addTable(tabledata);
                $("#myTable").tablesorter({
                    theme: 'blue',
                    showProcessing: true,
                    widgets: ["zebra"],
                    widgetOptions: {
                        zebra: ["normal-row", "alt-row"]
                    }
                });
                var sortedcol = "", sortDir = ""; var JSONtable = "";
                $(function () {
                    $('#myTable')
                        .on('sortEnd', function () {
                            debugger;
                            var currentSort = this.config.sortList,
                                firstColumn = currentSort[0][0];
                            sortedcol = this.config.headerList[firstColumn].innerText;
                            sortDir = currentSort[0][1] === 0 ? 'Ascending' : 'Descending';
                            console.log('column = ' + sortedcol, ', direction = ' + sortDir);
                            JSONtable = createJSON();
                            //console.log(JSONtable);
                            JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Unique Visitors":').join('"uniqueVisitors":'));
                            JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Section":').join('"url":'));
                            JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Duration":').join('"totalDurationSec":'));
                            JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Sec":').join('"sec":'));

                            //console.log(JSON.stringify(JSONtable));
                            createVisitorChart(JSONtable.Key);
                            createDurationChart(JSONtable.Key);

                        })
                });

                /*** UniqueVisitor Chart ***/
                createVisitorChart(data);

                /*** Chart***/

                /*** Duration Chart ***/

                createDurationChart(data);

                /*** Duration Chart ***/
                
            }, function (error) {
                $("#mydiv").hide();
                alert('PopularArea error');
            });

        //}, function (error) {
        //    alert('Calling error');
        //});



        function addTable(data) {
            var myTableDiv = document.getElementById("metric_results")
            //myTableDiv.innerHTML = "";
            if (data.length > 0) {
                $('.tablesorter').remove();
                var table = document.createElement('TABLE')
                var tableheader = document.createElement('THEAD')
                var tableBody = document.createElement('TBODY')
                var tablefoot = document.createElement('TFOOT')
                

                table.border = '1'
                table.className = "tablesorter";
                table.id = "myTable";
                table.appendChild(tableheader);
                // table.appendChild(tableBody);

                var heading = new Array();
                heading[0] = "Section"
                heading[1] = "Unique Visitors"
                heading[2] = "Duration"
                heading[3] = "% Visitors"
                heading[4] = "% Duration"
                heading[5] = "Sec"

                var Hover = new Array();
                Hover[0] = "Section"
                Hover[1] = "Unique Visitors"
                Hover[2] = "Duration"
                Hover[3] = "% Visitors"
                Hover[4] = "% Duration"
                Hover[5] = "Sec"


                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tableheader.appendChild(tr);
                for (var i = 0; i < heading.length; i++) {
                    var th = document.createElement('TH')
                    //th.width = '75';
                    th.setAttribute('data-title', Hover[i]);
                    th.appendChild(document.createTextNode(heading[i]));
                    tr.appendChild(th);
                }
                table.appendChild(tableBody);
                //TABLE ROWS
                for (var i = 0; i < data.length; i++) {
                    var tr = document.createElement('TR');
                    for (var j = 0; j < data[i].length; j++) {
                        var td = document.createElement('TD')
                        td.appendChild(document.createTextNode($.isNumeric(data[i][j]) == true ? Math.round(data[i][j]) : data[i][j]));
                        tr.appendChild(td)
                    }
                    tableBody.appendChild(tr);
                }

                //TABLE FOOTER
                table.appendChild(tablefoot);
                
                var footer = new Array();
                footer[0] = ""
                footer[1] = "Total:"
                footer[2] = "Total:"
                footer[3] = "Total:"
                footer[4] = "Total:"
                footer[5] = ""

                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tr.className = "grandtotal";
                tablefoot.appendChild(tr);
                for (var i = 0; i < footer.length; i++) {
                    var td = document.createElement('TD');
                    td.className = "total";
                    td.appendChild(document.createTextNode(footer[i]));
                    tr.appendChild(td);
                }

                //myTableDiv.appendChild(table);
                myTableDiv.insertBefore(table, myTableDiv.childNodes[0]);

                $('.tablesorter thead th').each(function (i) {
                    calculateColumn(i);
                });

                $('.tablesorter th:nth-child(6),td:nth-child(6)').hide();
                // tr.outerHTML = "<tr class='grandtotal'>< td ></td ><td>Total:</td><td>Total:</td><td>Total:</td><td>Total:</td></tr >";
                //tableBody.appendChild(tr);





                function calculateColumn(index) {
                    var total = 0;
                    $('.tablesorter tbody tr').each(function () {
                        if (index == 2) {

                            var value = parseInt($('td', this).eq(5).text());
                            if (!isNaN(value)) {
                                total += value;
                            }

                        }
                        var value = parseFloat($('td', this).eq(index).text());
                        if (!isNaN(value)) {
                            total += value;
                        }
                    });
                    if (index != 0) {
                        if (index == 3 || index == 4) {
                            $('.tablesorter tfoot .grandtotal td').eq(index).text((total).toFixed(1) + '%');
                        }
                        else if (index == 2) {
                            $('.tablesorter tfoot .grandtotal td').eq(index).text(secondsToHms(total, ""));
                            //total = secondsToHms(total);
                        }
                        else {
                            $('.tablesorter tfoot .grandtotal td').eq(index).text((total).toFixed(1));
                        }

                    }
                    else {
                        $('.tablesorter tfoot .grandtotal td').eq(index).text("Summary of Table");
                        $('.tablesorter tfoot .grandtotal td').eq(index).css('font-weight', 'bold');
                    }
                }
            }


        }

        $("#btnSubmit").button().click(function () {
            debugger;
            tabledata = [];
            var sum = localStorage.getItem("visitorsum");
            var data = appendData(counter, 0, sum);
            counter = counter + 5;
            $.each(data, function (index, value) {
                tabledata.push([value.url, value.uniqueVisitors, value.totalDurationSec, value.perVisitor, value.perDuration, value.sec]);
            });
            addTable(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            var sortedcol = "", sortDir = ""; var JSONtable = "";
            $(function () {
                $('#myTable')
                    .on('sortEnd', function () {
                        debugger;
                        var currentSort = this.config.sortList,
                            firstColumn = currentSort[0][0];
                        sortedcol = this.config.headerList[firstColumn].innerText;
                        sortDir = currentSort[0][1] === 0 ? 'Ascending' : 'Descending';
                        console.log('column = ' + sortedcol, ', direction = ' + sortDir);
                        JSONtable = createJSON();
                        //console.log(JSONtable);
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Unique Visitors":').join('"uniqueVisitors":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Section":').join('"url":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Duration":').join('"totalDurationSec":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Sec":').join('"sec":'));

                        //console.log(JSON.stringify(JSONtable));
                        createVisitorChart(JSONtable.Key);
                        createDurationChart(JSONtable.Key);

                    })
            });
            createVisitorChart(data);
            createDurationChart(data);
        });
        $("#btnAll").button().click(function () {
            debugger;
            tabledata = [];
            var sum = localStorage.getItem("visitorsum");
            counter = 0;
            var data = appendData(counter, 1, sum);
            $.each(data, function (index, value) {
                tabledata.push([value.url, value.uniqueVisitors, value.totalDurationSec, value.perVisitor, value.perDuration, value.sec]);
            });
            addTable(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            var sortedcol = "", sortDir = ""; var JSONtable = "";
            $(function () {
                $('#myTable')
                    .on('sortEnd', function () {
                        debugger;
                        var currentSort = this.config.sortList,
                            firstColumn = currentSort[0][0];
                        sortedcol = this.config.headerList[firstColumn].innerText;
                        sortDir = currentSort[0][1] === 0 ? 'Ascending' : 'Descending';
                        console.log('column = ' + sortedcol, ', direction = ' + sortDir);
                        JSONtable = createJSON();
                        //console.log(JSONtable);
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Unique Visitors":').join('"uniqueVisitors":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Section":').join('"url":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Duration":').join('"totalDurationSec":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Sec":').join('"sec":'));

                        //console.log(JSON.stringify(JSONtable));
                        createVisitorChart(JSONtable.Key);
                        createDurationChart(JSONtable.Key);

                    })
            });
            createVisitorChart(data);
            createDurationChart(data);
            $("#btnLess").show();
            $("#btnAll").hide();
            //appendData();
        });

        $("#btnLess").button().click(function () {
            $("#btnLess").hide();
            $("#btnAll").show();

            tabledata = [];
            var sum = localStorage.getItem("visitorsum");
            var data = appendData(counter, 0, sum);
            counter = counter + 5;
            $.each(data, function (index, value) {
                tabledata.push([value.url, value.uniqueVisitors, value.totalDurationSec, value.perVisitor, value.perDuration, value.sec]);
            });
            addTable(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            var sortedcol = "", sortDir = ""; var JSONtable = "";
            $(function () {
                $('#myTable')
                    .on('sortEnd', function () {
                        debugger;
                        var currentSort = this.config.sortList,
                            firstColumn = currentSort[0][0];
                        sortedcol = this.config.headerList[firstColumn].innerText;
                        sortDir = currentSort[0][1] === 0 ? 'Ascending' : 'Descending';
                        console.log('column = ' + sortedcol, ', direction = ' + sortDir);
                        JSONtable = createJSON();
                        //console.log(JSONtable);
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Unique Visitors":').join('"uniqueVisitors":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Section":').join('"url":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Duration":').join('"totalDurationSec":'));
                        JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Sec":').join('"sec":'));

                        //console.log(JSON.stringify(JSONtable));
                        createVisitorChart(JSONtable.Key);
                        createDurationChart(JSONtable.Key);

                    })
            });
            createVisitorChart(data);
            createDurationChart(data);

        });

        function appendData(counter, flag, vistsum = null) {
            var html = '';
            // $('.table-striped').empty();
            arr = [];
            var data = JSON.parse(localStorage.getItem("areaData"));
            var length = Object.keys(data).length;
            //alert(length);

            var j = 0;
            if (flag == 0) j = counter + 5;
            else if (flag == 2) { j = length; counter = 0; }
            else j = length;

            //addTable(data);

            var table = $("<table class='table-striped'>");
            if (counter == 0) {
                var tr = $('<tr>');
                $('<th>').html("Section").appendTo(tr);
                $('<th>').html("Unique Visitors").appendTo(tr);
                $('<th>').html("Duration").appendTo(tr);
                $('<th>').html("% Visitors").appendTo(tr);
                $('<th>').html("% Duration").appendTo(tr);
                $('<th>').html("Sec").appendTo(tr);
                $('.table-striped').append(tr);
            }

            for (var k = 0; k < j && k < length; k++) {

                var tr = $('<tr>');
                var section = data[k].url;
                var visitor = data[k].uniqueVisitors;
                var duration = secondsToHms(data[k].totalDurationSec, "ms");
                var perVisitor = parseFloat((data[k].uniqueVisitors / vistsum) * 100).toFixed(1) + '%';
                var perDuration = Math.round(data[k].totalDurationPCT).toFixed(1) + '%';
                var sec = data[k].totalDurationSec;

                $('<td>').html(section).appendTo(tr);
                $('<td>').html(visitor).appendTo(tr);
                $('<td>').html(duration).appendTo(tr);
                $('<td>').html(perVisitor).appendTo(tr);
                $('<td>').html(perDuration).appendTo(tr);
                $('<td>').html(sec).appendTo(tr);
                $('.table-striped').append(tr);
                if (counter == length) $('#btnSubmit').val("No more data");

                arr.push({ 'url': data[k].url, 'uniqueVisitors': data[k].uniqueVisitors, 'totalDurationSec': secondsToHms(data[k].totalDurationSec, "ms"), 'perVisitor': parseFloat((data[k].uniqueVisitors / vistsum) * 100).toFixed(1) + '%', 'perDuration': Math.round(data[k].totalDurationPCT).toFixed(1) + '%', 'sec': data[k].totalDurationSec });

            }
            // counter++;
            console.log(arr);
            return arr;
        }

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#myTable th');
            $('#myTable tbody tr').each(function (i, tr) {
                var obj = {},

                    $tds = $(tr).find('td');

                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }

        function createVisitorChart(data) {
            debugger;
            if (data.length > 0) {
                //d3.selectAll("svg > *").remove();
                d3.selectAll("#visitorsvg > svg").remove();

                var margin = { top: 20, right: 20, bottom: 70, left: 40 },
                    width = 345,
                    height = 300 - margin.top - margin.bottom;


                // set the ranges
                var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);

                var y = d3.scale.linear().range([height, 0]);

                // define the axis
                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")


                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .ticks(10);

                //Add tooltip to Chart on hover
                var tip = d3.tip()
                    .attr('class', 'd3-tip')
                    .offset([-10, 0])
                    .html(function (d) {
                        return "<strong>Visitors:</strong> <span style='color:red'>" + d.uniqueVisitors + "</span>";
                    })


                // add the SVG element
                var svg = d3.select("#visitorsvg").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", 349)//height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");
                svg.call(tip);

                // load the data
                //  d3.json("data.json", function (error, data) {

                data.forEach(function (d) {
                    d.text = d.url;
                    d.value = d.uniqueVisitors;
                });

                // scale the range of the data
                x.domain(data.map(function (d) { return d.text; }));
                y.domain([0, d3.max(data, function (d) { return d.value; })]);

                // add axis
                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis)
                    .selectAll("text")
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", "-.55em")
                    .attr("transform", "rotate(-90)");

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 5)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Visitors");


                // Add bar chart & mouse hover tooltip
                svg.selectAll("bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("class", "bar")
                    .attr("x", function (d) { return x(d.text); })
                    .attr("width", x.rangeBand())
                    .attr("y", function (d) { return y(d.value); })
                    .attr("height", function (d) { return height - y(d.value); })
                    .on('mouseover', tip.show)
                    .on('mouseout', tip.hide)
                    .style('stroke', function (d, i) { return "#000000"; });

                svg.append("text")
                    .attr("x", (width / 2))
                    .attr("y", 300)//0 - ((margin.top / 2) - 1))
                    .attr("text-anchor", "middle")
                    .style("font-size", "16px")
                    .style("text-decoration", "underline")
                    .style("font-weight", "bold")
                    .text("Unique Visitors");

                //svg.selectAll(".text")
                //    .data(data)
                //    .enter()
                //    .append("text")
                //    .attr("class", "label")
                //    .attr("x", (function (d) { return x(d.text) + x.rangeBand() / 3; }))
                //    .attr("y", function (d) { return y(d.value) + 1; })
                //    .attr("dy", ".75em")
                //    .text(function (d) { return d.value; });  
            }

        }

        function createDurationChart(data) {

            if (data.length > 0) {
                d3.selectAll("#durationsvg > svg").remove();

                var margin = { top: 20, right: 20, bottom: 70, left: 160 },
                    width = 345,//600 - margin.left - margin.right,
                    height = 300 - margin.top - margin.bottom;

                data.forEach(function (d) {
                    d.sec = d.sec;
                });


                // set the ranges
                var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);

                var y = d3.scale.linear().range([height, 0]);

                // define the axis
                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                // Formatters for counts and times (converting numbers to Dates).
                var formatCount = d3.format(",.0f"),
                    formatTime = d3.time.format("%H:%M"),
                    formatMinutes = function (d) { return formatTime(new Date(2018, 0, 1, 0, d)); };

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(formatMinutes);
                //.ticks(10);

                //Add tooltip to Chart on hover
                var tip = d3.tip()
                    .attr('class', 'd3-tip')
                    .offset([-10, 0])
                    .html(function (d) {
                        return "<strong>Duration:</strong> <span style='color:red'>" + d.sec + "</span>";
                    })


                // add the SVG element
                var svg = d3.select("#durationsvg").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", 349)//height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");


                svg.call(tip);
                // load the data

                data.forEach(function (d) {
                    d.text = d.url;
                    d.value = d.sec;
                });

                // scale the range of the data
                x.domain(data.map(function (d) { return d.text; }));
                y.domain([0, d3.max(data, function (d) { return d.value; })]);

                // add axis
                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis)
                    .selectAll("text")
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", "-.55em")
                    .attr("transform", "rotate(-90)");

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 5)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Duration");


                // Add bar chart & mouse hover tooltip
                svg.selectAll("bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("class", "bar")
                    .attr("x", function (d) { return x(d.text); })
                    .attr("width", x.rangeBand())
                    .attr("y", function (d) { return y(d.value); })
                    .attr("height", function (d) { return height - y(d.value); })
                    .on('mouseover', tip.show)
                    .on('mouseout', tip.hide)
                    .style('stroke', function (d, i) { return "#000000"; });


                svg.append("text")
                    .attr("x", (width / 2))
                    .attr("y", 324)//0 - ((margin.top / 2) - 1))
                    .attr("text-anchor", "middle")
                    .style("font-size", "16px")
                    .style("text-decoration", "underline")
                    .style("font-weight", "bold")
                    .text("Duration");
                /**** Add Label on Chart ****/
                //svg.selectAll(".text")
                //    .data(data)
                //    .enter()
                //    .append("text")
                //    .attr("class", "label")
                //    .attr("x", (function (d) { return x(d.url); }))
                //    .attr("y", function (d) { return y(d.totalDurationSec) - 20; })
                //    .attr("dy", ".75em")
                //    .text(function (d) { return d.totalDurationSec; });   	  
                /**** Add Label on Chart ****/
            }


            //var svg = d3.select("#durationsvg")
            //    .append("svg")
            //    .append("g")

            //svg.append("g")
            //    .attr("class", "slices");
            //svg.append("g")
            //    .attr("class", "labels");
            //svg.append("g")
            //    .attr("class", "lines");

            //var width = 490,
            //    height = 250,
            //    radius = Math.min(width, height) / 2;

            //var pie = d3.layout.pie()
            //    .sort(null)
            //    .value(function (d) {
            //        return d.value;
            //    });

            //var arc = d3.svg.arc()
            //    .outerRadius(radius * 0.8)
            //    .innerRadius(radius * 0.4);

            //var outerArc = d3.svg.arc()
            //    .innerRadius(radius * 0.9)
            //    .outerRadius(radius * 0.9);

            //svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            //var key = function (d) { return d.data.label; };

            //var color = d3.scale.ordinal()
            //    .domain(["Lorem ipsum", "dolor sit", "amet", "consectetur", "adipisicing", "elit", "sed", "do", "eiusmod", "tempor", "incididunt"])
            //    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

            //function extractColumn(arr, column) {
            //    return arr.map(x => x[column])
            //}

            //function randomData() {
            //    var labels = extractColumn(data, 'url');//color.domain();
            //    return labels.map(function (label, index) {
            //        return {
            //            label: label, value: data[index].totalDurationSec
            //        }//Math.random() }
            //    });
            //}

            //change(randomData());

            //d3.select(".randomize")
            //    .on("click", function () {
            //        change(randomData());
            //    });


            //function change(data) {

            //    /* ------- PIE SLICES -------*/
            //    var slice = svg.select(".slices").selectAll("path.slice")
            //        .data(pie(data), key);

            //    slice.enter()
            //        .insert("path")
            //        .style("fill", function (d) { return color(d.data.label); })
            //        .attr("class", "slice");

            //    slice
            //        .transition().duration(1000)
            //        .attrTween("d", function (d) {
            //            this._current = this._current || d;
            //            var interpolate = d3.interpolate(this._current, d);
            //            this._current = interpolate(0);
            //            return function (t) {
            //                return arc(interpolate(t));
            //            };
            //        })

            //    slice.exit()
            //        .remove();

            //    /* ------- TEXT LABELS -------*/

            //    var text = svg.select(".labels").selectAll("text")
            //        .data(pie(data), key);

            //    text.enter()
            //        .append("text")
            //        .attr("dy", ".35em")
            //        .text(function (d) {
            //            return d.data.label;
            //        });

            //    function midAngle(d) {
            //        return d.startAngle + (d.endAngle - d.startAngle) / 2;
            //    }

            //    text.transition().duration(1000)
            //        .attrTween("transform", function (d) {
            //            this._current = this._current || d;
            //            var interpolate = d3.interpolate(this._current, d);
            //            this._current = interpolate(0);
            //            return function (t) {
            //                var d2 = interpolate(t);
            //                var pos = outerArc.centroid(d2);
            //                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
            //                return "translate(" + pos + ")";
            //            };
            //        })
            //        .styleTween("text-anchor", function (d) {
            //            this._current = this._current || d;
            //            var interpolate = d3.interpolate(this._current, d);
            //            this._current = interpolate(0);
            //            return function (t) {
            //                var d2 = interpolate(t);
            //                return midAngle(d2) < Math.PI ? "start" : "end";
            //            };
            //        });

            //    text.exit()
            //        .remove();

            //    /* ------- SLICE TO TEXT POLYLINES -------*/

            //    var polyline = svg.select(".lines").selectAll("polyline")
            //        .data(pie(data), key);

            //    polyline.enter()
            //        .append("polyline");

            //    polyline.transition().duration(1000)
            //        .attrTween("points", function (d) {
            //            this._current = this._current || d;
            //            var interpolate = d3.interpolate(this._current, d);
            //            this._current = interpolate(0);
            //            return function (t) {
            //                var d2 = interpolate(t);
            //                var pos = outerArc.centroid(d2);
            //                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
            //                return [arc.centroid(d2), outerArc.centroid(d2), pos];
            //            };
            //        });

            //    polyline.exit()
            //        .remove();
            //};
        }

        function secondsToHms(d, type) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            var hDisplay = h > 0 ? h : "";
            var mDisplay = m > 0 ? m : "";
            var sDisplay = s > 0 ? s : "";
            if (type == "ms") {
                return mDisplay + ":" + sDisplay;
            }
            else {
                return hDisplay + ":" + mDisplay + ":" + sDisplay;
            }


            //var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
            //var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
            //var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
            //return hDisplay + mDisplay + sDisplay;

        }


    }])