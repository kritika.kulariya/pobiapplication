﻿'use strict';

/* Controllers */

angular.module('Feedbackuse.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

    .controller('FeedbackuseController', ['$scope', 'dataFactory', '$http', '$rootScope', '$timeout', function ($scope, dataFactory, $http, $rootScope, $timeout) {

        var token = ""; var tabledata = []; var counter = 0;var arr = []; var objects = []; 
        $scope.formInfo = {};
        $scope.Col1 = 0;
        $scope.Col2 = 0;
        $scope.Col3 = 0;
        $scope.Col4 = 0;

        token = $rootScope.token;
        //$('#mydiv').hide(); 
        dataFactory.FedbckUse(token)
            .then(function (response) {
                var data = response.data;
                localStorage.setItem("FeedbackData", JSON.stringify(response.data));
                
                $rootScope.$on("Feedsearchevent", function (event, message) {
                    $scope.formInfo = message;
                    debugger;
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username); //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/

                    localStorage.setItem("FeedbackData", JSON.stringify(data1));

                    var limitdata = appendData(counter, 0);
                    counter = counter + 3;
                    tabledata = [];
                    $.each(limitdata, function (index, value) {
                        tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT.toFixed(1) + '%',
                        value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT.toFixed(1) + '%']);
                    });
                    var chartdata = addTable(tabledata, "FeedbckTbl");
                    addanothertbl(tabledata);
                    //  debugger;
                    // makeFeedbackChart(chartdata);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["normal-row", "alt-row"]
                        }
                    });

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    // createProActChart(data1);
                    /*** Re-draw chart ***/


                });

                $rootScope.$on("Feedclearevent", function (event, message) {
                    debugger;
                    $scope.formInfo = message;
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username); //, $scope.formInfo.frmDate, $scope.formInfo.toDate
                    console.log(data1);

                    data1 = (data1.length == 0 ? data : data1);
                    localStorage.setItem("FeedbackData", JSON.stringify(data1));

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    counter = 0;
                    var limitdata = appendData(counter, 0);
                    counter = counter + 3;
                    tabledata = [];

                    $.each(limitdata, function (index, value) {
                        tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT.toFixed(1) + '%',
                        value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT.toFixed(1) + '%']);
                    });
                    addTable(tabledata, "FeedbckTbl");
                    addanothertbl(tabledata);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["normal-row", "alt-row"]
                        }
                    });

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    // createProActChart(data1);
                    /*** Re-draw chart ***/


                });



                $('#mydiv').hide();

                var newdata = appendData(counter, 0);
                counter = counter + 3;

                $.each(newdata, function (index, value) {
                    tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT + '%',
                    value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT + '%']);
                });
                addTable(tabledata, "FeedbckTbl");
                addanothertbl(tabledata);
                $("#myTable").tablesorter({
                    theme: 'blue',
                    showProcessing: true,
                    widgets: ["zebra"],
                    widgetOptions: {
                        zebra: ["normal-row", "alt-row"]
                    }
                });
                

                var targetColumn = 0;

                var targetColumn = 0;

                $(".sortddl").change(function () {
                    debugger;
                    var direction = parseInt($(this).val(), 10),
                        sort = [[targetColumn, direction]];
                    $("#myTable").trigger("sorton", [sort]);
                    return false;
                });

                // debugger;


            }, function (error) {
                $("#mydiv").hide();
                alert('Feedback error');
            });

        //}, function (error) {
        //    alert('Calling error');
        //}),3000);


        function addTable(data,id) {
            var myTableDiv = document.getElementById("FeedbckTbl")//id
           // myTableDiv.innerHTML = "";
            if (data.length > 0) {
                $('#myTable').remove();
                $("#btnSubmit").show();
                $("#btnAll").show();
                $("#btnLess").hide();
                var table = document.createElement('TABLE')
                var tableheader = document.createElement('THEAD')
                var tableBody = document.createElement('TBODY')
                var tablefoot = document.createElement('TFOOT')

                table.border = '1'
                table.className = "tablesorter";
                table.id = "myTable";
                table.appendChild(tableheader);
                table.appendChild(tableBody);

                var heading = new Array();
                heading[0] = "Name"
                heading[1] = "Role Name"
                heading[2] = "Number of Questions Asked"
                heading[3] = "Number of Questions Answered"
                heading[4] = "Percent"
                heading[5] = "Number of People Asked"
                heading[6] = "Number of People Answered"
                heading[7] = "Percent"


                var Hover = new Array();
                Hover[0] = "Name"
                Hover[1] = "Role Name"
                Hover[2] = "Question Asked"
                Hover[3] = "Question Answered"
                Hover[4] = "% Complete"
                Hover[5] = "People Asked"
                Hover[6] = "People Answered"
                Hover[7] = "% Complete"


                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tableheader.appendChild(tr);
                for (var i = 0; i < heading.length; i++) {
                    var th = document.createElement('TH')
                    //th.width = '75';
                    // th.id = 'facility_header';
                    th.setAttribute('title', Hover[i]);
                    th.appendChild(document.createTextNode(heading[i]));
                    tr.appendChild(th);
                }
                table.appendChild(tableBody);
                //TABLE ROWS
                for (var i = 0; i < data.length; i++) {
                    tr = document.createElement('TR');
                    for (var j = 0; j < data[i].length; j++) {
                        var td = document.createElement('TD')
                        td.appendChild(document.createTextNode(data[i][j]));
                        tr.appendChild(td)
                    }

                    tableBody.appendChild(tr);


                }

                //TABLE FOOTER
                table.appendChild(tablefoot);
                var footer = new Array();
                footer[0] = ""
                footer[1] = ""
                footer[2] = "Total:"
                footer[3] = "Total:"
                footer[4] = "Total:"
                footer[5] = "Total:"
                footer[6] = "Total:"
                footer[7] = "Total:"


                //TABLE COLUMNS
                tr = document.createElement('TR');
                tr.className = "grandtotal";
                tablefoot.appendChild(tr);
                for (var i = 0; i < footer.length; i++) {
                    td = document.createElement('TD');
                    td.className = "total";
                    td.appendChild(document.createTextNode(footer[i]));
                    tr.appendChild(td);
                }

                //myTableDiv.appendChild(table);
                myTableDiv.insertBefore(table, myTableDiv.childNodes[0]);

                $('#myTable thead th').each(function (i) {
                    calculateColumn(i, id);
                });
                
            }
            else {
                //myTableDiv.innerHTML = "No data";
                $('#myTable').remove();
                $("#btnSubmit").hide();
                $("#btnAll").hide();
                $("#btnLess").hide();
            }
            
        }


        function calculateColumn(index, id) {
            var total = 0;
            $('#' + id + ' ' + 'tbody tr').each(function () {
                if (index == 2) {
                    
                }
                var value = parseFloat($('td', this).eq(index).text());
                if (!isNaN(value)) {
                    total += value;
                }
            });
            if (index != 0 && index != 1) {
                if (index == 4 || index == 7) {
                    $('#' + id + ' ' + 'tfoot .grandtotal td').eq(index).text(parseInt(total).toFixed(1) + '%');
                }
                else {
                    $('#' + id + ' ' +'tfoot .grandtotal td').eq(index).text(parseInt(total));
                }
            }
            else if(index==0) {
                $('#' + id + ' ' + 'tfoot .grandtotal td').eq(index).text("Summary of Table");
                $('#' + id + ' ' + 'tfoot .grandtotal td').eq(index).css('font-weight', 'bold');
            }
        }

        function addanothertbl(data) {
            if (data.length > 0) {
                $('.tableparent').empty();
                var table = $('.tableparent')
                var tr = $('<tr>');

                $('<th  rowspan="2" colspan="0" title="Overall Summary" class="first-child">').html("Overall Summary").appendTo(tr);

                $('<th title="Number of Questions Asked" style="width:11.5%;">').html("Number of Questions Asked").appendTo(tr);
                $('<th title="Number of Questions Answered" style="width:11.5%;">').html("Number of Questions Answered").appendTo(tr);
                $('<th title="Percent" style="width:11.5%;">').html("Percent").appendTo(tr);
                $('<th title="Number of People Asked" style="width:11.5%;">').html("Number of People Asked").appendTo(tr);
                $('<th title="Number of People Answered" style="width:11.5%;">').html("Number of People Answered").appendTo(tr);
                $('<th title="Percent">').html("Percent").appendTo(tr);
                table.append(tr);

                var a = 0; var b = 0; var c = 0; var d = 0; var e = 0; var f = 0; var g = 0.0;
                $.each(data, function (i, item) {

                    a += item[2]
                    b += item[3]
                    c += parseFloat(item[4].replace('%', ''))
                    d += item[5]
                    e += item[6]
                    f += parseFloat(item[7].replace('%', ''))
                });
                tr = $('<tr style="background-color: rgb(212, 227, 232);">');
                $scope.Col1 = a;
                $scope.Col2 = b;
                $scope.Col3 = d;
                $scope.Col4 = e;
                $('<td>').html(a).appendTo(tr);
                $('<td>').html(b).appendTo(tr);
                $('<td>').html(c.toFixed(1) + '%').appendTo(tr);
                $('<td>').html(d).appendTo(tr);
                $('<td>').html(e).appendTo(tr);
                $('<td>').html(f.toFixed(1) + '%').appendTo(tr);
                table.append(tr);
                var jsondata = createJSON();
                jsondata = JSON.parse(JSON.stringify(jsondata).split('"Number of Questions Asked":').join('"questionsAsked":'));
                jsondata = JSON.parse(JSON.stringify(jsondata).split('"Number of Questions Answered":').join('"questionsAnswered":'));
                jsondata = JSON.parse(JSON.stringify(jsondata).split('"Number of People Asked":').join('"individualsAsked":'));
                jsondata = JSON.parse(JSON.stringify(jsondata).split('"Number of People Answered":').join('"individualsAnswered":'));
                debugger;
                makeSummaryFeedChart(jsondata.Key);
            }
            else {
                $('.tableparent').empty();
            }

        }

        $("#btnSubmit").button().click(function () {
            debugger;
            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 3;
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT + '%',
                value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT + '%']);
            });
            addTable(tabledata, "FeedbckTbl");
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
        });
        $("#btnAll").button().click(function () {
            debugger;
            tabledata = [];
            counter = 0;
            var data = appendData(counter, 1);
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT + '%',
                value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT + '%']);
            });
            addTable(tabledata, "FeedbckTbl");
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
         
            $("#btnLess").show();
            $("#btnAll").hide();
        });

        $("#btnLess").button().click(function () {
            $("#btnLess").hide();
            $("#btnAll").show();

            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 3;
            $.each(data, function (index, value) {
                tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT + '%',
                value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT + '%']);
            });
            addTable(tabledata, "FeedbckTbl");
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            

            $("#btnLess").show();
            $("#btnAll").hide();
        });

        function appendData(counter, flag) {
            debugger;
            var html = '';
            // $('.table-striped').empty();
            arr = [];
            var data = JSON.parse(localStorage.getItem("FeedbackData"));
            var length = Object.keys(data).length;
            //alert(length);

            var j = 0;
            if (flag == 0) j = counter + 3;
            else if (flag == 2) { j = length; counter = 0; }
            else j = length;
            });


            var table = $("<table class='table-striped'>");
            if (counter == 0) {
                var tr = $('<tr>');
                $('<th>').html("Name").appendTo(tr);
                $('<th>').html("Role Name").appendTo(tr);
                $('<th>').html("Number of Questions Asked").appendTo(tr);
                $('<th>').html("Number of Questions Answered").appendTo(tr);
                $('<th>').html("Percent").appendTo(tr);
                $('<th>').html("Number of People Asked").appendTo(tr);
                $('<th>').html("Number of People Answered").appendTo(tr);
                $('<th>').html("Percent").appendTo(tr);
                $('.table-striped').append(tr);
            }
            //$.each(data, function (index, value) {
            //    tabledata.push([value.userName, value.roleName, value.questionsAsked, value.questionsAnswered, value.questionsAnsweredPCT + '%',
            //    value.individualsAsked, value.individualsAnswered, value.individualsAnsweredPCT + '%']);
            //});
            for (var k = 0; k < j && k < length; k++) {

                var tr = $('<tr>');
                var userName = data[k].userName;
                var roleName = data[k].roleName;
                var questionsAsked = data[k].questionsAsked;
                var questionsAnswered = data[k].questionsAnswered;
                var questionsAnsweredPCT = data[k].questionsAnsweredPCT;
                var individualsAsked = data[k].individualsAsked;
                var individualsAnswered = data[k].individualsAnswered;
                var individualsAnsweredPCT = data[k].individualsAnsweredPCT;

                $('<td>').html(userName).appendTo(tr);
                $('<td>').html(roleName).appendTo(tr);
                $('<td>').html(questionsAsked).appendTo(tr);
                $('<td>').html(questionsAnswered).appendTo(tr);
                $('<td>').html(questionsAnsweredPCT).appendTo(tr);
                $('<td>').html(individualsAsked).appendTo(tr);
                $('<td>').html(individualsAnswered).appendTo(tr);
                $('<td>').html(individualsAnsweredPCT).appendTo(tr);
                $('.table-striped').append(tr);
                (counter >= length) ? ($('#btnSubmit').val("No more data")) : ($('#btnSubmit').val("Load More..."));

                arr.push({ 'userName': data[k].userName, 'roleName': data[k].roleName, 'questionsAsked': data[k].questionsAsked, 'questionsAnswered': data[k].questionsAnswered, 'questionsAnsweredPCT': questionsAnsweredPCT, 'individualsAsked': data[k].individualsAsked, 'individualsAnswered': data[k].individualsAnswered, 'individualsAnsweredPCT': data[k].individualsAnsweredPCT });

            }
            // counter++;
            console.log(arr);
            makeFeedbackChart(arr);
            return arr;
        }
        


        function filter(data1, manager, role, aspiration, username) {
            debugger;
            var check = 0, result = [];
            if (manager != "" && manager != undefined) {
                check = 1;
                result = data1.filter(function (item) { return item.managerName == manager });
            }

            if (role != "" && role != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.roleName == role });
            }

            if (aspiration != "" && aspiration != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.roleName == aspiration });
            }

            if (username != "" && username != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.userName == username });
            }

            return result;
        }

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#parenttbl th');
            $('#parenttbl  tr').each(function (i, tr) {
                i = 2;
                var obj = {},
                    $tds = $(tr).find('td');
                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }
        
        function makeFeedbackChart(data) {

        function makeFeedbackChart(data) {

            $(".chart").empty();
            $(".xaxis").empty();
<<<<<<< HEAD

           var userName = data.map(function (item) { return item.userName });
=======

            if (chartHeight > 340) { $(".chart").css("overflow-y", "scroll").css("height", "340px"); }
            else { $(".chart").removeAttr("style") }

            var userName = data.map(function (item) { return item.userName });
>>>>>>> b029483b93b4ecf355d746036ed1e903b56a7c0f
            var questionsAskedAnswer = data.map(function (item) { return { 'questionsAsked': item.questionsAsked, 'questionsAnswered': item.questionsAnswered } });
            var individualsAskedAnswer = data.map(function (item) { return { "individualsAsked": item.individualsAsked, "individualsAnswered": item.individualsAnswered } });

            var data = {
                labels:
                    userName,
                series: [
                    {
                        label: 'queans1',
                        values: questionsAskedAnswer
                    },
                    {
                        label: 'queans2',
                        values: individualsAskedAnswer
                    },]
            };

            var chartWidth = 600,
                barHeight = 20,
                groupHeight = barHeight * data.series.length,
                gapBetweenGroups = 30,
                spaceForLabels = 150,
                spaceForLegend = 150;
            var onboardedval = 0;

           
            // Zip the series data together (first values, second values, etc.)
            var zippedData = [];

            for (var i = 0; i < data.labels.length; i++) {
                for (var j = 0; j < data.series.length; j++) {
                    zippedData.push(data.series[j].values[i]);
                }
            }

            console.log(zippedData);
            var color = d3.scale.ordinal().range(["#FF6308", "#004165"]);
            var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;
            var multiplywidth = chartWidth / d3.max(zippedData).questionsAsked;
            if (chartHeight > 340) { $(".chart").css("overflow-y", "scroll").css("height", "340px"); }
            else { $(".chart").removeAttr("style") }

            
            var x = d3.scale.linear()
                .domain([0, d3.max(zippedData).questionsAsked])
                .range([0, chartWidth]);

            var y = d3.scale.linear()
                .range([chartHeight + gapBetweenGroups, 0]);

            var yAxis = d3.svg.axis()
                .scale(y)
                .tickFormat('')
                .tickSize(0)
                .orient("left");

            var chart = d3.select(".chart").append("svg")
                .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                .attr("height", chartHeight)
                .style("padding", "1px").style("padding-left", "183").style("padding-top", "16px");

            // Create bars
            var bar = chart.selectAll("g")
                .data(zippedData)
                .enter().append("g")
                .attr("transform", function (d, i) {
                    return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i / data.series.length))) + ")";
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return 110; })
                .attr("y", (barHeight / 2) + 10)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % 2 == 0) {
                        onboardedval = 0;
                    }
                    onboardedval += d;
                    if (onboardedval == 0) {
                        if (i % 2 == 0) return "";
                    }
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return -5; })
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .html(function (d, i) {
                    if (i % data.series.length === 0)
                        return "Question Asked/Replied";
                    if (i % data.series.length === 1)
                        return "People asked/Replied";
                });

            bar.append("rect")
                .attr("fill", function (d, i) { return color(i % data.series.length); })
                .attr("class", "bar")
                .attr("width", function (d, i) {
                    if (i % 2 == 0) return (d.questionsAnswered * multiplywidth)
                    else return (d.individualsAnswered * multiplywidth);
                })
                .attr("x", 0)
                .style('stroke', function (d, i) { return "#000000"; })
                .attr("height", barHeight - 1);

            // Add text label in bar
            bar.append("text").attr("x", function (d, i) {
                if (i % 2 == 0) {
                    if (d.questionsAnswered > 0) return ((d.questionsAnswered * multiplywidth - 13))
                    else return ((d.questionsAsked * multiplywidth - 13))
                }
                else {
                    if (d.individualsAnswered > 0) return ((d.individualsAnswered * multiplywidth - 13))
                    else return ((d.individualsAsked * multiplywidth - 13))
                }
            })

                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % 2 == 0) return d.questionsAnswered
                    else return d.individualsAnswered;
                });

            //question asked
            bar.append("rect")
                .attr("fill", function (d, i) { return "#fff" })
                .attr("class", "bar")
                .attr("width", function (d, i) {
                    if (i % 2 == 0) return ((d.questionsAsked - d.questionsAnswered) * multiplywidth)
                    else return ((d.individualsAsked - d.individualsAnswered) * multiplywidth);
                })
                .attr("x", function (d, i) {
                    if (i % 2 == 0) return (d.questionsAnswered * multiplywidth)
                    else return (d.individualsAnswered * multiplywidth);
                })
                .style('stroke', function (d, i) { return "#333"; })
                .attr("height", barHeight - 1);

            bar.append("text").attr("x", function (d, i) {
                if (i % 2 == 0) return (d.questionsAsked * multiplywidth)
                else return (d.individualsAsked * multiplywidth);
            })

                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("fill",
                    function (d) {
                        if (d.questionsAsked > 0 && d.questionsAsked != d.questionsAnswered) {
                            return "black";
                        } else if (d.individualsAsked > 0 && d.individualsAsked != d.individualsAnswered) {
                            return "black";
                        }
                    })
                .text(function (d, i) {

                    if (i % 2 == 0)
                        if (d.questionsAnswered == 0) return "0/" + (d.questionsAsked)
                        else {
                            if (d.questionsAnswered != d.questionsAsked) return (d.questionsAsked)
                            else return "/" + (d.questionsAsked)
                        }
                    else
                        if (d.individualsAnswered == 0) return "0/" + (d.individualsAsked)
                        else {
                            if (d.individualsAnswered != d.individualsAsked) return (d.individualsAsked)
                            else return "/" + (d.individualsAsked)
                        }
                    return "/" + (d.individualsAsked)
                });

            bar.append("foreignObject").append("xhtml:body")
                .attr("class", "innerNode")
                .style("width", "200px")
                .style("margin-left", "-500px")
                .style("font-weight", "bold")
                .attr("x", function (d) { return - 150; })
                .attr("y", groupHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % data.series.length === 0)
                        return data.labels[Math.floor(i / data.series.length)];
                    else return ""
                });

            chart.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + spaceForLabels + ", 0)")
                .call(yAxis).selectAll(".tick text")
            //.call(wrap, 40);

            var xAxisSvg = d3.select('.xaxis').append('svg')
                .attr("width", chartWidth)
                .attr("height", 20)
                .attr("margin-left", "-100px")
                .append("g");

            var xAxisDom = xAxisSvg.selectAll('.x.axis')
            if (xAxisDom.empty()) {
                xAxisDom = xAxisSvg.append("g")
                    .attr("class", "x axis")
            }
            xAxisDom
                .attr("transform", "translate(" + (174) + "," + 1 + ")")
                .call(d3.svg.axis().scale(x).orient("bottom").tickFormat(function (d, i) {
                    return d;
                }));
        }

        function makeSummaryFeedChart(data) {
            debugger;
            $(".summarychart").empty();
            $(".summaryxaxis").empty();
            
            var questionsAskedAnswer = { 'questionsAsked': $scope.Col1, 'questionsAnswered': $scope.Col2};
            var individualsAskedAnswer = { "individualsAsked": $scope.Col3, "individualsAnswered": $scope.Col4 };
            var data = {
                labels:
                    ["Overall Summary"],
                series: [
                    {
                        label: 'queans1',
                        values: [questionsAskedAnswer]
                    },
                    {
                        label: 'queans2',
                        values: [individualsAskedAnswer]
                    },]
            };

            var chartWidth = 600,
                barHeight = 20,
                groupHeight = barHeight * data.series.length,
                gapBetweenGroups = 30,
                spaceForLabels = 150,
                spaceForLegend = 150;
            var onboardedval = 0;
            // Zip the series data together (first values, second values, etc.)
            var zippedData = [];

            for (var i = 0; i < data.labels.length; i++) {
                for (var j = 0; j < data.series.length; j++) {
                    zippedData.push(data.series[j].values[i]);
                }
            }

            console.log(zippedData);
            var color = d3.scale.ordinal().range(["#FF6308", "#004165"]);
            var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;
            var multiplywidth = chartWidth / d3.max(zippedData).questionsAsked;
            var x = d3.scale.linear()
                .domain([0, d3.max(zippedData).questionsAsked])
                .range([0, chartWidth]);

            var y = d3.scale.linear()
                .range([chartHeight + gapBetweenGroups, 0]);

            var yAxis = d3.svg.axis()
                .scale(y)
                .tickFormat('')
                .tickSize(0)
                .orient("left");

            var chart = d3.select(".summarychart").append("svg")
                .attr("width", spaceForLabels + chartWidth + spaceForLegend)
                .attr("height", chartHeight)
                .style("padding", "1px").style("padding-left", "174").style("padding-top", "16px");

            // Create bars
            var bar = chart.selectAll("g")
                .data(zippedData)
                .enter().append("g")
                .attr("transform", function (d, i) {
                    return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i / data.series.length))) + ")";
                });

            // chart.call(tip);

            //question answered
            debugger;
            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return 110; })
                .attr("y", (barHeight / 2) + 10)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % 2 == 0) {
                        onboardedval = 0;
                    }
                    onboardedval += d;
                    if (onboardedval == 0) {
                        if (i % 2 == 0) return "";
                    }
                });

            bar.append("text")
                .attr("class", "label")
                .attr("x", function (d) { return -5; })
                .attr("y", barHeight / 2)
                //.attr("fill", "red")
                .attr("dy", ".35em")
                .html(function (d, i) {
                    if (i % data.series.length === 0)
                        return "Question Asked/Replied";
                    if (i % data.series.length === 1)
                        return "People asked/Replied";
                });
            bar.append("rect")
                .attr("fill", function (d, i) { return color(i % data.series.length); })
                .attr("class", "bar")
                .attr("width", function (d, i) {
                    if (i % 2 == 0) return (d.questionsAnswered * multiplywidth)
                    else return (d.individualsAnswered * multiplywidth);
                })
                .attr("x", 0)
                .style('stroke', function (d, i) { return "#000000"; })
                .attr("height", barHeight - 1);

            // Add text label in bar
            bar.append("text").attr("x", function (d, i) {
                if (i % 2 == 0) {
                    if (d.questionsAnswered > 0) return ((d.questionsAnswered * multiplywidth - 13))
                    else return ((d.questionsAsked * multiplywidth - 13))
                }
                else return ((d.individualsAnswered * multiplywidth - 13));
            }).attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % 2 == 0) return d.questionsAnswered
                    else return d.individualsAnswered;
                });

            //bar for question asked
            bar.append("rect")
                .attr("fill", function (d, i) { return "#fff" })
                .attr("class", "bar")
                .attr("width", function (d, i) {
                    if (i % 2 == 0) return ((d.questionsAsked - d.questionsAnswered) * multiplywidth)
                    else return ((d.individualsAsked - d.individualsAnswered) * multiplywidth);
                })
                .attr("x", function (d, i) {
                    if (i % 2 == 0) return (d.questionsAnswered * multiplywidth)
                    else return (d.individualsAnswered * multiplywidth);
                })
                .style('stroke', function (d, i) { return "#333"; })
                .attr("height", barHeight - 1);

            bar.append("text").attr("x", function (d, i) {
                if (i % 2 == 0) return (d.questionsAsked * multiplywidth)
                else return (d.individualsAsked * multiplywidth);
            }).attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("fill", "black")
                .text(function (d, i) {
                    if (i % 2 == 0)
                        if (d.questionsAnswered == 0) return "0/" + (d.questionsAsked)
                        else {
                            if (d.questionsAnswered != d.questionsAsked) return (d.questionsAsked)
                            else return "/" + (d.questionsAsked)
                        }
                    else
                        if (d.individualsAnswered == 0) return "0/" + (d.individualsAsked)
                        else {
                            if (d.individualsAnswered != d.individualsAsked) return (d.individualsAsked)
                            else return "/" + (d.individualsAsked)
                        }
                    return "/" + (d.individualsAsked)
                });


            bar.append("foreignObject").append("xhtml:body")
                .attr("class", "innerNode")
                .style("width", "200px")
                .style("margin-left", "-500px")
                .style("font-weight", "bold")
                .attr("x", function (d) { return - 150; })
                .attr("y", groupHeight / 2)
                .attr("dy", ".35em")
                .text(function (d, i) {
                    if (i % data.series.length === 0)
                        return data.labels[Math.floor(i / data.series.length)];
                    else
                        return ""
                });

            chart.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + spaceForLabels + ", 0)")
                .call(yAxis).selectAll(".tick text")
            //.call(wrap, 40);

            var xAxisSvg = d3.select('.summaryxaxis').append('svg')
                .attr("width", chartWidth)
                .attr("height", 20)
                .attr("margin-left", "-100px")
                .append("g");

            var xAxisDom = xAxisSvg.selectAll('.x.axis')
            if (xAxisDom.empty()) {
                xAxisDom = xAxisSvg.append("g")
                    .attr("class", "x axis")
            }
            xAxisDom
                .attr("transform", "translate(" + (174) + "," + 1 + ")").call(d3.svg.axis().scale(x).orient("bottom").tickFormat(function (d, i) {
                    return d;
                })
                );
        }
    }])