﻿'use strict';

/* Controllers */

angular.module('ProAct.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

    .controller('ProActController', ['$scope', 'dataFactory', '$http', '$rootScope', '$timeout', function ($scope, dataFactory, $http, $rootScope, $timeout) {

        var token = ""; var tabledata = []; var colname = ""; var arr = []; var objects = []; $scope.notstarted = []; $scope.arrcount = 0;
        $scope.formInfo = {};
        $scope.Col1 = 0;
        $scope.Col2 = 0;
        $scope.Col3 = 0;
        //$scope.limit = 10;
        var counter = 0;
        $scope.propertyName = 'Name';
        $scope.reverse = true;
        $scope.MainData = [];



        $scope.sortBy = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        if ($(".grpddl").val() == 1) {
            colname = "value.roleName";
        }
        else if ($(".grpddl").val() == 2) {
            colname = "value.managerName";
        }
        else if ($(".grpddl").val() == 4) {
            colname = "value.department";
        }
        else if ($(".grpddl").val() == 5) {
            colname = "value.location";
        }

        token = $rootScope.token;
        $scope.tableData = [];
        //$('#mydiv').hide(); 
        dataFactory.proAct(token)
            .then(function (response) {
                var data = response.data;
                $scope.MainData = data;
                localStorage.setItem("ProactData", JSON.stringify(response.data));
                //$('#mydiv').hide(); 
                $rootScope.$on("TemActsearchevent", function (event, message) {
                    debugger;
                    $scope.formInfo = message;
                    var data = $scope.MainData;//JSON.parse(localStorage.getItem("ProactData"));
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username, $scope.formInfo.Loc,  $scope.formInfo.Dept); 
                    console.log(data1);

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    counter = 0;
                    localStorage.setItem("ProactData", JSON.stringify(data1));
                    var limitdata = appendData(counter, 0);
                    counter = counter + 10;

                    tabledata = [];
                    $.each(limitdata, function (index, value) {
                        tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer]);
                    });
                    addTable(tabledata);
                    addanothertbl(tabledata);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["even", "odd"]
                        }
                    }).find('tr:nth-child(even)')
                        .css('background-color', 'white')
                        .end()
                        .find('tr:nth-child(odd)')
                        .css('background-color', 'rgb(212,227,232)')
                        .end();
                  

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    createChart(limitdata);
                    /*** Re-draw chart ***/


                });

                $rootScope.$on("TemActclearevent", function (event, message) {
                    debugger;
                    $scope.formInfo = message;
                    var data = $scope.MainData;
                    var data1 = filter(data, $scope.formInfo.Manager, $scope.formInfo.Role, $scope.formInfo.Aspr, $scope.formInfo.Username, $scope.formInfo.Loc, $scope.formInfo.Dept);
                    console.log(data1);

                    data1 = (data1.length == 0 ? data : data1);
                    localStorage.setItem("ProactData", JSON.stringify(data1));

                    $(".leftsidebar").toggleClass('active');
                    $(".leftsidebar").toggleClass('hide');
                    $('#Tsidebar').toggleClass('active');
                    $('#sidebarCollapse').toggleClass('active');
                    //$("#tabletab").toggleClass('active');

                    /***  Re-draw table ***/
                    counter = 0;
                    var limitdata = appendData(counter, 0);
                    counter = counter + 10;
                    tabledata = [];

                    $.each(limitdata, function (index, value) {
                        tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer]);
                    });
                    addTable(tabledata);
                    addanothertbl(tabledata);
                    $("#myTable").tablesorter({
                        theme: 'blue',
                        showProcessing: true,
                        widgets: ["zebra"],
                        widgetOptions: {
                            zebra: ["even", "odd"]
                        }
                    }).find('tr:nth-child(even)')
                        .css('background-color', 'white')
                        .end()
                        .find('tr:nth-child(odd)')
                        .css('background-color', 'rgb(212,227,232)')
                        .end();
                    

                    /*** Re-draw table ***/

                    /*** Re-draw chart ***/
                    createChart(limitdata);
                    /*** Re-draw chart ***/


                });

                
                var data = appendData(counter, 0);
                counter = counter + 10;


                $.each(data, function (index, value) {
                    tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer]);
                });
                

                addTable(tabledata);
                addanothertbl(tabledata);
                
                $("#myTable").tablesorter({
                    theme: 'blue',
                    showProcessing: true,
                    widgets: ["zebra"],
                    widgetOptions: {
                        zebra: ["even", "odd"]
                    }
                }).find('tr:nth-child(even)')
                    .css('background-color', 'white')
                    .end()
                    .find('tr:nth-child(odd)')
                    .css('background-color', 'rgb(212,227,232)')
                    .end();
                

                //$("#charttab").click(function () {
                //    var serviceArray = new Array("Group By", "Manager", "Department", "Location");
                //    $('.grpddl').empty();
                //    for (var i = 0; i < serviceArray.length; i++) {
                //        var data = '<option value=' + i + '>' + serviceArray[i] + '</option>'
                //        $('.grpddl').append(data);
                //    }
                //});

                //$("#tabletab").click(function () {
                //    var serviceArray = new Array("Group By", "Role Name", "Manager Name", "User Profile", "Department", "Location");
                //    $('.grpddl').empty();
                //    for (var i = 0; i < serviceArray.length; i++) {
                //        var data = '<option value=' + i + '>' + serviceArray[i] + '</option>'
                //        $('.grpddl').append(data);
                //    }
                //});

                
                $(".grpddl").change(function () {
                    
                    
                    if ($(".grpddl").val() == 1) {
                         var sort = [[1, 0]];
                        $("#myTable").trigger("sorton", [sort]);

                        $(function () {
                            $('#myTable')
                                .on('sortEnd', function () {
                                    var currentSort = this.config.sortList,
                                        firstColumn = currentSort[0][0],
                                        sortedcol = this.config.headerList[firstColumn].innerHTML,
                                        sortDir = 'Ascending';
                                    console.log('column = ' + sortedcol, ', direction = ' + sortDir);
                                    debugger;
                                    var JSONtable = createJSON();
                                    console.log(JSONtable);
                                    JSONtable = JSON.parse(JSON.stringify(JSONtable).split('"Name":').join('"name":'));

                                    //console.log(JSON.stringify(JSONtable));
                                    createChart(JSONtable.Key);

                                })
                        });

                        return false;
                    }
                    
                });

                console.log("ChartDATA", data);
                createChart(data);
                
                $('#mydiv').hide();

            }, function (error) {
                $('#mydiv').hide();
                alert('ProActivity error');
            });

        //}, function (error) {
        //    alert('Calling error');
        //}),3000);


        function addTable(data) {
            var myTableDiv = document.getElementById("ActTbl")
            //myTableDiv.innerHTML = "";
            if (data.length > 0) {
                $('#myTable').remove();
                $("#btnSubmit").show();
                $("#btnAll").show();
                $("#btnLess").hide();
                var table = document.createElement('TABLE')
                var tableheader = document.createElement('THEAD')
                var tableBody = document.createElement('TBODY')
                var tablefoot = document.createElement('TFOOT')
                

                table.border = '1'
                table.className = "tablesorter";
                table.id = "myTable";
                table.appendChild(tableheader);
                
                 table.appendChild(tableBody);

                var heading = new Array();
                heading[0] = "Name"
                heading[1] = "Manager"
                heading[2] = "Assessed by Both"
                heading[3] = "Areas moderated"
                heading[4] = "% of moderation complete"
                //heading[5] = "totalStarted"
                //heading[6] = "totalAssessed"
                //heading[7] = "userID"

                var Hover = new Array();
                Hover[0] = "Name"
                Hover[1] = "Manager"
                Hover[2] = "Assessed by Both"
                Hover[3] = "Areas moderated"
                Hover[4] = "% of moderation complete"

                //TABLE COLUMNS
                var tr = document.createElement('TR');
                tableheader.appendChild(tr);
                for (var i = 0; i < heading.length; i++) {
                    var th = document.createElement('TH')
                    th.setAttribute('data-title', Hover[i]);
                    th.appendChild(document.createTextNode(heading[i]));
                    tr.appendChild(th);
                }
                table.appendChild(tableBody);
                //TABLE ROWS
                for (var i = 0; i < data.length; i++) {
                    tr = document.createElement('TR');
                    for (var j = 0; j < data[i].length; j++) {
                        var td = document.createElement('TD')
                        td.appendChild(document.createTextNode(data[i][j]));
                        tr.appendChild(td)
                    }
                    
                    tableBody.appendChild(tr);
                }

                //TABLE FOOTER
                table.appendChild(tablefoot);
                var footer = new Array();
                footer[0] = ""
                footer[1] = ""
                footer[2] = "Total:"
                footer[3] = "Total:"
                footer[4] = "Total:"

                //TABLE COLUMNS
                tr = document.createElement('TR');
                tr.className = "grandtotal";
                tablefoot.appendChild(tr);
                for (var i = 0; i < footer.length; i++) {
                    var td = document.createElement('TD');
                    td.className = "total";
                    td.appendChild(document.createTextNode(footer[i]));
                    tr.appendChild(td);
                }
                
                myTableDiv.insertBefore(table, myTableDiv.childNodes[0]);

                //$('#myTable thead th').eq(5).hide();
                //$('#myTable thead th').eq(6).hide();

                $('#myTable tbody tr').each(function (i) {
                    calculateColumn(i);
                });
             
                
                $('#myTable thead th').each(function (i) {
                    calculateColumn(i);
                });
                
            }
            else {
                $('#myTable').remove();
                $("#btnSubmit").hide();
                $("#btnAll").hide();
                $("#btnLess").hide();
            }

            function calculateColumn(index) {
                var total = 0;
                $('#myTable tbody tr').each(function () {
                    
                    var value = parseFloat($('td', this).eq(index).text());
                    if (!isNaN(value)) {
                        total += value;
                    }
                });
                if (index != 0) {
                    if (index == 1) {
                        $('#myTable tfoot .grandtotal td').eq(index).text("");
                    }
                    else if (index == 4) {
                        $('#myTable tfoot .grandtotal td').eq(index).text((total).toFixed(1) + '%');
                       // $scope.Col3 = parseInt(total) + '%';
                    }
                    else {
                        $('#myTable tfoot .grandtotal td').eq(index).text(parseInt(total));
                        //$scope.Col1 = parseInt(total);
                        //$scope.Col2 = parseInt(total);
                    }

                }
                else {
                    $('#myTable tfoot .grandtotal td').eq(index).text("Summary of Table");
                    $('#myTable tfoot .grandtotal td').eq(index).css('font-weight', 'bold');
                }
            }
        }
        
        function addanothertbl(data) {
            if (data.length > 0) {
                $('.tableparent').empty();
                var table = $('.tableparent')
                var tr = $('<tr>');

                $('<th  rowspan="2" colspan="0" data-title="Overall Summary" class="first-child">').html("Overall Summary").appendTo(tr);

                $('<th data-title="Assessed by Both" style="width:19.5%;">').html("Assessed by Both").appendTo(tr);
                $('<th data-title="Areas moderated" style="width:19.5%;">').html("Areas moderated").appendTo(tr);
                $('<th data-title="% of moderation complete">').html("% of moderation complete").appendTo(tr);
                table.append(tr);

                var a = 0; var b = 0; var c = 0; var d = 0; var e = []; var f = []; var g = 0.0;
                $.each(data, function (i, item) {
                    //a += item[0]
                    //b += item[1]
                    c += item[2]
                    d += item[3]
                    g += parseFloat(item[4].replace('%', ''))
                });
                tr = $('<tr style="background-color: rgb(212, 227, 232);">');
                $scope.Col1 = c;
                $scope.Col2 = d;
                $scope.Col3 = g;

                $('<td>').html(c).appendTo(tr);
                $('<td>').html(d).appendTo(tr);
                $('<td>').html(g.toFixed(1) + '%').appendTo(tr);
                table.append(tr);
            }
            else {
                $('.tableparent').empty();
            }
        }

        $("#btnSubmit").button().click(function () {
            debugger;
            $scope.arrcount = 1;
            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 10;
            $.each(data, function (index, value) {
                tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer, value.totalStarted, value.totalAssessed]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
            createChart(data);
        });
        $("#btnAll").button().click(function () {
            debugger;
            tabledata = [];
            counter = 0;
            var data = appendData(counter, 1);
            $.each(data, function (index, value) {
                tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
           
            createChart(data);
            $("#btnLess").show();
            $("#btnAll").hide();
        });

        $("#btnLess").button().click(function () {
            $("#btnLess").hide();
            $("#btnAll").show();

            tabledata = [];
            var data = appendData(counter, 0);
            counter = counter + 10;
            $.each(data, function (index, value) {
                tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, value.moderatedPer]);
            });
            addTable(tabledata);
            addanothertbl(tabledata);
            $("#myTable").tablesorter({
                theme: 'blue',
                showProcessing: true,
                widgets: ["zebra"],
                widgetOptions: {
                    zebra: ["normal-row", "alt-row"]
                }
            });
            
            createChart(data);

        });

        function appendData(counter, flag) {
            var html = '';
            // $('.table-striped').empty();
            arr = [];
            var data = JSON.parse(localStorage.getItem("ProactData"));
            var length = Object.keys(data).length;
            //alert(length);

            var j = 0;
            if (flag == 0) j = counter + 10;
            else if (flag == 2) { j = length; counter = 0; }
            else j = length;

            //addTable(data);

            var table = $("<table class='table-striped'>");
            if (counter == 0) {
                var tr = $('<tr>');
                $('<th>').html("Name").appendTo(tr);
                $('<th>').html("Manager").appendTo(tr);
                $('<th>').html("Assessed by Both").appendTo(tr);
                $('<th>').html("Areas moderated").appendTo(tr);
                $('<th>').html("% of moderation complete").appendTo(tr);
                $('.table-striped').append(tr);
            }
        //    $.each(data, function (index, value) {
        //    tabledata.push([value.name, value.managerName, value.areasAssessedNum, value.areasModeratedNum, Math.round((value.areasModeratedNum / (value.totalModerated == 0 ? 1 : value.totalModerated)) * 100).toFixed(1) + '%']);
            //});
            for (var k = 0; k < j && k < length; k++) {

                var tr = $('<tr>');
                var name = data[k].name;
                var managerName = data[k].managerName;
                var areasAssessedNum = data[k].areasAssessedNum;
                var areasModeratedNum = data[k].areasModeratedNum;
                var moderatedPer = ((data[k].areasModeratedNum / (data[k].totalModerated == 0 ? 1 : data[k].totalModerated)) * 100).toFixed(1) + '%';

                $('<td>').html(name).appendTo(tr);
                $('<td>').html(managerName).appendTo(tr);
                $('<td>').html(areasAssessedNum).appendTo(tr);
                $('<td>').html(areasModeratedNum).appendTo(tr);
                $('<td>').html(moderatedPer).appendTo(tr);
                $('.table-striped').append(tr);
                if (counter >= length) $('#btnSubmit').val("No more data");

                arr.push({ 'name': data[k].name, 'managerName': data[k].managerName, 'areasAssessedNum': data[k].areasAssessedNum, 'areasModeratedNum': data[k].areasModeratedNum, 'moderatedPer': ((data[k].areasModeratedNum / (data[k].totalModerated == 0 ? 1 : data[k].totalModerated)) * 100).toFixed(1) + '%', 'totalStarted': data[k].totalStarted, 'totalAssessed': data[k].totalAssessed, 'userID': data[k].userID});

            }
            // counter++;
            console.log(arr);
            return arr;
        }

        

        function createJSON() {

            var myJSON = { Key: [] };

            var headers = $('#myTable th');
            $('#myTable tbody tr').each(function (i, tr) {
                var obj = {},

                    $tds = $(tr).find('td');

                headers.each(function (index, headers) {
                    obj[$(headers).text()] = $tds.eq(index).text();
                });

                myJSON.Key.push(obj);
            });

            //console.log(JSON.stringify(myJSON));
            return myJSON;
        }

        function createChart(data) {
            debugger;
            data.forEach(function (d) { // convert strings to numbers
                d.totalAssessed = +d.totalAssessed;
                d.totalStarted = +d.totalStarted;
                });
                makeVis(data);

            // var makeVis = function (data) {
            function makeVis(data) {
                d3.selectAll("#proActsvg > svg").remove();
                // Common pattern for defining vis size and margins
                var margin = { top: 20, right: 20, bottom: 30, left: 40 },
                    width = 960 - margin.left - margin.right,
                    height = 500 - margin.top - margin.bottom;

                // Add the visualization svg canvas to the vis-container <div>

                var canvas = d3.select("#proActsvg").insert("svg","#legend")//.append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", 550)//height + margin.top + margin.bottom
                    .attr("style", "float:left")
                    .attr("style","margin-left:-3.5%")
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                var config = {
                    "avatar_size": 25
                }

                var defs = canvas.append('svg:defs');
               
                // Define our scales
                var colorScale = d3.scale.category10();

                var xScale = d3.scale.linear()
                    .domain([d3.min(data, function (d) { return d.totalStarted; }) - 1,
                        d3.max(data, function (d) { return d.totalStarted; }) + 1])
                    .range([0, width]);

                var yScale = d3.scale.linear()
                    .domain([d3.min(data, function (d) { return d.totalAssessed; }) - 1,
                        d3.max(data, function (d) { return d.totalAssessed; }) + 1])
                    .range([height, 0]); // flip order because y-axis origin is upper LEFT

                // Define our axes
                
                var len = xScale.ticks().length;
                var xAxis = d3.svg.axis()
                    .scale(xScale)
                    .orient('bottom').tickFormat(function (d, i) {
                        return i == 0 ? "0%" : (i == (len - 1) ? "100%" : "");
                    });
                

                var yAxis = d3.svg.axis()
                    .scale(yScale)
                    .orient('left').tickFormat(function (d, i) {
                        return i == 0 ? "0%" : (i == 12 ? "100%" : "");
                    });
                

                // Add x-axis to the canvas
                canvas.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")") // move axis to the bottom of the canvas
                    .call(xAxis)
                    .append("text")
                    .attr("class", "label")
                    .attr("x", width/2) // x-offset from the xAxis, move label all the way to the right
                    .attr("y", 42)    // y-offset from the xAxis, moves text UPWARD!
                    .style("text-anchor", "end") // right-justify text
                    .style("font-size", "17px")
                    .text("Employees");

                // Add y-axis to the canvas
                canvas.append("g")
                    .attr("class", "y axis") // .orient('left') took care of axis positioning for us
                    .call(yAxis)
                    .append("text")
                    .attr("class", "label")
                    .attr("transform", "rotate(-90)") // although axis is rotated, text is not
                    //.attr("y", 15) // y-offset from yAxis, moves text to the RIGHT because it's rotated, and positive y is DOWN
                    .attr("y", -54)//0 - margin.left)
                    .attr("x", 0 - (height / 2))
                    .attr("dy", "1.5em")
                    .style("text-anchor", "end")
                    .style("font-size", "17px")
                    .text("Manager");

                canvas.append("text")
                    .attr("x", (width / 2))
                    .attr("y", 0 - ((margin.top / 2) - 2))
                    .attr("text-anchor", "middle")
                    .style("font-size", "17px")
                    .style("text-decoration", "underline")
                    .style("font-weight", "bold")
                    .text("Assessment Completed (%)");

                var gdots = canvas.selectAll("g.dot")
                    .data(data)
                    .enter().append('g');

                // Add the tooltip container to the vis container
                // it's invisible and its position/contents are defined during mouseover
               

                var tooltip = d3.select("#proActsvg")
                    .append("div")
                    .style("position", "absolute")
                    .style("z-index", "10")
                    .style("visibility", "hidden")
                    .text("a simple tooltip");

                $scope.notstarted = [];
                var xscaleMax = d3.max(data, function (d) { return d.totalStarted; });
                var yscaleMax = d3.max(data, function (d) { return d.totalAssessed; });
                console.log(xscaleMax, yscaleMax);

                

                data.forEach(function (d, i) {
                    
                    if ((d.totalStarted) > 0 || (d.totalAssessed) > 0) {
                        defs.append("svg:pattern")
                            .attr("id", "grump_avatar" + i)
                            .attr("width", config.avatar_size)
                            .attr("height", config.avatar_size)
                            //.attr("patternUnits", "userSpaceOnUse")
                            .append("svg:image")
                            .attr("xlink:href", "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID)
                            .attr("width", 35)
                            .attr("height", 35)
                            .attr("x", 0)
                            .attr("y", 0);
                        // Add data points!
                        var circle = gdots.append("circle")
                            //.attr("transform", "translate(" + d.totalStarted + "," + d.totalAssessed + ")")
                            .attr("r", 15) // radius size, could map to another data dimension
                            .attr("cx", xScale(d.totalStarted))     // x position
                            .attr("cy", yScale(d.totalAssessed) - 10)  // y position
                            //.style("fill", "#fff")
                            .style("fill", "url(#grump_avatar" + i + ")")
                            .on("mouseover", function () { return tooltip.style("visibility", "visible"), tooltip.text(d.name); })
                            .on("mousemove", function () { return tooltip.style("top", (d3.event.pageY - 221) + "px").style("left", (d3.event.pageX - 15) + "px"); })
                        .on("mouseout", function () { return tooltip.style("visibility", "hidden"); });
                        
                        //gdots.append("foreignObject")
                        //    .attr("class", "innerNode")
                        //    .attr("x", function (d) {
                        //        if (d.totalStarted == xscaleMax && d.totalAssessed == yscaleMax) {
                        //            return xScale(d.totalStarted) - 53;
                        //        }
                        //        else {
                        //            return xScale(d.totalStarted)-48;
                        //        }
                                
                        //    })
                        //    .attr("y", function (d) {
                        //        if (d.totalStarted == xscaleMax && d.totalAssessed == yscaleMax) {
                        //            return yScale(d.totalAssessed) - 28;
                        //        }
                        //        else {
                        //            return yScale(d.totalAssessed) - 50;
                        //        }
                                
                        //    })
                            
                        //    .attr("font-size", "10.5px")  // Font size
                        //    .attr("fill", "black")   // Font color
                        //    .text(function (d) {
                        //        if ((d.totalStarted) > 0 || (d.totalAssessed) > 0) {
                        //            return d.name;
                        //        }
                        //    });
                        
                    }
                    else {
                        //console.log((d.totalStarted), (d.totalAssessed));
                        if ($scope.arrcount == 0) {
                            $scope.notstarted.push({ 'Name': d.name, 'Url': "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID });
                        }
                        else {
                            $scope.$apply($scope.notstarted.push({ 'Name': d.name, 'Url': "https://dev.pobi.co.uk/api/api/UserProfiles/Picture/" + d.userID }));
                        }
                        
                        console.log($scope.notstarted);
                    }
                })
               
                
                
            };
        }

        function filter(data1, manager, role, aspiration, username, loc, dept) {
            debugger;
            var check = 0, result = [];
            if (manager != "" && manager != undefined) {
                check = 1;
                result = data1.filter(function (item) { return item.managerName == manager });
            }

            if (role != "" && role != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.roleName == role });
            }

            if (aspiration != "" && aspiration != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.roleName == aspiration });
            }

            if (username != "" && username != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.name == username });
            }

            if (loc != "" && loc != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.location == loc });
            }

            if (dept != "" && dept != undefined) {
                if (check == 1) {
                    data1 = result;
                }
                check = 1;
                result = data1.filter(function (item) { return item.department == dept });
            }

            return result;
        }
    }])
