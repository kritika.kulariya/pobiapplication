﻿'use strict';

/* Controllers */

var module = angular.module('sysuse.controllers', [])
    .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    });

module.controller('SystemUseController', ['$scope', 'dataFactory', '$rootScope', function ($scope, dataFactory, $rootScope) {
    var arrArea = new Array();
    var tabledata = [];
    var token = "";
    var arr = [];
    var objects = [];
    $scope.Col1 = 0;
    $scope.Col2 = 0;
    $scope.Col3 = 0;
    $scope.Col4 = 0;
    $scope.Col5 = 0;
    $scope.Col6 = 0;
    $scope.MainData = [];
    $scope.roleList = [];

    $scope.propertyName = 'Name';
    $scope.reverse = true;
    $scope.counter = 10;



    $scope.sortBy = function (propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };


    var data = $.param({
        username: "insights@pobi.co.uk",
        password: "Xd2actQu74pB$",
        grant_type: "password"
    });
    var config = {
        async: false
    };

    $scope.formInfo = {
        Username: '',
        Manager: '',
        Dept: '',
        Aspr: '',
        Loc: '',
        frmDate: '',
        toDate: '',
        Role: ''
    };

    $scope.filterTable = function (formInfo) {
        debugger;

        //var filteredData = $scope.MainData.filter(function (el) {
        //    return el.name === ((formInfo.Username != "") ? formInfo.Username : 0) ||
        //        el.roleName === ((formInfo.Role != "") ? formInfo.Role : 0) ||
        //        el.managerName === ((formInfo.Manager != "") ? formInfo.Manager : 0);
        //});

        if (formInfo.Manager != undefined || formInfo.Role != undefined || formInfo.Aspr != undefined || formInfo.Username != undefined) {
            var filteredData = filter($scope.MainData, formInfo.Manager, formInfo.Role, formInfo.Aspr, formInfo.Username);
            console.log(filteredData);
            $scope.tableData = filteredData;
        }
        else {
            console.log(filteredData);
            $scope.tableData = $scope.MainData;
        }
        

        

    };
    
    function filter(data1, manager, role, aspiration, username) {
        debugger;
        var check = 0,result = [];
        if (manager != "" && manager != undefined) {
            check = 1;
            result = data1.filter(function (item) { return item.managerName == manager });
        }
            
        if (role != "" && role != undefined) {
            if (check == 1) {
                data1 = result;
            }
            check = 1;
            result = data1.filter(function (item) { return item.roleName == role });
        }

        if (aspiration != "" && aspiration != undefined) {
            if (check == 1) {
                data1 = result;
            }
            check = 1;
            result = data1.filter(function (item) { return item.roleName == aspiration });
        }

        if (username != "" && username != undefined) {
            if (check == 1) {
                data1 = result;
            }
            check = 1;
            result = data1.filter(function (item) { return item.name == username });
        }

        return result;
    }
    


    //$("#btnapply").button().click(function () {

    //event.preventDefault();
    //var data = JSON.parse(localStorage.getItem("dataSystemUse"));
    //console.log(data);
    //var username = $('#usertxt').val();
    //var managername = $('#managertxt').val();
    //var rolename = $("#rolelistdata option:selected").text();

    //if (managername != "" || username != "" || rolename != "") {
    //    data = jQuery.grep(data, function (product, i) {
    //        if (managername != "")
    //            return product.managerName == managername;
    //        if (username != "")
    //            return product.name == username;
    //        if (rolename != "")
    //            return product.roleName == rolename;
    //    });
    //}
    //localStorage.setItem("dataSystemUse", JSON.stringify(data));
    //tabledata = [];
    //$.each(data, function (index, value) {
    //    tabledata.push([value.name, value.roleName, value.managerName, convertDate(value.firstLogon), convertDate(value.lastLogon), value.totalLogins, (value.averageUsesPerWeek).toFixed(2), ConvertTime(value.averageTimeUsed), ConvertTime(value.lastSessionDuration)]);
    //});
    //addnewTable(tabledata);
    //addanothertbl(data);
    //// addTable(data);
    ////addOtherTable(data);
    //});
    $scope.tableData = [];

    token = $rootScope.token;
    dataFactory.SysUse(token)
        .then(function (response) {
                var data = response.data;
                $scope.MainData = data;
                $scope.tableData = $scope.MainData.slice(0, $scope.counter);

                localStorage.setItem("dataSystemUse", JSON.stringify(data));
                var roles = [];
                $scope.MainData.map(function (x) {
                    if (roles.indexOf(x.roleName) === -1) {
                        roles.push(x.roleName);
                    }
                });
                $scope.roleList = roles;

                //addnewTable(tabledata);
                addanothertbl(data);
                $("#mydiv").hide();
        }, function (error) {
            $("#mydiv").hide();
            alert('System use error');
        });

    //$http.post("https://dev.pobi.co.uk/api/Token", data).then(function (response) {

    //    token = response.data.access_token;
    //    $http({
    //        method: 'GET',
    //        url: 'https://dev.pobi.co.uk/api/api/Insights/Usage/SystemUse',
    //        async: false,
    //        headers: {
    //            'Authorization': 'Bearer ' + token
    //        }
    //    }).then(function (response) {

    //        var data = response.data;
    //        $scope.MainData = data;
    //        $scope.tableData = $scope.MainData.slice(0, $scope.counter);

    //        localStorage.setItem("dataSystemUse", JSON.stringify(data));
    //        var roles = [];
    //        $scope.MainData.map(function (x) {
    //            if (roles.indexOf(x.roleName) === -1) {
    //                roles.push(x.roleName);
    //            }
    //        });
    //        $scope.roleList = roles;

    //        addnewTable(tabledata);
    //        addanothertbl(data);
    //    });
    //});


    $('#btnLoadMore').click(function () {
        $scope.counter += 10;
        $scope.$apply(function () {
            $scope.tableData = $scope.MainData.slice(0, $scope.counter);
        });
    });


    $('#btnLoadAll').click(function () {
        $scope.$apply(function () {
            $scope.tableData = $scope.MainData;
        });
    });


    function ConvertTime(totalSeconds) {
        // const formatted = moment.utc(val * 1000).format('HH:mm:ss');
        var hours = Math.floor(totalSeconds / 3600);
        var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
        var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

        // round seconds
        seconds = Math.round(seconds * 100) / 100
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);
        return result;
    }

    function ConvertToseconds(time) {
        var a = time.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
        return seconds;
    }

    function addanothertbl(data) {
        $('.tableparent').empty();
        var table = $('.tableparent');
        var tr = $('<tr>');

        $('<th  rowspan="2"  title="Overall Summary" class="first-child">').html("Overall Summary").appendTo(tr);

        $('<th title="1st Accessed" class="otherchild">').html("1st Accessed").appendTo(tr);
        $('<th title="Last Accessed" class="otherchild1">').html("Last Accessed").appendTo(tr);
        $('<th title="Total Times Used" class="otherchild2">').html("Total Times Used").appendTo(tr);
        $('<th title="Avg. Uses Per Week" class="otherchild1">').html("Avg. Uses Per Week").appendTo(tr);
        $('<th title="Avg. Session Time" class="otherchild1">').html("Avg. Session Time").appendTo(tr);
        $('<th title="Last Session Time">').html("Last Session Time").appendTo(tr);
        table.append(tr);

        var a = 0; var b = 0; var c = 0; var d = 0; var e = []; var f = []; var g = 0;
        $.each(data, function (i, item) {
            a += item.totalLogins
            b += item.totalTimeUsed
            c += item.averageUsesPerWeek
            d += item.averageTimeUsed
            g += item.lastSessionDuration
            e.push(new Date(item.firstLogon));
            f.push(new Date(item.lastLogon));
        });
        //var dates = data.map(function (x) { return new Date(x[4]); })
        var firstlogin = new Date(Math.min.apply(null, e));
        var lastlogin = new Date(Math.max.apply(null, f));
        tr = $('<tr class="number">');
        var firstCol = convertDate(firstlogin);
        var secondCol = convertDate(lastlogin);
        $scope.Col1 = a;
        var fourthCol = $scope.Col2 = c.toFixed(2);
        var fifthCol = $scope.Col3 = ConvertTime(d);
        var sixthCol = $scope.Col4 = ConvertTime(g);

        $scope.Col5 = firstCol;
        $scope.Col6 = secondCol;

        //$('<td>').html("").appendTo(tr);
        $('<td class="string">').html(firstCol).appendTo(tr);
        $('<td class="string">').html(secondCol).appendTo(tr);
        $('<td class="">').html(a).appendTo(tr);
        $('<td class="">').html(fourthCol).appendTo(tr);
        $('<td class="">').html(fifthCol).appendTo(tr);
        $('<td class="">').html(sixthCol).appendTo(tr);
        table.append(tr);
    }

    function convertDate(date) {
        var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        let d = new Date(Date.parse(date));

        var u = d.getDate() + ' ' + monthShortNames[d.getMonth()] + ' ' + d.getFullYear();
        return u;
    }

    function addOtherTable(data) {
        $('.tablestripedChild').empty()
        $('.tablestripedChild').css("display", "block");
        // $('.tablestripedChild').css("width", "100%");
        var table = $('.tablestripedChild')
        var tr = $('<tr>');
        var TotalTimesUsed = 3.33//data[0].totalLogins;
        $('<td style="width:900px">').html("<b>Total Times Used:</b> " + TotalTimesUsed).appendTo(tr);
        table.append(tr);

        tr = $('<tr>');
        var a = "100";
        $('<td style="width:90px">').html("<progress id='progressbar' style='width:100%' max=" + a + " value=" + TotalTimesUsed + "></progress>").appendTo(tr);
        table.append(tr);

        tr = $('<tr>');
        var UsesPerWeek = 2.44//data[0].averageUsesPerWeek;
        $('<td>').html("<b>Uses Per Week:</b> " + UsesPerWeek).appendTo(tr);
        table.append(tr);

        tr = $('<tr>');
        a = "100";
        $('<td>').html("<progress style='width:100%' max=" + a + " value=" + UsesPerWeek + "></progress>").appendTo(tr);
        table.append(tr);
    }

    function addTable(data) {
        $('.tablestriped').empty()
        var myTableDiv = document.getElementById("systemuse")
        var currentTr = $('<tr>');
        var table = $('.tablestriped')
        for (var k = 0; k < data.length; k++) {
            var tr = $('<tr>');
            var name = data[k].name;
            var Role = data[k].roleName;
            var FirstAccessed = data[k].firstLogon;
            var TotalTimesUsed = data[k].totalLogins;
            var AvgSessionTime = data[k].averageTimeUsed;


            $('<td>').html(name).appendTo(tr);
            $('<td>').html(Role).appendTo(tr);
            $('<td>').html("<b>First Accessed:</b> " + convertDate(FirstAccessed)).appendTo(tr);
            $('<td>').html("<b>Total Times Used:</b> " + TotalTimesUsed.toFixed(2)).appendTo(tr);
            $('<td>').html("<b>Avg Session Time:</b> " + AvgSessionTime).appendTo(tr);
            table.append(tr);

            tr = $('<tr>');
            var Manager = data[k].managerName;
            var LastAccessed = data[k].lastLogon;
            var UsesPerWeek = data[k].averageUsesPerWeek;
            var LastSessionTime = data[k].lastSessionDuration;
            $('<td>').html("").appendTo(tr);
            $('<td>').html("<b>Manager:</b> " + Manager).appendTo(tr);
            $('<td>').html("<b>Last Accessed: </b> " + convertDate(LastAccessed)).appendTo(tr);
            $('<td>').html("<b>Uses Per Week:</b> " + UsesPerWeek.toFixed(2)).appendTo(tr);
            $('<td>').html("<b>Last Session Time:</b> " + LastSessionTime).appendTo(tr);
            table.append(tr);
        }
    }

    function addnewTable(data) {
        //var myTableDiv = document.getElementById("systemuse");
        //myTableDiv.innerText = "";
        if (data.length > 0) {

            //var table = document.createElement('TABLE')
            //var tableheader = document.createElement('THEAD')
            //var tableBody = document.createElement('TBODY')

            //table.border = '1'
            //table.className = "tablesorter";
            //table.id = "myTable";
            //table.appendChild(tableheader);
            // table.appendChild(tableBody);
            var heading = new Array();
            heading[0] = "name"
            heading[1] = "Role"
            heading[2] = "Manager Name"
            heading[3] = "1st Accessed"
            heading[4] = "Last Accessed"
            heading[5] = "Total Times Used"
            heading[6] = "Avg. Uses Per Week"
            heading[7] = "Avg. Session Time"
            heading[8] = "Last Session Time"


            //var Hover = new Array();
            //Hover[0] = "Name"
            //Hover[1] = "Role"
            //Hover[2] = "Manager Name"
            //Hover[3] = "1st Accessed"
            //Hover[4] = "Last Accessed"
            //Hover[5] = "Total Times Used"
            //Hover[6] = "Avg. Uses Per Week"
            //Hover[7] = "Avg. Session Time"
            //Hover[8] = "Last Session Time"

            ////TABLE COLUMNS
            //var tr = document.createElement('TR');
            //tableheader.appendChild(tr);
            //for (var i = 0; i < heading.length; i++) {
            //    var th = document.createElement('TH')
            //    //th.width = '75';
            //    // th.id = 'facility_header';
            //    th.title = Hover[i];
            //    th.appendChild(document.createTextNode(heading[i]));
            //    tr.appendChild(th);
            //}
            //table.appendChild(tableBody);
            //TABLE ROWS
            //for (var i = 0; i < data.length; i++) {
            //    tr = document.createElement('TR');
            //    for (var j = 0; j < data[i].length; j++) {
            //        var td = document.createElement('TD')
            //        td.appendChild(document.createTextNode(data[i][j]));
            //        //  td.css("word-break: break-all");
            //        tr.appendChild(td)
            //    }
            //    tableBody.appendChild(tr);
            //}

            //var footer = new Array();
            //footer[0] = ""
            //footer[1] = ""
            //footer[2] = ""
            //footer[3] = ""
            //footer[4] = ""
            //footer[5] = "Total:"
            //footer[6] = "Total:"
            //footer[7] = "Total:"
            //footer[8] = "Total:"


            //TABLE COLUMNS
            //tr = document.createElement('TR');
            //tr.className = "grandtotal";
            //tableBody.appendChild(tr);
            //for (var i = 0; i < footer.length; i++) {
            //    td = document.createElement('TD');
            //    td.className = "total";
            //    td.appendChild(document.createTextNode(footer[i]));
            //    tr.appendChild(td);
            //}

            //myTableDiv.appendChild(table);
            $('.tablesorter thead th').each(function (i) {
                calculateColumn(i);
            });

            //myTableDiv.appendChild(table)
        }
        else {
            //myTableDiv.innerHTML = "No data";
        }

    }

    function calculateColumn(index) {
        var total = 0;
        $('.tablesorter tbody tr').each(function () {
            var value = "";
            if (index == 1 || index == 2 || index == 3 || index == 4 || index == 0) { return; }
            else {
                if (index == 8 || index == 7) {
                    value = $('td', this).eq(index).text();
                    value = ConvertToseconds(value);
                }
                else {
                    value = parseFloat($('td', this).eq(index).text());
                }
            }


            if (!isNaN(value)) {
                total += value;
            }

        });
        if (index != 0 && index != 1 && index != 2 && index != 3 && index != 4) {
            if (index == 7 || index == 8) {
                if (index == 7) {

                    $scope.Col1 = total;
                }
                if (index == 8) {
                    $scope.Col2 = total;
                }
                if (index == 9) {
                    $scope.Col3 = total.toFixed(2);
                }
                if (index == 10) {
                    $scope.Col4 = total.toFixed(2);
                }
                $('.tablesorter tbody .grandtotal td').eq(index).text();
                total = ConvertTime(total);
                $('.tablesorter tbody .grandtotal td').eq(index).text((total));
            }
            else {
                $('.tablesorter tbody .grandtotal td').eq(index).text((total.toFixed(2)));
            }
        }

        else {
            $('.tablesorter tbody .grandtotal td').eq(index).text("");
        }
    }

    function sortResults(prop, asc) {
        var SysData = JSON.parse(localStorage.getItem("dataSystemUse"));
        SysData = SysData.sort(function (a, b) {
            if (asc) {
                return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
            } else {
                return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
            }
        });
        tabledata = [];
        $.each(SysData, function (index, value) {
            tabledata.push([value.name, value.roleName, value.managerName, convertDate(value.firstLogon), convertDate(value.lastLogon), value.totalLogins, (value.averageUsesPerWeek).toFixed(2), ConvertTime(value.averageTimeUsed), ConvertTime(value.lastSessionDuration)]);
        });
        addnewTable(tabledata);
    }

    $("#Sortdata").change(function () {
        alert("test");
        var SortPara = $("#Sortdata option:selected").text();
        var para = "";
        if (SortPara == "Employee") para = "name";
        if (SortPara == "Role") para = "roleName";
        if (SortPara == "Manager") para = "managerName";
        sortResults(para, true);
    });
}]);


module.filter("mydate", function () {
    return function (x) {
        if (x) {
            var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

            let d = new Date(Date.parse(x));

            var u = d.getDate() + ' ' + monthShortNames[d.getMonth()] + ' ' + d.getFullYear();

            return u;
        }
        else return null;
    };
});

module.filter("ConvertTime", function () {
    return function (totalSeconds) {
        // 
        var hours = Math.floor(totalSeconds / 3600);
        var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
        var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

        // round seconds
        seconds = Math.round(seconds * 100) / 100
        var result = (hours < 10 ? "0" + hours : hours);
        result += ":" + (minutes < 10 ? "0" + minutes : minutes);
        result += ":" + (seconds < 10 ? "0" + seconds : seconds);
        return result;
    }
});
