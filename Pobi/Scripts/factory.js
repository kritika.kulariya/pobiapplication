﻿angular.module('Pobi')
    .factory('dataFactory', ['$http', function ($http) {

        var urlBase = 'https://dev.pobi.co.uk/api/';
        var dataFactory = {};


        dataFactory.getToken = function (data) {
            //$('#mydiv').show();
            return $http.post(urlBase + 'Token', data);
        };

        dataFactory.popularAreas = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/PopularAreas', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.systemUse = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/SystemUse', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.proAct = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/TeamActivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.getUserPic = function (userid) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api//UserProfiles/Picture/' + userid, { responseType: "blob" });
        }

        dataFactory.goalAct = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/teamgoalproactivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.outstAct = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/OutstandingActivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.FedbckUse = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/FeedbackUse', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.SysUse = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Usage/SystemUse', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        dataFactory.Capability = function (token) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api/Insights/Capability', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        return dataFactory;
    }]);
