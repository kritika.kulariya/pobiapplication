﻿angular.module('Pobi')
    .service('dataService', ['$http', function ($http) {

        var urlBase = 'https://dev.pobi.co.uk/api/';

        this.getToken = function (data) {
            return $http.post(urlBase + 'Token', data);
        };

        this.popularAreas = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/PopularAreas', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.systemUse = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/SystemUse', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.proAct = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/TeamActivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.getUserPic = function (userid) {
            $('#mydiv').show();
            return $http.get(urlBase + 'api//UserProfiles/Picture/' + userid, { responseType: "blob" });
        };

        this.goalAct = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/teamgoalproactivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.outstAct = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/OutstandingActivity', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.FedbckUse = function (token) {
            return $http.get(urlBase + 'api/Insights/Usage/FeedbackUse', { headers: { 'Authorization': 'Bearer ' + token } });
        };

        this.Capability = function (token) {
            return $http.get(urlBase + 'api/Insights/Capability', { headers: { 'Authorization': 'Bearer ' + token } });
        };

    }]);