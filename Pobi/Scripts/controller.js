﻿'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
})
    
    .controller('SearchController', ['$scope', 'dataFactory', '$http', '$rootScope', '$timeout', function ($scope, dataFactory, $http, $rootScope, $timeout) {

        var arrRoles = new Array();
        var arrDepts = new Array();
        var arrLocs = new Array();
        //var token = "";

        var data = $.param({
            username: "insights@pobi.co.uk",
            password: "Xd2actQu74pB$",
            grant_type: "password"
        });
       // $timeout(
            dataFactory.getToken(data)
            .then(function (response) {
                //$('#mydiv').hide(); 
                //token = response.data.access_token;
                //
                $rootScope.token = response.data.access_token;
                localStorage.setItem('JWT', $rootScope.token);
                BindDDL($rootScope.token);//token
            }, function (error) {
                //$scope.status = 'Unable to load customer data: ' + error.message;
                alert('Calling error in search');
            });//,3000);
        
        $scope.formInfo = {};
        $scope.formInfo.Manager = "me";
        $scope.saveData = function () {
            //$scope.formInfo.fromDate = $('#datetimepickerFrom').datetimepicker('getValue');
            //$scope.formInfo.toDate = $('#datetimepickerTo').datetimepicker('getValue');
            console.log($scope.formInfo);
            $rootScope.$broadcast("searchevent", $scope.formInfo);
            $rootScope.$broadcast("Outsearchevent", $scope.formInfo);
            $rootScope.$broadcast("Feedsearchevent", $scope.formInfo);
            $rootScope.$broadcast("TemActsearchevent", $scope.formInfo);
        };
        $scope.resetForm = function () {
            $scope.formInfo = {};
            console.log($scope.formInfo);
            $rootScope.$broadcast("clearevent", $scope.formInfo);
            $rootScope.$broadcast("Outclearevent", $scope.formInfo);
            $rootScope.$broadcast("Feedclearevent", $scope.formInfo);
            $rootScope.$broadcast("TemActclearevent", $scope.formInfo);
        };
        
        //    $scope.tableData = filteredData.length > 0 ? filteredData : $scope.MainData;
        //    return $scope.tableData;
        //};
        function BindDDL(token) {

            /** Role **/
            $http({
                method: 'GET',
                url: 'https://dev.pobi.co.uk/api/api/Search/Roles',
                async: false,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }).then(function (response) {
                $.map(response.data, function (item) {
                    arrRoles.push(item.name);
                });
                $scope.rollist = arrRoles;
            });

            /** Department **/

            $http({
                method: 'GET',
                url: 'https://dev.pobi.co.uk/api/api/Search/Dept',
                async: false,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }).then(function (response) {
                $.map(response.data, function (item) {
                    arrDepts.push(item.name);
                });
                $scope.deplist = arrDepts;
            });

            /** Location **/

            $http({
                method: 'GET',
                url: 'https://dev.pobi.co.uk/api/api/Search/Location',
                async: false,
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }).then(function (response) {
                $.map(response.data, function (item) {
                    arrLocs.push(item.name);
                });
                $scope.loclist = arrLocs;
            });
        }


    }])
 

   



