﻿var pobiapp = angular.module('Pobi', ['myApp.controllers', 'ProAct.controllers', 'PDPTeamAct.controllers', 'OutStAct.controllers', 'Feedbackuse.controllers', 'sysuse.controllers', 'progrerssRep.controllers', 'Area.controllers', 'Capbility.controllers']).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/search', {
            templateUrl: 'views/filter.html'
            //controller: storeController
        }).
        when('/progreport', {
            templateUrl: 'views/progressRep.html'
        }).
        when('/areasreport', {
            templateUrl: 'views/popularAreas.html'
        }).
        when('/proActreport', {
            templateUrl: 'views/TemProAct.html'
        }).
        when('/pdpTeamActreport', {
            templateUrl: 'views/PDPTeamAct.html'
        }).
        when('/OutstActreport', {
            templateUrl: 'views/OutstandingAct.html'
        }).
        when('/Feedbackusereport', {
            templateUrl: 'views/FeedbackRep.html'
        }).
        when('/SystemUse', {
            templateUrl: 'views/SystemUse.html'
        }).
        when('/ProgressRep', {
            templateUrl: 'views/ProgressRep.html'
        }).
        when('/capability', {
            templateUrl: 'views/Capability.html'
        }).
        otherwise({
            redirectTo: '/search'
        });
}])